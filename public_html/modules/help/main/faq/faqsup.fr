
:@Q: Comment dois-je faire pour ajouter un exercice
  dans une feuille de travail&nbsp;?
@R: � partir de la page d'accueil de la classe (quand vous �tes
  connect� en tant qu'enseignant), trouvez l'exercice d�sir�, et choisissez
  la configuration d�sir�e (niveau de difficult� etc.), puis allez travailler
  dessus. Dans le bandeau contenant diff�rents liens, il y aura un lien
  appel� `ins�rer dans une feuille d'exercices'. Cliquez dessus et suivez
  les instructions.
  <p>
  Veuillez noter que l'exercice ainsi ajout� � la feuille aura exactement
  la configuration que vous avez choisie, quand vos �l�ves travailleront
  dessus.</p>
:@Q: Est-il possible pour un �l�ve de changer la configuration
  d'un exercice fix�e par moi (enseignant)&nbsp;?
@R: Oui, c'est possible. Mais aucune note obtenue sur un exercice
  reconfigur� ne sera enregistr�e.

:@Q: Puis-je effacer une feuille active&nbsp;?
@R: Non. Une fois la feuille activ�e, elle ne peut plus �tre
  ni modifi�e ni effac�e. Pourtant, vous pouvez changer son statut �
  <em>expir�e</em> (les participants la voient mais les notes ne seront
  pas enregistr�es) ou <em>expir�e et cach�e</em> (les participants ne la
  voient plus). Mais la feuille sera toujours visible par vous.
:@Q: Je voudrais ouvrir l'enregistrement de notes d'une
  feuille pour exactement les PC d'une salle informatique.
@R: D'habitude ces PC ont tous des adresses IP commen�ant par les
  m�mes nombres, par exemple
  134.59.102.212, 134.59.102.109, 134.59.102.57, etc.
  Dans ce cas vous pouvez ouvrir l'enregistrement de notes pour l'adresse IP
  <span class="tt">134.59.102.</span>, et tous les PC ci-dessus seront admis (en plus de
  quelques autres machines ayant la m�me adresse de d�part).
:@Q: Un participant a oubli� son nom de login ou mot de passe.
@R: � partir de la page d'accueil d'enseignant, cliquez sur le lien
  `Gestion des participants' puis sur le nom du participant en question.
  Dans le cadre de propri�t�s du compte se trouve un bouton pour changer
  son mot de passe, ce que vous pouvez faire sans conna�tre
  le mot de passe actuel.
