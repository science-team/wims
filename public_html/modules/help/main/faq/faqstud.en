
:@Q: I have forgotten my personal password!
@R: You may ask your teacher to attribute a new password to your
 registration (which you should change afterwords).

:@Q: I finished an exercise but got no score.
@R: Certain exercises require you to finish a session with several
 questions before giving you a score. Be sure that you arrive at the end
 of the session.

:@Q: The server refuses to register my score.
@R: There are several things you should verify.
 <ul>
  <li>Sometimes your teacher allows you to work on the exercises but
   restrict the registration of the scores to a limited period and
   computers. If this is the case, you will see a message <em>`the
   registration of score is closed for your connection'</em> at the bottom
   of your exercise page.
  <li>If you get more scores on an exercise than required by your teacher,
   the extra points will become hidden and not counted for the total of points.
   However, the point quality will be computed on ALL scores, hidden or not.
  <li>If you change the configuration of an exercise, scores obtained by
   working on the reconfigured exercise will not be counted. And in this case
   the link ``worksheet'' will disappear from the top and bottom
   of your exercise page.
 </ul>

:@Q: Do I have the right to choose a same exercise
  several times before working on it?
@R: You can do it a few number of times (the exact number being kept
  secret) without penalty. Beyond that, you will get some zeros on
  this exercise (which reduce the score quality but do not change the
  number of points obtained).

:@Q: Can I get more points on an exercise than the number
  assigned by the teacher?
@R: You can work on an exercise as many times as you like, and get
  as many points as you like. But extra points exceeding the assigned number
  will be discarded by the server when counting your achievement (percentage
  of work done).
  <p>
  On the other hand, the score quality is calculated on all your works on
  the exercises, assigned or extra alike. So by continuing to work on the
  exercise, you have the possibility of improving this quality (and also the
  possibility of lowering it if you make errors).
  </p>

:@Q: How do the two numbers (percentage of work done
  and score quality) affect my global grades?
@R: The server computes the grades according to a formula which depends
  on the severity level of the sheet. This level is determined by the
  supervisor, and the formulas are described in the page of grades (click on
  the link `help' at the top of that page).
