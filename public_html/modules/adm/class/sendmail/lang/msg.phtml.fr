!set wims_module_log=error: $error

!if $error=nosupervisoremail
  Vous n'avez pas indiqu� d'email. Il ne vous est donc pas possible d'envoyer un mail.
  !exit
!endif

!if empty_msg=$error
  Votre message est vide !
  !exit
!endif

!if empty_user=$error
  Vous n'avez s�lectionn� aucun participant. V�rifiez les filtres.
  !exit
!endif

!if empty_mailuser=$error
  Aucun des participants s�lectionn�s n'a d'adresse email
  !exit
!endif

!if noclass=$error
  Vous n'�tes pas identifi� dans une classe. Ne jouez pas avec la barre d'adresse du navigateur !
  !exit
!endif

!if notsupervisor=$error
  Vous n'�tes pas l'enseignant de cette classe. Ne jouez pas avec la barre d'adresse du navigateur !
  !exit
!endif

!if nouser=$error
  Aucun des participants de cette classe n'a d�fini d'adresse �lectronique. Il n'est donc pas possible d'envoyer de message !
  !exit
!endif

!if nousermail=$error
  Aucun participant avec une adresse �lectronique ne correspond aux crit�res que vous avez d�finis. Aucun message ne peut �tre envoy�.
  !exit
!endif

!if sendmailteacherclose=$error
  L'enseignant de cette classe a ferm� l'exp�dition de message ou n'a pas d�fini d'adresse �lectronique. Il n'est donc pas possible de lui envoyer un message.
  !exit
!endif

!msg $error
