!set lang_exists=yes

name_title1=Envoi de mails group�s
name_title2=Message
name_noemail2=Les participants suivants n'ont pas d'email

name_participant1=Message envoy� avec succ�s �
name_participant2=Le message n'a pas pu �tre envoy� aux participants suivants
name_intro=L'envoi group� permet d'envoyer un courriel � tout ou partie des participants

name_expert0=Si vous d�sirez s�lectionner certains participants, plusieurs m�thodes sont propos�es.\
Cochez la m�thode d�sir�e.

name_expert1=� l'aide des fl�ches, faites passer dans la fen�tre de droite les participants\
auxquels vous d�sirez que le mail soit envoy�.

name_expert2=Entrez la liste des logins s�par�s par des virgules.

name_expert3=Filtrer les participants selon les valeurs des variables techniques. \

name_warning=Vous allez envoyer le message suivant.

!distribute items Destinataires,Exp�diteur,Destinataires enseignant,Liste de logins&nbsp;,Message&nbsp;,Sujet&nbsp;,S�lection&nbsp;,\
 Tous les participants (avec adresse �lectronique)\
into name_recipients,name_sender,name_recip_teacher,name_loginlist,name_message,name_subject,name_select,name_allparticipants

name_user=enseignant,enseignants,participant,participants
name_without=n'a pas d�fini d'adresse �lectronique,n'ont pas d�fini d'adresse �lectronique
name_noemail_login=Les identifiants suivants correspondent aux crit�res de s�lection mais n'ont pas d'adresse �lectronique valide
name_noemail_filter=Les participants suivants correspondent aux crit�res de s�lection mais n'ont pas d'adresse �lectronique valide
name_and=et
name_sendotherteacher=Envoyer une copie du message aux autres enseignants de la classe
name_selfsend=m'envoyer une copie du message
name_recipient=Destinataire
name_success=Message envoy� avec succ�s
name_nosendermail=Vous n'avez pas d�fini d'adresse �lectronique : l'enseignant de la classe ne pourra pas vous r�pondre.
name_sendto=Envoy� � (participant),Envoy� � (enseignant)
name_noreply=!nosubst Ceci est un message automatique. Pour y r�pondre vous devez <a href="$httpd_REMOTE_ADDR/wims/wims.cgi?module=adm/class/classes&+type=$localtype&+class=$localclass">vous connecter � la classe</a>.
name_sendby=!nosubst Envoy� par $supervisorname
name_suptype=Enseignant principal,Enseignant de la classe,Administrateur,Enseignant
