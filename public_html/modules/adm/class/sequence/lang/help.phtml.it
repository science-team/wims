!if $wims_read_parm!=$empty
  !goto $wims_read_parm
!else
  !exit
!endif

!!:hide
!!<h2>$name_hidedate</h2>
!!Se in questo campo si inserisce una data, il percorso sar� portato automaticamente nello stato <span class="tt">nascosto</span> nel giorno indicato.<br>
!!La modifica viene effettuata nell'orario della manutenzione quotidiana del server. Se la data � gi� passata, non succeder� nulla.
!!!exit

!!:show
!!<h2>$name_showdate</h2>
!!Se in questo campo si inserisce una data, il percorso sar� portato automaticamente nello stato <span class="tt">visibile</span> nel giorno indicato.<br>
!!La modifica viene effettuata nell'orario della manutenzione quotidiana del server. Se la data � gi� passata, non succeder� nulla.
!!exit

:allowtype
!! this part is a copy of file adm/class/sheet/lang/help.phtml.fr with some litle terminologie change.
!! !changeto help.phtml.fr $wims_read_parm
<h2 id="allowtype">Opzioni per la visualizzazione del percorso</h2>

� possibile inserire un controllo per limitare la visualizzazione dei percorsi
a determinati periodi e/o a determinate postazioni.
Questa configurazione non viene passata ai corsi vicini che condividono i percorsi.  L'impostazione di visibilit� del percorso ha la precedenza rispetto a questa configurazione.
<br>
Le opzioni disponibili sono
<ul>
 <li><span class="tt wims_code_words">$(name_allowtype[1])</span>:
 il percorso � visibile per tutti.</li>
 <li><span class="tt wims_code_words">$(name_allowtype[2])</span>:
 il percorso non � visbile per nessuno.</li>
 <li><span class="tt wims_code_words">$(name_allowtype[3])</span>:
 nel campo <span class="tt wims_code_words">$(name_shinfo[6])</span>,
 sar� possibile inserire un intervallo di tempo e gli IP delle postazioni abilitate alla visualizzazione del percorso. Questa restrizione pu� essere scritta mediante tre stringhe (che possono essere presenti solo in parte): <br>
 <ul>
  <li>Una data e un orario di inizio;</li>
  <li>Una data e un orario di termine;</li>
  <li>Un elenco di IP (separati da spazi);</li>
 </ul>
 (pi� avanti in questa pagina maggiori dettagli sui formati di queste
 stringhe);
</li>
<li><span class="tt wims_code_words">$(name_allowtype[4])</span>:
questa opzione pu� essere utilizzata per indicare filtri di accesso a seconda dell'appartenenza dello studente a un gruppo individuato mediante una <span class="tt wims_code_words">variabile tecnica</span> (questa variabile tecnica deve essere definita per ciascun iscritto, pu� essere definita manualmente dal docente attraverso la pagina di gestione delle variabile o la scelta pu� essere lasciata agli studenti attraverso l'utilizzo di un <span class="tt wims_code_words">$wims_name_Vote</span>). Se si sceglie questa opzione e si seleziona una variabile per ogni gruppo definito da tale variabile sar� possibile inserire (e successivamente modificare) un filtro (secondo le regole definite sopra).
Se non si precisa alcun valore come filtro per un certo valore della variabile, allora la visualizzazione del percorso � disattivata per gli studenti che hanno questo valore.
<p>
Si segnala che si pu� impotare un <span class="tt wims_code_words">$wims_name_Vote</span> utilizzando il modello <span class="tt wims_code_words">Accessi individualizzati</span> specifico per la gestione di questi filtri orari tramite variabili tecniche. Se si utilizza una variabile tecnica creata attraverso tale questionario i campi corrispondenti a ogni gruppo vengono automaticamente riempiti con i valori definiti attraverso il questionario. Questi valori possono essere comunque modificati dal docente, direttamente in questa pagina, per esempio variando date e orari (le modifiche effettuate in questa pagina non modificano i dati contenuti nel questionario).
</p>
<p>Esempio: Se abbiamo una variabile tecnica <span class="tt wims_code_variable">gruppo</span> che pu� assumere i valori 1, 2 e 3,  possiamo, ad esempio, costruire i filtri per questa variabile come nella tabella che segue:
!readproc slib/text/matrixhtml [$name_value,$name_filtre\
$name_EMPTY, 129.50.10. &#62;20160915.12&#58;00\
1,&#62;20160915.15&#58;00 &#60;20160925.12&#58;00\
2,\
3,127.0.],wimscenter wimsborder wimstable,TH=[1;]
$slib_out
</p>
Quindi
<ul><li>
per gli studenti per cui la variabile
<span class="tt wims_code_variable">gruppo</span> non � stata definita,
il percorso � visibile dal giorno
15/09/2016 alle ore 12 e unicamente per gli IP della sottorete
129.50.10. (attenzione al punto finale!)
</li><li>
per gli studenti per cui la variabile <span class="tt wims_code_variable">gruppo=1</span>,
il percorso � visibile a partire dal giorno 15/09/2016 alle ore 15 e fino al giorno 25/09/2016 alle ore 12.
</li><li>
per gli studenti per cui la variabile <span class="tt wims_code_variable">gruppo=2</span>, il percorso non � visibile.
</li><li>
per gli studenti per cui la variabile
<span class="tt wims_code_variable">gruppo=3</span>,
il percorso � visibile unicamente dalle postazioni con IP nella classe 127.0. (attenzione al punto finale!)
</li></ul>
</li>
</ul>
!if $tv_listtechvar!=$empty
 Le variabili tecniche attualmente disponibili nel corso sono:
 !read adm/vfilter/listvar.phtml
!else
 Non ci sono attualmente variabili tecniche definite nel corso. Se volete utilizzare questa opzione, dovete definire una variabile tecnica.
!endif

<h3>Formati utilizzabili per i filtri</h3>
<p>
Per le date:
</p><p>
<span class="tt wims_code_words">
&gt;yyyymmdd.hh:mm</span>
(data e ora di inizio) e/o
<span class="tt wims_code_words">
&lt;yyyymmdd.hh:mm</span>
(data e ora di fine). Queste date devono essere espresse nel fuso orario
del SERVER, e devono essere separate tramite spazi l'una dall'altra (o da
eventuali altre istruzioni simili).
</p><p>
Per le postazioni:
</p>
!read help/hosts.phtml
<p>
Esempio:
<span class="tt wims_code_words">&lt;20131007.10:30 &gt;20131001.10:00 127.0.</span>
permette la registrazione dei voti tra le due date indicate e solamente per
connessioni provenienti IP che cominciano con
<span class="tt wims_code_words">127.0</span>.
</p>

!exit
