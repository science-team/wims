!set lang_exists=yes

!set name_sequence=S�quence
!set name_title=Ajouter/Modifier la s�quence
!set name_addseq= Mettez des num�ros correspondant � l'ordre des ressources que vous d�sirez ins�rer.\
Si vous d�sirez effacer un num�ro, �crivez 0.

!set name_show1=Vous pouvez cr�er une s�quence p�dagogique
!set name_show1c=. Elle sera alors avec le statut "En pr�paration"\
  et donc non visible par les participants. Pour que la pr�sentation en s�quences remplace la pr�sentation propos�e par\
  d�faut par le serveur, vous devez l'indiquer � l'aide du menu Configurer. Chaque s�quence pourra �tre ensuite rendue\
  visible ou cach�e � un groupe de participants pendant une plage horaire choisie ind�pendamment du statut des autres s�quences.

!set name_show2=!nosubst (cliquer sur le lien Ajouter une s�quence) ou modifier les s�quences d�j� cr��es.\
  Pour que la pr�sentation en s�quences remplace la pr�sentation propos�e par d�faut par le serveur, vous devez\
  l'avoir indiqu� � l'aide du menu Configurer. Chaque s�quence pourra �tre ensuite rendue visible ou cach�e\
  � un groupe de participants pendant une plage horaire choisie ind�pendamment du statut des autres s�quences.

!set name_show3=Si vous ajoutez une ressource dans votre classe, vous devez l'ajouter dans une s�quence pour qu'elle puisse �tre rendue visible aux participants.

!set name_config_reverse=Les s�quences sont pr�sent�es par d�faut par ordre chronologique,\
  de la plus ancienne � la plus r�cente. Les pr�senter dans l'ordre inverse
!set name_config_generic=Mot g�n�rique pour "$name_sequence"

!set name_config_status=Utilisation des s�quences dans la page d'accueil des participants
!set wims_name_sequence=!defof sequence_Title in wimshome/log/classes/$wims_class/seq/.def
!set name_info=Informations g�n�rales
!set name_content=Contenu de la s�quence

!set name_desc_title=!nosubst limit� � $title_limit caract�res
!set name_desc_desc=!nosubst limit� � $desc_limit caract�res; les tags et liens html sont admis
!!set name_showdate=Date d'activation automatique de la s�quence
!!set name_hidedate=Date de fermeture automatique de la s�quence
!!set name_desc_showdate=A cette date, la s�quence sera automatiquement montr�e.
!!set name_desc_hidedate=A cette date, la s�quence sera automatiquement cach�e.

!set name_showhidetitle=Affichage de la s�quence aux participants
!let name_allowtype=visible pour tous,cach�e pour tous,visible pour postes (et/ou heures) suivants,visibilit� r�gl�e par une variable technique
!set name_desc_allowtechvar=Choix de la variable technique
!set name_desctableval=Table de filtre en fonction de la valeur de la variable technique
!set name_value=Valeur
!set name_filtre=Filtre
!set name_EMPTY=(aucune valeur)
!set name_allowshare=pour toutes les classes en partage :
