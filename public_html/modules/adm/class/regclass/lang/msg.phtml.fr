!set lang_exists=yes

!default wims_name_passsup=$wims_name_Password ($name_sup)
!default wims_name_password=$wims_name_Password ($name_classesss)
!default wims_name_institution=$name_Name_portal
!default wims_name_description=$wims_name_name $name_classesss

!set cname=!item $cltype+1 of une classe virtuelle,,un groupement de classe,,\
  un portail d'�tablissement

!if noagreecgu iswordof $error
  $noagreecgu_msg
  !exit
!endif

!if no_subclass iswordof $error
  Vous n'avez pas le droit de cr�er des sous-classes dans la situation
  actuelle.
  !set restart=no
  !exit
!endif

!if no_right iswordof $error
  D�sol�, vous n'avez pas le droit de cr�er des classes virtuelles sur ce site.
  Veuillez
  !mailurl $wims_site_manager contacter le gestionnaire du site\
Cr�ation de classe virtuelle
  pour plus de d�tails.
  !set restart=no
  !exit
!endif

!if not_manager iswordof $error
  D�sol�, seul le gestionnaire du site a le droit de cr�er $cname.
  !exit
!endif

!if getpass iswordof $error
  <div class="wims_msg warning">
    Le mot de passe de cr�ation qui vous a �t� transmis par l'administrateur
    ne doit pas �tre communiqu�.
    !if $cltype=2
      De m�me, vous vous engagez � ne diffuser le mot de passe d'inscription des enseignants
      � votre groupement qu'aux enseignants dont vous souhaitez l'inscription
      dans votre groupement. En aucun cas ce mot de passe n'aura une diffusion publique.
      L'administrateur du serveur se r�serve le droit de fermer votre groupement
      en cas de manquement � cette r�gle.
    !endif
  </div>
  !if $sendmail!=$empty
    Le mot de passe a �t� envoy� � l'adresse �lectronique $sendmail.
    <br class="spacer">
  !endif
  !if $regpass!=$empty
    <div class="wims_msg warning">
      D�sol�, votre mot de passe n'est pas correct. Veuillez r�essayer.
    </div>
  !else
    !if $sendmail=$empty
      La cr�ation d'$cname sur ce site est prot�g�e par un mot de passe.
    !endif
  !endif
  !form reply
  <input type="hidden" name="step" value="$step">
  <input type="hidden" name="job" value="$job">
  <label for="regpass">
    Entrez le mot de passe&nbsp;:
  </label>
  <input size="16" name="regpass" type="password" id="regpass" required>
  <input type="submit" value="$wims_name_send">
  !formend
  </div>
  <div class="wims_msg info">
    Le mot de passe n�cessaire � la cr�ation de classes virtuelles
    peut �tre obtenu aupr�s du
    !mailurl $wims_site_manager gestionnaire\
Mot de passe pour la cr�ation de classes virtuelles
    de ce site WIMS.
    !if $regpassmail!=$empty and $sendmail=$empty
      !form reply
      Si vous disposez d'une adresse �lectronique dont le domaine est reconnu par le serveur,
        vous pouvez �galement recevoir le mot de passe par message �lectronique
        en saisissant <label for="adresse1">votre adresse �lectronique </label> ci-contre :
        <div class="wimscenter">
          <input type="text" id="adresse1" name="adresse1" value="$adresse1" size="20">
          <input type="hidden" name="step" value="0">
          !let nbaddr=!itemcnt $regpassmail
          !if $nbaddr=1
            @$regpassmail
            <input type="hidden" name="adresse2" value="$regpassmail">
          !else
            @
            !formselect adresse2 list $regpassmail
          !endif
          <input type="submit" value="$wims_name_send">
        </div>
      !formend
    !else
      !reset sendmail
    !endif
  </div>
  !set restart=no
  !exit
!endif

!if getid iswordof $error
  !if $regpass$regid!=$empty
    D�sol�, votre mot de passe n'est pas correct. Veuillez r�essayer.
  !else
    La cr�ation de classes virtuelles sur ce site est prot�g�e par un syst�me
    d'authentification. Veuillez vous identifier.
  !endif
  </div>
  !form reply
    <fieldset class="property_fields halfwidth blockcenter">
      <div class="field box">
        <label for="regid">
          Entrez le nom de votre compte :
        </label>
        <input size="20" name="regid" id="regid" required>
      </div>
      <div class="field box">
        <label for="regpass">
          Et le mot de passe :
        </label>
        <input size="16" name="regpass" id="regpass" type="password"  required>
      </div>
      <div class="wimscenter">
        <input type="submit" value="$wims_name_send">
      </div>
    </fieldset>
  !formend
  <p>
    Remarque. Veuillez �crire au
    !mailurl $wims_site_manager gestionnaire\
Mot de passe pour la cr�ation de classes virtuelles
    de ce site si vous voulez un compte de cr�ation de classes virtuelles.
  </p>
  !set restart=no
  !exit
!endif

!if class_quota=$error
  Vous avez le droit d'installer jusqu'� $r_quota1 classes. Vous avez fait le
  plein&nbsp;; vous ne pouvez plus en ajouter d'autres.
  !set restart=no
  !exit
!endif

!if bad_secure=$error
  Votre poste actuel ne fait pas partie de la d�finition
  d'acc�s s�curis� (<span class="tt wims_code_words">$secure</span>)&nbsp;!
  C'est probablement une erreur quiaurait la cons�quence de vous interdire l'acc�s
  de contr�le de votre classe.
  <p>
  Lisez attentivement la documentation ci-dessous. Vous pouvez aussi
  laisser ce champ vide (ou mettez un espace) : des codes secrets
  temporaires vous seront envoy�s � chaque op�ration sensible. Vous pouvez aussi
  pour d�sactiver cette mesure de s�curit� en mettant le mot <span class="tt wims_code_words">all</span>.
  </p><hr>
  !read help/hosts.phtml
  !reset secure
  !exit
!endif

!if has_empty=$error
  Vous n'avez pas donn� toutes les informations n�cessaires pour la cr�ation
  d'une classe. Veuillez compl�ter vos donn�es avant de les soumettre.<br>
  <span class="tt wims_code_words">$(wims_name_$error_subject) requis.</span>
  !exit
!endif

!if too_short=$error
  Le champ de donn�es <span class="tt wims_code_words">$(wims_name_$(error_subject))</span>
  semble �tre trop court.
  !exit
!endif

!if bad_date=$error
  Votre date d'expiration n'est pas correcte.
  !exit
!endif

!if anti_date=$error
  Votre classe a une date d'expiration ant�rieure � aujourd'hui. Elle serait donc
  expir�e avant d'�tre cr��e!
  !exit
!endif

!if bad_level=$error
  Mauvaise valeur de niveau.
  !exit
!endif

!if bad_email=$error
  Votre adresse �lectronique n'est visiblement pas valable.
  <p>
  La cr�ation de classe ne peut r�ussir que si vous
    soumettez VOTRE VRAIE adresse �lectronique.
  </p>
  !exit
!endif

!if existing=$error
  La classe $classname semble d�j� exister. Vous ne pouvez pas recr�er la
  m�me classe.
  !exit
!endif

!if pass_discord isin $error
  Le mot de passe (
  !if sup isin $error
    enseignant
    !if class isin $error
      et classe
    !endif
  !else
    !if class isin $error
      classe
    !endif
  !endif
  ) que vous avez retap� ne correspond pas au premier. Votre
  cr�ation de classe n'est donc pas pris en compte&nbsp;; vous pouvez r�essayer encore
  une fois.
  !exit
!endif

!if bad_pass=$error
  Le <strong>$(wims_name_$(error_subject))</strong> que vous avez choisi
  contient des caract�res ill�gaux.<br>
  Veuillez utiliser un mot contenant uniquement des chiffres ou des lettres
  sans accent et sans espace entre eux.
  !exit
!endif

!if bad_code=$error
  Vous n'avez pas entr� le bon code pour la classe. L'adresse �lectronique
  que vous avez fournie est-elle correcte&nbsp;?
  <p>
  Nous avons enregistr� cet �chec.
  </p>
  !exit
!endif

!if define_fail=$error or abuse=$error
  Le serveur n'a pas r�ussi � ins�rer votre classe dans la base de
  donn�es. C'est une erreur interne du logiciel.
  <p>
    Veuillez rapporter le probl�me au
    !mailurl $wims_site_manager gestionnaire de ce site\
user registration failure
. Merci&nbsp;!
  </p>
  !exit
!endif

!if duplicate=$error
  Vous avez tent� de recr�er une classe d�j� cr��e. Peut-�tre
  avez-vous cliqu� sur le bouton <span class="tt">actualiser</span>&nbsp;? En tout cas, votre classe
  $classname est d�j� en place et la deuxi�me tentative de cr�ation
  est ignor�e.
  <div>
  !read adm/lang/links.phtml.$modu_lang
  </div>
  !exit
!endif

!if classdontexists=$error
  Cette classe n'existe pas.
  !exit
!endif

!if notexempleclass=$error
  Cette classe n'est pas une classe d'exemple : impossible de la copier.
  !exit
!endif

!if badcpmethod=$error
  M�thode de copie impossible � identifier. Recommencer l'op�ration.
  Si l'erreur persiste contacter l'administrateur du serveur.
  !exit
!endif

!if cloningnotallow=$error
  La duplication de cette classe n'est pas autoris�e par son administrateur.
  !exit
!endif

!if badcloningpwd=$error
  Mauvais mot de passe de duplication.
  !exit
!endif

!if disallowcloning=$error
  Aucune duplication de cette classe ne peut �tre r�alis�e.
  !exit
!endif

!if subclasslimit=$error
  Vous avez atteint le nombre maximum de sous-classes ($max_subclasses).
  Il n'est pas possible d'en cr�er d'autres.
  !exit
!endif

!if bad_file iswordof $error
  D�sol� mais je ne peux pas reconna�tre <em>$wims_deposit</em>
  comme un fichier de sauvegarde d'une classe. Soit vous avez envoy� un mauvais
  fichier, soit le fichier est endommag�.
  !exit
!endif

!msg $error
