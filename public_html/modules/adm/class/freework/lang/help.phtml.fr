!if $wims_read_parm!=$empty
  !goto $wims_read_parm
!endif

:type
<h2>Types possibles de devoirs libres</h2>
Il y a trois types possibles de devoirs libres&nbsp;:
<ul>
  <li><b>$(name_type_list[1])</b>&nbsp;:
    Permet simplement de diffuser l'�nonc� d'un devoir libre (fichier) aux participants
    puis de diffuser la correction du devoir libre.
  </li>
  <li><b>$(name_type_list[2])</b>&nbsp;:
!if $conf_nofile=yes
  <span style="color:grey">(d�sactiv� sur ce serveur)</span>
!endif
    Permet de diffuser l'�nonc� d'un devoir libre aux participants (fichier) puis de
    diffuser le corrig�. Chaque participant peut d�poser sa composition,
    l'enseignant peut
    <ul><li>
    corriger et diffuser aupr�s de chaque participant sa copie corrig�e&nbsp;;
    </li><li>
    diffuser un corrig� global&nbsp;;
    </li><li>
    attribuer une note manuelle relative � ce devoir.
    </li></ul>
    Cette activit� utilise beaucoup de capacit� de stockage de la classe.
  </li>
  <li><b>$(name_type_list[3])</b>&nbsp;:
    Permet de construire un assemblage de zones de r�ponses avec
    du texte ou diff�rents applets. Les applets disponibles sont&nbsp;:
    <ul>
      <li>zone de texte&nbsp;;</li>
      <li>d�p�t de fichiers&nbsp;;
        !if $conf_nofile=yes
          <span style="color:grey">(d�sactiv� sur ce serveur)</span>
        !endif
      </li>
      <li>figure GeoGebra&nbsp;;</li>
      <li>s�ries d'exercices WIMS.</li>
    </ul>
    Chaque participant r�alise sa composition
    (dans les diff�rentes zones). L'enseignant peut
    <ul><li>visualiser les compositions de chaque participant&nbsp;;
      </li><li>diffuser un corrig� global&nbsp;;
      </li><li>attribuer une note manuelle relative � ce devoir.
    </li></ul>
  </li>
</ul>
!exit

:sizelimitfile
<h2>Limitation de la taille des fichiers</h2>
La taille globale des fichiers que le participant pourra d�poser est limit�e
(en Mo).
La taille limite multipli�e par le nombre de participants doit rester inf�rieure �
la place disponible dans la classe (affich�e sur la page).
<div class="wims_msg warning">
Attention, un participant ne pourra pas d�poser un fichier m�me de taille inf�rieure
� la taille limite lorsque la capacit� globale de la classe est atteinte.
</div>
Par exemple : si vous disposez de 30 Mo dans une classe comportant 35 participants
et que vous fixez la capacit� par participant � 0.75 Mo, le devoir libre pourra �tre activ�
(puisque vous consommerez th�oriquement 26,25 Mo). Cependant, si entre temps vous
d�posez des fichiers dans la classe ou que des participants enregistrent des exercices
ou des examens,la place disponible risque de diminuer et certains participants pourraient
ne pas pouvoir d�poser leur devoir.<br>
Dans le cas de devoir libre de type 3, la taille s'applique � l'ensemble des zones de type fichier.
!exit

:datedeadline
<h2>Date � laquelle le participant devra avoir rendu sa copie.</h2>
<ul>
<li>Lorsque le devoir libre est de type <span class="wims_code_words">$(name_type_list[2])
</span> ou <span class="wims_code_words">$(name_type_list[3])</span>, l'interface de
d�p�t de copies est activ�e avant la date et tant que le statut du devoir libre est
<span class="wims_code_words">Actif</span>.
</li><li>
Une fois que cette date est d�pass�e, l'enseignant pourra t�l�charger les copies
des participants et d�poser la copie corrig�e de chaque participant.</li>
<li>Les participants ne peuvent pas acc�der � leur copie corrig�e tant que toutes
les copies corrig�es ne sont pas d�pos�es et que la date de diffusion de
la solution n'est pas d�pass�e.
</li></ul>
!exit

:datetimesoldate
<h2>Date � partir de laquelle les �l�ments de correction seront disponibles
au t�l�chargement par les participants.</h2>
<ul>
<li>Les fichiers communs de solutions peuvent �tre ajout�s/modifi�s/effac�s � tout moment.</li>
<li>Les participants peuvent �galement, apr�s cette date, t�l�charger leur copie
corrig�e une fois que toutes les corrections de copies ont �t� d�pos�es par
l'enseignant de la classe et tant que le statut du devoir libre reste actif.</li>
<li>Lorsque le statut est p�rim�, les participants ne peuvent plus t�l�charger
leur copie corrig�e mais seulement les fichiers d'�nonc� et de corrig� communs
� tous (quelle que soit la date de diffusion de solution).</li>
<li>Les participants peuvent acc�der aux fichiers communs de correction d�s que
la date de solution est d�pass�e, m�me si toutes les copies ne sont pas corrig�es
dans le cas des types <span class="wims_code_words">$(name_type_list[2])</span> ou
<span class="wims_code_words">$(name_type_list[3])</span>.</li>
</ul>
!exit

:scoring
<h2>Notation</h2>
Il est possible d'ajouter une note manuelle pour chaque participant de la classe.
<br>
L'interface permet de saisir directement la note de chaque participant en
m�me temps que le d�p�t de copies corrig�es.<br>
Lorsque cette option est activ�e, une colonne (avec un titre g�n�rique et un coefficient 1)
est automatiquement rajout�e au tableau des notes manuelles lors de l'activation du devoir libre.
<br>
Cette option n'est pas disponible dans un portail.
!exit

:typezone
<h2>Type des zones</h2>
Un devoir est compos� de deux parties&nbsp;:
<ul>
<li>la partie de construction de l'�nonc� (qui sera ensuite visible aux participants)&nbsp;;</li>
<li>la partie de construction de la zone de r�ponse de le participant. Cette partie permettra
� le participant de saisir sa r�ponse.
</li>
</ul>
Chaque partie est d�compos�e en un certain nombre de zones. Il y a quatre types
de zone (pour l'�nonc� aussi bien que pour les r�ponses des participants)&nbsp;:
<ul>
<li><b>GeoGebra</b> Affiche un applet GeoGebra interactif.</li>
<li><b>texte</b> une zone de saisie de texte. Les tags html sont accept�s.
Il est possible d'�crire des formules math�matiques en les pla�ant entre \( et \).<br>
Par exemple, \(\sqrt{2}\) affichera
!insmath \sqrt{2}
.
</li>
<li><b>fichier</b>
!if $conf_nofile=yes
  <span style="color:grey">(d�sactiv� sur ce serveur)</span>
!endif
 une zone de d�p�t de fichier. On peut d�poser plusieurs fichiers dans
chaque zone de fichier.</li>
<li><b>RandFile</b> (uniquement pour la partie �nonc�)
!if $conf_nofile=yes
  <span style="color:grey">(d�sactiv� sur ce serveur)</span>
!endif
 une zone de d�p�t de fichier. On peut d�poser plusieurs fichiers dans
chaque zone de fichier, l'un de ces fichiers sera choisi au hasard et propos� � l'�tudiant.</li>
<li><b>exercice WIMS </b> (uniquement pour la partie �nonc�)
Une liste d'exercices WIMS � r�aliser. Le participant doit r�ussir la s�rie avant de pouvoir
l'enregistrer puis r�diger une solution d�taill�e. Cette solution peut �tre remise
� l'enseignant en utilisant les zones de la partie r�ponse. Si aucune zone de
r�ponse n'est mise en place, il s'agit d'une remise au format papier.
</li>
</ul>
Il peut y avoir plusieurs zones du m�me type dans la partie �nonc� (sauf pour le type exercice WIMS)
aussi bien que dans la partie r�ponse des participants. Le nombre de zones de r�ponse utilis�es
et leur nature sont librement choisis.<br>
Si la partie r�ponse ne contient aucune zone, c'est que le document devra �tre rendu directement
sous format papier.
!exit

:zonegeogebra
Le descriptif des champs � remplir pour ce type de zone&nbsp;:
<ul>
<li><b>Titre :</b> le titre de la zone. Si ce champ reste vide un titre automatique sera g�n�r�.</li>
<li><b>Description :</b> une description de la zone pouvant contenir des consignes
  sp�cifiques (il sera affich� avec le style css
  <span class="wims_code_words">freeworkdesczone</span>).</li>
<li><b>Option de param�trage de l'applet&nbsp;:</b> cette zone permet de d�finir les options
  de param�trages de l'applet (voir le site de GeoGebra pour plus de d�tails).
  Il faut �crire une option par ligne. Par exemple&nbsp;:
<pre>
showToolBar=true
customToolBar="0|40|||1|2|5@10"
width=800
height=500
number=1
</pre></li>
<li><b>Faire appara�tre cette zone pour les participants</b> (option disponible seulement
  dans la partie �nonc�)&nbsp;: cette option permet de faire appara�tre ou non cette zone
  dans la page d'�nonc� pour le participant (� combiner avec l'option suivante,
  elle permet de mettre � disposition des participants une zone GeoGebra avec un contenu).
  Si cette zone est affich�e sur la page participant, elle ne sera pas modifiable
  par ces derniers.</li>
<li><b>Copier le contenu de la zone enseignante num�ro</b> (option disponible
  seulement dans la partie r�ponse du participant)&nbsp;: cette option permet de pr�charger
  l'applet avec le contenu de l'un des applets GeoGebra de la zone enseignant
  (par exemple pour contenir une figure de d�part).</li>
<li><b>Contenu de la zone</b> (seulement dans la partie �nonc�) une fen�tre d'applet
  permet de r�aliser la figure.
</li></ul>
!exit

:zonetexte
Le descriptif des champs � remplir pour ce type de zone&nbsp;:
<ul>
<li><b>Titre :</b> le titre de la zone. Si ce champ reste vide un titre automatique sera g�n�r�.</li>
<li><b>Description :</b> une description de la zone pouvant contenir des
  consignes sp�cifiques (il sera affich� avec le style css
    <span class="wims_code_words">freeworkdesczone</span>).</li>
<li><b>Contenu de la zone</b> (seulement dans la partie �nonc�) une zone de
  texte permet de saisir le texte de l'�nonc�.
</li></ul>
!exit

:zonefile
Le descriptif des champs � remplir pour ce type de zone&nbsp;:
<ul>
<li><b>Titre :</b> le titre de la zone. Si ce champ reste vide un titre automatique sera g�n�r�.</li>
<li><b>Description :</b> une description de la zone pouvant contenir
  des consignes sp�cifiques (il sera affich� avec le style css
  <span class="wims_code_words">freeworkdesczone</span>).</li>
<li><b>Nombre maximum de fichiers</b> (seulement dans la partie r�ponse)&nbsp;:
 permet de limiter le nombre de fichiers d�posables par les participants.</li>
<li><b>Contenu de la zone</b> (seulement dans la partie �nonc�)&nbsp;:
  elle permet de d�poser des fichiers pour l'�nonc�
  (l'interface de d�p�t se trouve en bas de page).</li>
</ul>
!exit

:zonerandfile
Le descriptif des champs � remplir pour ce type de zone&nbsp;:
<ul>
<li><b>Titre :</b> le titre de la zone. Si ce champ reste vide un titre automatique sera g�n�r�.</li>
<li><b>Description :</b> une description de la zone pouvant contenir
  des consignes sp�cifiques (il sera affich� avec le style css
  <span class="wims_code_words">freeworkdesczone</span>).</li>
<li><b>Contenu de la zone</b> (seulement dans la partie �nonc�)&nbsp;:
  elle permet de d�poser des fichiers pour l'�nonc�
  (l'interface de d�p�t se trouve en bas de page).
  Pour chaque participant, un seul de ces fichiers, choisi au hasard, sera montr�.</li>
</ul>
!exit

:zonewimsexo
Le descriptif des champs � remplir pour ce type de zone&nbsp;:
<ul>
  <li><b>Titre :</b> le titre de la zone. Si ce champ reste vide un titre automatique sera g�n�r�.</li>
  <li><b>Description :</b> une description de la zone pouvant contenir des
  consignes sp�cifiques (il sera affich� avec le style css
    <span class="wims_code_words">freeworkdesczone</span>).</li>
  <li><b>Contenu de la zone :</b> un tableau pr�sente la liste des exercices � traiter. Le rajout de s�ries d'exercices s'effectue par la
m�me m�thode que pour les feuilles d'exercices (en utilisant le moteur de recherche de ressources). Attention pour les
exercices OEF, le param�trage de l'affichage de la solution d�taill�e et des feedback ne sera pas conserv�. Ces options sont
g�r�es en interne lors de l'insertion de la s�rie d'exercices dans le devoir libre.
  </li>
  <li><b>Note minimale :</b> il s'agit de la note � partir de laquelle la s�rie d'exercices pourra �tre enregistr�e pour
    passer � la phase de r�daction. Cet enregistrement peut �tre&nbsp;:
      <ul>
        <li>Manuel : le participant d�cide d'enregistrer ou non la s�rie d'exercices. S'il le fait il ne peut plus recommencer une
          nouvelle s�rie pour ce devoir libre. S'il d�cide de ne pas enregistrer la s�rie, il devra � nouveau la
          r�ussir en atteignant le score minimal demand�.</li>
        <li>Automatique : l'enregistrement est automatiquement effectu�. Le participant ne peut plus recommencer d�s le premier succ�s.
         </li>
        </ul>
  </li>
</ul>
� noter que l'activit� des participants est trac�e. Autrement dit, il est possible de conna�tre le nombre de tentatives
sur chaque s�rie d'exercices propos�e (et les scores obtenus).
!exit

:allowtype
!! this part is a copy of file adm/class/sheet/lang/help.phtml.fr need to be checked.
Vous pouvez imposer une restriction, par exemple dans le temps, pour
l'enregistrement des notes. Plusieurs options sont disponibles&nbsp;:
<ul>
 <li><span class="tt wims_code_words">$(name_allowtype[1])</span>&nbsp;:
les notes seront enregistr�es.</li>
 <li><span class="tt wims_code_words">$(name_allowtype[2])</span>&nbsp;:
aucune note ne sera enregistr�e.</li>
 <li><span class="tt wims_code_words">$(name_allowtype[3])</span>&nbsp;:
une interface de saisie,
<span class="tt wims_code_words">$(name_shinfo[6])</span>,
appara�t et vous pouvez indiquer une plage horaire ainsi que les
adresses IP � partir desquelles les notes seront enregistr�es.
Cette restriction s'�crit � l'aide d'une expression en trois parties (qui peuvent
 ne pas toutes �tre pr�sentes)&nbsp;: <br>
 <ul>
  <li>Une date et une heure de d�but&nbsp;;</li>
  <li>Une date et une heure de fin&nbsp;;</li>
  <li>Une plage d'IP&nbsp;;</li>
 </ul>
Les dates (format <span class="tt wims_code_words">aaaammjj</span>) et heures
(format <span class="tt wims_code_words">hh:mm</span>) doivent �tre en temps local du SERVEUR.
 <p>
Exemple :
$wims_name_form <span class="tt wims_code_words">20131007</span> $wims_name_at <span class="tt wims_code_words">10:30</span> $wims_name_to <span class="tt wims_code_words">20131001</span> $wims_name_at <span class="tt wims_code_words">10:00</span> $wims_name_IP<span class="tt wims_code_words"> 127.0.</span> <br>
permet l'enregistrement des notes entre 2 dates et seulement pour les num�ros IP commen�ant
par <span class="tt wims_code_words">127.0.</span>.
</p>
</li><li><span class="tt wims_code_words">$(name_allowtype[4])</span>&nbsp;:
cette option peut �tre choisie afin d'individualiser les restrictions
d'acc�s � l'aide d'une <span class="tt wims_code_words">variable
technique</span> (cette variable technique est d�finie pour chaque
participant soit manuellement en utilisant le module de gestion des
variables techniques soit � l'aide d'un
<span class="tt wims_code_words">$wims_name_Vote</span>).
Lors du choix de cette option, un menu permet de choisir la variable
 technique qui sert de diff�renciation et un tableau permet
d'effectuer un r�glage du filtre (en utilisant le m�me format que
pr�c�demment) pour chaque valeur possible de la variable.
Ne rien pr�ciser comme filtre  pour une valeur de la variable technique signifie
ne pas ouvrir l'enregistrement des notes pour les participants qui ont cette valeur.
<p>
Dans le cas d'une variable technique d�finie � l'aide du mod�le
<span class="tt wims_code_words">Acc�s individualis�</span>
d'un <span class="tt wims_code_words">$wims_name_Vote</span>
sp�cifique � la cr�ation de r�servation de cr�neau horaire,
le tableau est d�j� pr�-rempli avec les donn�es propos�es lors de la
 cr�ation du questionnaire. Ces donn�es peuvent �tre modifi�es,
mais les modifications ne sont pas r�percut�es sur le questionnaire.
</p>
<p>Exemple : Supposons que le tableau d�crivant les filtres pour la variable technique
<span class="tt wims_code_variable">groupe</span> prenant les valeurs 1, 2 et 3 soit&nbsp;:
!readproc slib/text/matrixhtml [$name_value,$name_filtre\
$name_EMPTY, 129.50.10. &#62;20160915.12&#58;00\
1,&#62;20160915.15&#58;00 &#60;20160925.12&#58;00\
2,\
3,127.0.],wimscenter wimsborder wimstable,TH=[1;]
$slib_out
</p>
Dans la limite de la date de p�remption de la feuille,
<ul><li>
pour les participants pour lesquels
<span class="tt wims_code_variable">groupe</span> n'a pas de valeur,
l'enregistrement des notes est ouvert � partir du
15/09/2016 12h et uniquement sur les postes dont l'IP commence par
129.50.10.
</li><li>
pour les participants pour lesquels <span class="tt wims_code_variable">groupe=1</span>,
l'enregistrement des notes est ouvert entre le 15/09/2016 15h et le 25/09/2016 12h.
</li><li>
pour les participants pour
lesquels <span class="tt wims_code_variable">groupe=2</span>, l'enregistrement des notes n'est pas ouvert.
</li><li>
pour les participants pour lesquels
<span class="tt wims_code_variable">groupe=3</span>,
l'enregistrement des notes est ouvert uniquement � partir des postes
dont l'IP commence par 127.0.
</li></ul>
</li>
<li>Ces r�glages peuvent �tre propag�s&nbsp;:
<ul>
  <li>Dans un groupement, pour des classes en partage � partir de la classe ayant initi� le partage et en utilisant une variable technique provenant du groupement.</li>
  <li>Dans un portail, � partir d'un programme vers les cours en utilisant une variable technique provenant du portail ou du niveau.</li>
</ul>
</li>
</ul>

!if $tv_listtechvar!=$empty
 Les variables techniques actuellement disponibles dans votre classe sont&nbsp;:
 !read adm/vfilter/listvar.phtml
!else
<div class="wims_msg info">
 Il n'y a actuellement aucune variable technique d�finie dans votre
classe. Si vous voulez utiliser cette option, il faut d'abord
d�finir une variable technique.
</div>
!endif
!exit