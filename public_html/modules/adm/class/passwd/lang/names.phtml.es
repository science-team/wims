!set lang_exists=yes

!!set module_title=Cambio de la contrase�a

!distribute line Elija el tipo de contrase�a\
  Su contrase�a personal\
  para entrar en la clase como profesor\
  Contrase�a de los participantes\
  Contrase�a de la clase\
  utilizada por los <strong>participantes</strong> para inscribirse\
  Contrase�a de la clase\
  utilizada por los <strong>profesors</strong> para inscribirse\
  Introduzca la contrase�a actual\
  Solo requerido cuando cambie su contrase�a personal\
  Ahora la contrase�a nueva\
  Vuelva a ingresar la contrase�a nueva\
  Para las contrase�as personales: preceda la contrase�a nueva con el signo <span class="tt d">+</span> si desea a�adir contrase�as de un solo uso.\
  La contrase�a debe estar compuesta �nicamente de entre 4 y 16 caracteres alfanum�ricos.\
  el programa acepta la contrase�a vac�a, pero es posible que el administrador del sitio prohiba las clases sin contrase�a\
into name_passwdtype, name_userpasswd, name_userpasswd_hlp, name_partpasswd, name_classpasswd, name_classpasswd_hlp, name_classpasswdt, name_classpasswdt_hlp,\
name_actualpasswd, name_actualpasswd_hlp, name_newpasswd,  name_newpasswd2, name_jetable, name_alphanumer, name_emptypasswd

name_desc_forcechpwd=Su contrase�a actual fue establecida por el servidor (procedimiento de env�o de contrase�a por correo electr�nico) \
o por su profesor/a. Debe definir una nueva contrase�a para garantizar la seguridad de su cuenta.
name_desc_forcechpwd2=Su contrase�a actual fue establecida por el servidor (procedimiento de env�o de contrase�a por correo electr�nico) \
 o por el administrador del grupo/portal. Debe definir una nueva contrase�a para garantizar la seguridad de su cuenta.
