!set wims_module_log=ERROR $error

!if $error=bad_password
  Mot de passe non reconnu. Veuillez r�essayer apr�s quelques secondes.
  <span class="wims_warning">$wims_name_warning</span> ! Tout mot de passe envoy� dans les 5 secondes
  qui suivent sera rejet�&nbsp;!
  <span id="badpwd"></span>.
  !exit
!endif

!if $error=in_exam
  Vous ne pouvez pas changer de classe quand vous �tes en train de passer
  un examen.
  !exit
!endif

!if $error=recent_rafale
  Ce compte est bloqu� pendant 10 minutes � cause d'activit�s irr�guli�res.
  !exit
!endif

!if $error=no_cgu
  Vous n'avez pas accept� les conditions g�n�rales d'utilisation de ce serveur (CGU).
  Vous devez d'abord accepter les CGU avant de pouvoir vous identifier.
  !form reply
  !formcheckbox agreecgu list yes prompt $name_acceptcgu
   [
   !href cmd=help $name_seecgu
   ]
   !let save_logincgu=$auth_user
  <div class="wimscenter wimsform">
  <input type="submit" value="$wims_name_tosave">
  </div>

  !formend
  !exit
!endif

!if $error=bad_loginmail
  Votre identifiant n'est pas reconnu ou vous n'avez pas d�fini d'adresse �lectronique lors de votre inscription.
  <br>
  Demandez � votre enseignant de vous attribuer un nouveau mot de passe.
  <span class="wims_warning">$wims_name_warning</span>&nbsp;!
  Cette proc�dure de r�cup�ration de mot de passe est d�sactiv�e pendant 5 secondes &nbsp;!
  <span id="badpwd"></span>.
  !exit
!endif

!if $error=bad_cas_connexion
  Un probleme est survenu lors de la connexion CAS.
  <div>$response</div>
  !exit
!endif

!if $error=fullexamplecls
  Il n'est pas possible de vous connecter � cette classe ouverte avec un compte anonyme (il y a d�j� trop de personnes connect�es de cette fa�on). Vous pouvez :
  <ul>
    <li>vous y inscrire (sous r�serve de place) ;</li>
    <li>essayez de travailler sur cette classe sur un 
    !href module=adm/light&+phtml=mirror.phtml.fr autre serveur
.
    </li>
  </ul>
  !exit
!endif

!msg $error
