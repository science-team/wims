!if $special_parm!=$empty
 !read help/$special_parm.phtml
 !goto end
!endif

!if $login=$empty
 <p>
 Cet outil permet la cr�ation et le d�veloppement en ligne de modules
 d'activit�s WIMS ordinaires.
 </p><p>
 Un tel module peut �tre un exercice de pleine puissance (compar� aux
 exercices OEF qui sont plus faciles � �crire mais ont des capacit�s
 limit�es&nbsp;; voir
 <a target="wims_help" href="wims.cgi?module=adm/createxo&+cmd=help&+special_parm=oef&+session=$wims_session">$wims_name_docoef</a>
 ) ou un outil de calcul sophistiqu�. Il doit �tre �crit dans le langage
 interpr�t� de WIMS, comme expliqu� dans
 <a target="wims_help" href="wims.cgi?module=help/wimsdoc.en">$wims_name_doctec</a>
.
 </p><p>
 Si vous �tes int�ress�, vous pouvez demander au
 !mailurl $wims_site_manager gestionnaire de ce site WIMS\
WIMS Modtool id
 une identification login/mot de passe de d�veloppeur qui
 vous permettra d'utiliser cet outil.
 </p>
 !exit
!endif

!if $mod=$empty
 <p>
 Pour travailler sur un module, vous devez d'abord le cr�er. Pour cr�er un
 nouveau module, veuillez cliquer sur les liens appropri�s et puis remplir les
 formulaires � cet effet.
 </p>
!endif
<p>
Veuillez consulter
 <a target="wims_help" href="wims.cgi?module=help/wimsdoc.en">$wims_name_doctec</a>
 afin de conna�tre la structure d'un module WIMS ainsi que la syntaxe et
 le format de son contenu.
</p><p>
Vous trouverez ici la
<a target="wims_help" href="wims.cgi?module=adm/createxo&+cmd=help&+special_parm=oef&+session=$wims_session">$wims_name_docoef</a>
</p>
<p>
Il est possible de copier les sources d'un module pr�sent sur le serveur dans le compte modtool. Pour cela, il suffit d'aller chercher le module par le moteur de recherche : un lien <span class="wims_code_words text_icon_modify">$wims_name_modify</span> apparait dans le menu du module (ce lien n'apparait que lorsqu'on est connect� � son compte modtool).  
</p>
<p>
Indications sp�ciales&nbsp;:
</p><ol>
<li>Pour debogger un fichier, vous pouvez placer une ligne
<pre>
$!debug ...
</pre>
dans ce fichier, o� ... peut �tre n'importe quelle cha�ne de texte. Quand
vous testez votre module, l'ex�cution va stopper � cette ligne et le
contenu de ... vous sera montr�. Si
... inclut des variables, ces derni�res sont substitu�es selon les r�gles
habituelles de substitution de variables de WIMS.

</li></ol>

:end
