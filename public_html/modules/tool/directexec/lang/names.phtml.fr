!set lang_exists=yes
!set name_remark_jsxgraph=La bo�te doit s'appeler <tt class="wims_code_words">jsxbox</tt> :\
<pre>var board = JXG.JSXGraph.initBoard('jsxbox');</pre>

!set name_non_execute = n'a pas pu ex�cuter votre code
!set name_msg=Message d'erreur de
!set name_empty=Vider
!set name_execute=Ex�cuter
!set name_execute_by=Faire ex�cuter par
!set name_code_to_execute=Code � ex�cuter�:

!set name_doc=Documentation

!set name_restrict=L'entr�e ou la sortie de toute ex�cution est limit�e � 16K. Pour des raisons\
  �videntes de s�curit�, il n'y a pas d'acc�s au r�seau, l'�criture sur des\
  fichiers est limit�e au r�pertoire actuel et peut m�me �tre totalement\
  bloqu�e et le temps d'ex�cution est limit� � quelques secondes.

!set name_nosecure=!nosubst D'autres logiciels sont peut-�tre accessibles sur d'autres serveurs wims\
  comme <span class="tt">$swname_comp</span>.

!set name_c=Note sp�ciale pour l'interface C : le point d'entr�e est \
<span class="tt">int test(void)</span>, mais pas <span class="tt">main()</span>&nbsp;!

!set name_canvasdraw_example=Vous pouvez copier l'exemple dans la fen�tre principale\
  et le modifier.

!!set name_remark_tikz=Pour obtenir la conversion en tikz d'un code flydraw,\
  entrer le code flydraw pr�c�d� de la ligne\
  <pre>new sizex, sizey</pre>\
  o� <span class="tt">sizex</span> et <span class="tt">sizey</span> sont les tailles\
  en pixel du dessin.
!set name_floathelp=Voir aussi, l'outil
