2_shape:Figure plane
2d_arrays:Tableaux 2D
3_shape:Figure dans l'espace
abelian_group:Groupe ab�lien
absolute_frequency:Effectif
absolute_value:Valeur absolue
abstract_algebra:Alg�bre abstraite
acceleration:Acc�leration
acid_base_reaction:R�action acido-basique
actions:Action
activation_energy:�nergie d'activation
active_site:site actif
adaptative_immunity:Immunit� adaptative
addition:Addition
addition_reaction:R�action d'addition
affine_function:Fonction affine
affine_geometry:G�om�trie affine
algebra:Alg�bre
algebra_geometry:Alg�bre g�om�trique
algebraic_fraction:Fraction rationnelle
algebraic_geometry:G�om�trie alg�brique
algorithmics:Algorithmique
altitude:Hauteur
american_civilisation:Civilisation am�ricaine
amino_acid:Acide amin�
analysis:Analyse
analytic_geometry:G�om�trie analytique
ancient_greek:Grec ancien
anesthetic_drug:Anesth�sique
angle_bisector:Bissectrice
angles:Angle
animal_cell:Cellule animale
animal_physiology:Physiologie animale
animal_tissue:Tissu animal
antibiotherapy:Antibioth�rapie
antibody:Anticorps
antibody_diversity:Diversit� des anticorps
antigen:Antig�ne
antiseptic_drug:Antiseptique
area:Aire
argument:Argument
arithmetic:Arithm�tique
arithmetic_sequence:Suite arithm�tique
arrays:Tableaux
arts:Disciplines artistiques
association_rate:Taux de liaison
astronomy:Astronomie
asymptote:Asymptote
atom:Atome
automation:Automatisme
bacteria:Bact�rie
barycenter:Barycentre
basis:Base
basis_change:Changement de bases
between_variance:Variance inter
bezout_algorithm:Algorithme de Bezout
bilinear_algebra:Alg�bre bilin�aire
binomial:Bin�me
binomial_distribution:Loi binomiale
binomial_test:Test binomial
biochemistry:Biochimie
biodiversity:Biodiversit�
bioinformatics:Bio-Informatique
biology:Biologie
biotechnology:Biotechnologie
bodenstein_steady_state:AEQS
body_system:Le corps humain
bonds:Obligation
botany:Botanique
bound:Borne
boxplot:Boite � moustaches
business_english:Anglais d'affaire
butterfly:Papillon
calculation:Calcul
calculus:Calcul diff�rentiel
calendar:Calendrier
canonical_form:Forme canonique
capacitor:Condensateur
catalysis:Catalyse
cell_biology:Biologie cellulaire
cell_cycle:Cycle cellulaire
cell_death:Mort cellulaire
cell_differentiation:Diff�renciation cellulaire
cell_division:Division cellulaire
cercl_a1:CERCL_A1
cercl_a2:CERCL_A2
cercl_b1:CERCL_B1
cercl_b2:CERCL_B2
cercl_c1:CERCL_C1
cercl_c2:CERCL_C2
characteristic_polynomial:Polyn�me caract�ristique
chemical_bond:Liaison chimique
chemical_equilibrium:Equilibre chimique
chemical_kinetics:Cin�tique chimique
chemical_nomenclature:Nomenclature en chimie
chemical_polarity:Polarit� en chimie
chemical_reaction:R�action chimique
chemical_synthesis:Synth�se chimique
chemistry:Chimie
chessgame:Jeux d'�chec
chisquare_test:Test du khi-2
chromatin:Chromatine
chromosome:Chromosome
cinematics:Cin�matique
circle:Cercle
circle_equation:�quation de cercle
circuit_law:Loi de Kirchhoff
circulation:Circulation
civic_education:�ducation civique
class_i:Classe I
class_ii:Classe II
classical_antiquity:Antiquit� classique
climate:Climat
clock:Heure
clonal_selection:S�lection clonale
clthm:Th�or�me central limite
clustering:Classification
code:Code
coding:Th�orie du codage
codon:Codon
collections:Collection
color:Couleur
combination:Combinaison
combinatorics:Analyse combinatoire
combustion:Combustion
common_ancestor:Anc�tre commun
complement_system:Syst�me du compl�ment
complementary_subspace:Sous-espace suppl�mentaire
complex_analysis:Analyse complexe
complex_number:Nombre complexe
complex_plane:Plan complexe
complexation:Complexation
complexity:Complexit�
composite_types:Type composite
cond_instruction:Instruction conditionnelle
conditional_frequency:Fr�quence conditionnelle
conditional_probability:Probabilit� conditionnelle
cone:Cone
confidence_interval:Intervalle de confiance
congruence:Congruence
conics:Conique
connective_tissue:Tissu conjonctif
contingency_table:Tableau de contingence
continued_fraction:Fraction continue
continuity:Continuit�
continuous_probability_distribution:Loi de probabilit� continue
contraposition:Contrapos�e
contrast_agent:Agent de contraste
contribution_variance:Contribution � la variance
convergence:Convergence
conversion:Conversion
convexity:Convexit�
cooperativity:Coop�rativit�
coordinates:Coordonn�e
coordination:Coordination
corporate_finance:Finances de l'entreprise
correlation:Corr�lation
covariance:Covariance
cram:Repr�sentation de Cram
critical_point:Point critique
cross_product:Produit vectoriel
cryptology:Cryptologie
crystallography:Cristallographie
cube:Cube
cubic_crystal_system: Syst�me cubique
cumulative_distribution:Fonction de r�partition
cumulative_frequency:Effectif cumul�
curl:Rotationnel
curve_fitting:Ajustement de courbe
curves:Courbe
cyclic_code:Code cyclique
cyclic_group:Groupe cyclique
cylinder:Cylindre
cytokine:Cytokine
data_analysis:Analyse des donn�es
data_gestion:Gestion de donn�es
data_structures:Structures de donn�es
decile:D�cile
decimals:Nombres d�cimaux,Nombre d�cimal
definite_integral:Int�grale d�finie
dendritic_cell:Cellule dendritique
density:Densit� de probabilit�
derivative:D�riv�e
descriptive_statistics:Statistique descriptive
determinant:D�terminant
development_of_lymphocytes:D�veloppement des lymphocytes
diabete_drug:Diab�te
diacritical_mark:Signe diacritique
diagonalization:Diagonalisation
diastereoisomerism:Diast�r�oisom�rie
differentiability:Diff�rentiabilit�
differential_equation:�quation diff�rentielle
differential_geometry:G�om�trie diff�rentielle
differential_system:Syst�me diff�rentiel
dimension:Dimension
dimensional_analysis:Analyse dimensionnelle
diophantine_equations:�quation diophantienne
dirac_delta_function:Distribution de Dirac
direction:Coefficient directeur
discrete_logarithm:Logarithme discret
discrete_mathematics:Math�matiques discr�tes
discrete_probability_distribution:Loi de probabilit� discr�te
distance:Distance
distance_method:M�thode de distance
divergence:Divergence
diversity:Diversit�
divisibility:Divisibilit�
division:Division
dna:ADN
dna_polymerase:ADN polym�rase
dna_repair:R�paration de l'ADN
dna_replication:R�plication de l'ADN
domain:Ensemble de d�finition
dose:Dosage
dynamic_system:Syst�me dynamique
dynamics:Dynamique
earth_sciences:Sciences de la terre
ecogestion:Ecogestion
ecology:�cologie
economics:Sciences �conomiques
electrical_engineering:�lectrotechnique
electricity:�lectricit�
electro_chemistry:�lectrochimie
electrode:�lectrode
electronics:�lectronique
electrophoresis:�lectrophor�sis
electrostatics:�lectrostatique
elementary_algebra:Alg�bre �l�mentaire
elementary_arithmetic:Arithm�tique �l�mentaire
elementary_calculus:Calcul �l�mentaire
elementary_geometry:G�om�trie �l�mentaire
elementary_mathematics:Math�matique �l�mentaire
elementary_reaction:R�action �l�mentaire
elimination_reaction:R�action d'�limination
ellipse:Ellipse
emphasis:Accentuation
enantiomerism:�nantiom�rie
encryption:Chiffrement
endomorphism_reduction:R�duction d'endomorphismes
energy:Energie
english:Anglais
english_grammar:Grammaire anglaise
english_vocabulary:Vocabulaire anglais
enthalpy:Enthalpie
entropy:Entropie
enzyme:Enzyme
epithelial_tissue:Tissu �pith�lial
equation_systems:Syst�me d'�quations
equations:�quation
error_correcting_codes:Code correcteur d'erreurs
error_detection:D�tection d'erreurs
estimation:Estimation
ethology:�thologie
etymology:�tymologie
eucaryote:Eucaryote
euclidean_algorithm:Algorithme d'Euclide
euclidean_cloud:Nuage euclidien
euclidean_geometry:G�om�trie euclidienne
eulerian_graph:Graphe eul�rien
events:�v�nement
evolution:�volution
expectation:Esp�rance
exponent:Exposant
exponential:Exponentielle
exponential_distribution:Loi exponentielle
extremum:Extremum
factorial:Factorielle
factorization:Factorisation
fc_receptor:R�cepteur Fc
field:Corps
financial_analysis:Analyse financi�re
financial_market:March�s financiers
financial_mathematics:Math�matiques financi�res
finite_field:Corps fini
finite_group:Groupe fini
finnish:Finnois
flow_cytometry:Cytom�trie de flux
fluid_mechanics:M�canique des fluides et des liquides
force:Force
fourier_series:S�rie de Fourier
fourier_transform:Transform�e de Fourier
fraction:Fraction
french:Fran�ais
french_conjugation:Conjugaison francaise
french_grammar:Grammaire fran�aise
french_reading:Lecture en fran�ais
french_spelling:Orthographe fran�aise
french_vocabulary:Vocabulaire fran�ais
frieze:Frise
function_variation:Tableau de variation
functional_group:Fonction chimique
functions:Fonction
games:Jeux
gas_mechanics:M�canique des gaz
gauss_algorithm:Algorithme de Gauss
gaz_mechanics:Gaz
gcd_lcm:PGCD et PPCM
gene_expression:Expression g�nique
gene_rearrangement:R�arrangement des g�nes
genetics:G�n�tique
geography:G�ographie
geology:G�ologie
geometric_distribution: Loi g�om�trique
geometric_sequence:Suite g�om�trique
geometric_vocabulary:Vocabulaire g�om�trique
geometry:G�om�trie
german:Allemand
german_conjugation:Conjugaison allemande
german_grammar:Grammaire allemande
german_vocabulary:Vocabulaire allemand
gestion:Sciences de la gestion
gibbs_free_energy:Enthalpie libre
goldbach:Goldbach
gradient:Gradient
grammar:Grammaire
graph:Th�orie des graphes
graph_coloring:Coloration
graphing:Graphe de fonctions
gravity:Gravitation
greek:Grec
group:Groupe
group_action:Action de groupes
group_theory:Th�orie des groupes
habitat:Habitat
hamming:Distance de Hamming
heat_exchange:�changes thermiques
hematopoiesis:H�matopo��se
hierarchical_cluster_analysis:Classification ascendante hi�rarchique
histocompatibility:Histocompatibilit�
histogram:Histogramme
histology:Histologie
history:Histoire
homography:Homographie
homology:Homology
homonymy:Homonomie
homothety:Homoth�tie
hyperbola:Hyperbole
hyperplane:Hyperplan
ideal_gas:Gaz parfait
image:Image
immunoglobulines:Immunoglobuline
immunology:Immunologie
imperative_programming:Programmation imp�rative
implication:Implication
independent_events:Ind�pendance d'�v�nements
independent_random_variables:Ind�pendance de variables al�atoires
inductance:Inductance
industrial_sciences:Sciences et techniques industrielles
inequalities:In�galit�
inequations:In�quation
inertia_principle:Principe d'inertie
infection:Infection
inferential_statistics:Statistique inf�rentielle
infonet:Technologies de l'Information et de la Communication
informatics:Informatique
information_theory:Th�orie de l'information
inhibitor:Inhibiteur
injectivity:Injectivit�
innate_immunity:Immunit� inn�e
inorganic_chemistry:Chimie min�rale et inorganique
integers:Entier
integral:Int�grale
integrated_rate_law:�quation horaire
integration:Calcul int�gral
interferon:Interf�ron
interpolation:Interpolation
intervals:Intervalle
invasive_species:Esp�ce envahissante
inverse_function:Fonction r�ciproque
ionisation:Ionisation
ioput:Entr�es-sorties
irreducible_fraction:Fraction irr�ductible
isomers:Isom�re
isometries:Isom�trie
kepler:Kepler
kinetic_mechanism:M�canisme cin�tique
kinetics:Cin�tique
language:Langues
laplace_distribution:Loi de Laplace
latin:Latin
length:Longueur
leucocyte:Leucocyte
level_set:Lignes de niveau
lewis:Repr�sentation de Lewis
lexical_field:Champ lexical
life_sciences:Sciences de la vie
limit:Limite
line:Droite
line_equation:�quation de droite
line_integral:Int�grale curviligne
linear_algebra:Alg�bre lin�aire
linear_differential_equation:�quation diff�rentielle lin�aire
linear_function:Fonction lin�aire
linear_maps:Application lin�aire
linear_optimisation:Optimisation lin�aire
linear_system:Syst�me lin�aire
lines:Droite
linguistics:Linguistique
lissajous:Lissajous
listening:�coute
literal_calculation:Calcul litt�ral
literature:Litt�rature
logarithm:Logarithme
logic:Logique
logic_recreation:R�cr�ation logique
loops:Boucle
lymphocyte:Lymphocyte
lymphoid_organ:Organe lympho�de
macrophage:Macrophage
magnetism:Magn�tisme
management:Gestion de l'entreprise
maps:Application
markov:Markov
markov_chains:Cha�nes de Markov
masse:Masse
math_skill:Comp�tence
math_symbols:Symbole math�matique
mathematics:Math�matique
matrix:Matrice
maximum_flow:Flots maximaux
maximum_parsimony:Maximum de parcimonie
mean:Moyenne
measurement:Grandeur et mesure
mechanics:M�canique g�n�rale
medecine:M�decine
median:M�diane
median_line:Droite m�diane
medication:M�dicament
medicinal_chemistry:Chimie th�rapeutique
meiosis:M�iose
membrane:Membrane
mental_calculation:Calcul mental
metabolism:M�tabolisme cellulaire
methodical_calculation:Calcul r�fl�chi
methodology:M�thodologie
mhc:CMH
microbe:Microbe
microbiology:Microbiologie
midpoint:Milieu
mitosis:Mitose
modelling:Mod�lisation
modular_arithmetic:Arithm�tique modulaire
module:Module
molecular_biology:Biologie mol�culaire
molecular_orbital:Orbitale mol�culaire
molecule:Mol�cule
monophyletic_group:Groupe monophyl�tique
monotonic_function:Fonction monotone
month_week:Mois et semaines
moon:Lune
multiplication:Multiplication
multivariable_function:Fonction de plusieurs variables
muscle_tissue:Tissu musculaire
music:Musique
mycology:Mycologie
negation:N�gation
nernst_equation:�quation de Nernst
nernst_potential:Potentiel de Nernst
nervous_tissue:Tissu nerveux
neurosciences:Neurosciences
neutrophil:Neutrophile
newman:Repr�sentation de Newman
nk_cell:Cellules NK
normal_distribution:Loi normale
normal_vector:Vecteur normal
nsaids:AINS
nucleic_acid:Acide nucl�ique
nucleotide:Nucl�otide
number:Nombre
number_line:Droite gradu�e
number_theory:Th�orie des nombres
number_writing:�criture des nombres
numbering_system:Syst�me de num�ration
numeration:Num�ration
numerical_analysis:Analyse num�rique
numerical_integration:Int�gration num�rique
numerical_method:M�thode num�rique
ode:EDO
ohm_law:Loi d'Ohm
oper_prec:Ordre de priorit�
operation:Op�ration
operational_research:Recherche op�rationnelle
optics:Optique
order:Ordre
organelle:Organite
organic_chemistry:Chimie Organique
oscillators:Oscillateur
oxidation_number:Nombre d'oxydation
oxydoreduction:Oxydor�duction
pain_drug:M�dicaments de la douleur
parabola:Parabole
parallel:Parall�le
parallelogram:Parall�logramme
parametric_curves:Courbe param�trique
parametric_surfaces:Surface param�trique
partial_derivative:D�riv�e partielle
passive_form:Forme passive
past_tenses:Temps du pass�
pbsolving:R�solution de probl�mes
peptide:Peptide
percentage:Pourcentage
percents:Pourcent
perimeter:P�rim�tre
periodic_table:Classification chimique
permutation:Permutation
perpendicular_bisector:M�diatrice
pert:PERT
phagocytosis:Phagocytose
pharmacology:Pharmacologie
pharmacy:Pharmacie
pharmocophore:Pharmocophore
phase_changes:Changements de phase
phi2:Carr� moyen de contingence
phonetics:Phon�tique
photosynthesis:Photosynth�se
phylo_homology:Homology
phylogenetic_tree:Arbre phylog�n�tique
phylogenetics:Phylog�nie
physical_chemistry:Chimie physique
physical_education:�ducation physique
physics:Physique
physiology:Physiologie
plane:Plan
plane_equation:�quation de plan
plane_section:Section plane
planet:Plan�te
planetology:Plan�tologie
plant:V�g�taux
plant_cell:Cellule v�g�tale
poisson_distribution:Loi de Poisson
polar_curves:Courbe polaire
polygons:Polygone
polyhedron:Poly�dre
polynomials:Polyn�me
population_genetics:G�n�tique des populations
potential:Potentiel
pourbaix_diagram:Diagramme de Pourbaix
power:Puissance
power_series:S�rie enti�re
pre_algebra:Alg�bre �l�mentaire
pre_calculus:Analyse �l�mentaire
precipitation:Pr�cipitation
prediction_interval:Intervalle de fluctuation
preimage:Ant�c�dent
pression:Pression
primes:Nombre premier
primitive:Primitive
priority_rule:R�gle de priorit�
prism:Prisme
prob_graph:Graphe probabiliste
probability:Probabilit�
probability_distribution:Loi de probabilit�
probability_generating_function:Fonction g�n�ratrice
probability_space:Espace probabilis�
procedures:Proc�dure
process_control:Conduites de proc�d�s
process_engineering:G�nie des proc�d�s
production_management:Maintenance des �quipements
prog_functions:Fonctions en programmation
programming:Programmation
pronunciation:Prononciation
proportionality:Proportionnalit�
protein:Prot�ine
protractor:Rapporteur
pyramid:Pyramide
pythagore:Th�or�me de Pythagore
python:Langage Python
qhse:QHSE
quadratic_form:Forme quadratique
quadrilateral:Quadrilat�re
quality_control:Contr�le qualit�
quantifier:Quantificateur
quantile:Quantile
quartile:Quartile
quizz:Automatisme
quotient_group:Groupe quotient
radiation:Radiation
random:Hasard
random_variable:Variable al�atoire
random_vector:Vecteur al�atoire
range_kernel:Image et noyau
rank:Rang
rate_constant:Constante de vitesse
rate_law:Loi de vitesse
ratio:Ratio
rational_number:Nombre rationnel
ray_optics:Optique g�om�trique
reaction_order:Ordre de r�action
reaction_rate:Vitesse de r�action
reading:Lecture
real_function:Fonction r�elle
real_number:Nombre r�el
reasoning:Raisonnement
receptor:R�cepteur
rectangles:Rectangle
recurrence_relation:Relation de r�currence
recursive_functions:Fonction r�cursive
reference_function:Fonction de r�f�rence
regression:R�gression
relative_number:Nombre relatif
relativity:Relativit�
resistance:R�sistance
ribosome:Ribosome
ring:Anneaux
riskreturn:Risque et rentabilit�
rna:ARN
rna_transcription:Transcription en g�n�tique
rna_translation:Traduction g�n�tique
road_traffic_safety:S�curit� routi�re
root_tree:Arbre racin�
roots:Racine
rotation:Rotation
ruler_and_compass:R�gle et compas
rv_convergence:Convergence de variables al�atoires
sampling:�chantillonnage
sampling_distribution:Distribution d'�chantillonnage
satellite:Satellite
scalar_product:Produit scalaire
scheduling:Ordonnancement
sciences:Sciences
scientific_notation:Notation scientifique
selection_of_lymphocytes:S�lection des lymphocytes
sense:Sens
sentence_stress:Accent tonique
sequence:Suite num�rique
series:S�rie
set_theory:Th�orie des ensembles
sets:Ensemble
signal_processing:Th�orie du signal
signaling_pathway:Signalisation intracellulaire
signdigits:Chiffre significatif
similarities:Similitude
simplex_method:M�thode du simplex
simulation:Simulation
skill_calculate:Comp�tence Calculer
skill_communicate:Comp�tence Communiquer
skill_modelling:Comp�tence Mod�liser
skill_reasoning:Comp�tence Raisonner
skill_represent:Comp�tence Repr�senter
skill_search:Comp�tence Chercher
solar_system:Syst�me solaire
solid_geometry:G�om�trie dans l'espace
solubility:Solubilit�
solutions:Chimie des solutions
spatial_skill:Rep�rage spatial
speaking:Langues parl�es
specificity:Sp�cificit�
spelling:Orthographe
sphere:Sph�re
sports:Sport
spreadsheet:Tableur
squareroot:Racine carr�e
standard_deviation:Ecart-type
static_register:Registre soutenu
statistical_test:Test d'hypoth�ses
statistics:Statistique
stereochemistry:St�r�ochimie
stereogenic_center:Centre st�r�og�ne
steroid:St�roide
stokes_thm:Th�or�me de Stokes
struct:Enregistrement
student_test:Test de Student
subst_integral:Int�gration par changement de variable
substitution_reaction:R�action de substitution
subtraction:Soustraction
supervision:Supervision
surfaces:Surface
surjectivity:Surjectivit�
sustainable_dev:D�veloppement durable
symmetric_group:Groupe sym�trique
symmetry:Sym�trie
t_cell_receptor:R�cepteur des cellules T
tangent:Tangente
tangent_plane:Plan tangent
tax:Fiscalit�
taylor_expansion:D�veloppement limit�
taylor_series:S�rie de Taylor
technical_language:Langue technique
tensor_field:
test:Test statistique
thales:Th�or�me de Thal�s
thermochemistry:Thermochimie
thermodynamics:Thermodynamique
threatened_species:Esp�ce menac�e
tiling:Pavage
time:Temps
timeline:Frise chronologique
titration:Titration
topology:Topologie
toxin:Toxine
trajectory:Trajectoire
transformation_group:Groupes de transformation
transition_matrix:Matrice de transition
translation:Translation
tree_topology:Topologie d'arbres
triangles:Triangle
trigonometric_circle:Cercle trigonom�trique
trigonometric_functions:Fonction trigonom�trique
trigonometry:Trigonom�trie
trinomial:Trin�me
truth_table:Tables de v�rit�
uniform_distribution:Loi uniforme
units_tests:Test unitaire
unroot_tree:Arbre non racin�
upper_bound:Majorant
vaccine:Vaccin
variables:Variables en programmation
variance:Variance
variation_coefficient:Coefficient de variation
vector_space:Espace vectoriel
vectorial_analysis:Analyse vectorielle
vectorial_field:Champ de vecteurs
vectorial_geometry:G�om�trie vectorielle
vectors:Vecteur
vegetal_physiology:Physiologie v�g�tale
vegetal_tissue:Tissu v�g�tal
velocity:Vitesse
vibration:Acoustique
virology:Virologie
virus:Virus
vocabulary:Vocabulaire
voltage:Pression
volume:Volume
ward_method:M�thode de Ward
weaver:Tissage
within_variance:Variance intra
writing:Langues �crites
written_english:Anglais �crit
zdomain:Autres domaines
zoology:Zoologie
