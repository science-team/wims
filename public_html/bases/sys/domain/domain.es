2_shape:Figura en dimensi�n 2
2d_arrays:Matrices 2D
3_shape:Figura en dimensi�n 3
abelian_group:Grupo abeliano
absolute_frequency:Frecuencia absoluta
absolute_value:Valor absoluto
abstract_algebra:�lgebra abstracta
acceleration:Aceleraci�n
acid_base_reaction:Reacci�n �cido base
actions:Acciones
activation_energy:Energ�a de activaci�n
active_site:Sitio activo
adaptative_immunity:Inmunidad adaptativa
addition:Adici�n
addition_reaction:Reacci�n de adici�n
affine_function:Funci�n af�n
affine_geometry:Geometr�a af�n
algebra:�lgebra
algebra_geometry:�lgebra geom�trica
algebraic_fraction:Fracci�n algebraica
algebraic_geometry:Geometr�a algebraica
algorithmics:Algoritmia
altitude:Altitud
american_civilisation:Civilizaci�n americana
amino_acid:Amino�cido
analysis:An�lisis
analytic_geometry:Geometr�a anal�tica
ancient_greek:Griego antiguo
anesthetic_drug:F�rmaco anest�sico
angle_bisector:Bisectriz
angles:�ngulos
animal_cell:C�lula animal
animal_physiology:Fisiolog�a animal
animal_tissue:Tejido animal
antibiotherapy:Antibioterapia
antibody:Anticuerpo
antibody_diversity:Diversidad de anticuerpos
antigen:Ant�geno
antiseptic_drug:F�rmaco antis�ptico
area:�rea
argument:Argumento
arithmetic:Aritm�tica
arithmetic_sequence:Progresi�n aritm�tica
arrays:Arreglos
arts:Artes
association_rate:Tasa de asociaci�n
astronomy:Astronom�a
asymptote:As�ntota
atom:�tomo
automation:Automatizaci�n
bacteria:Bacteria
barycenter:Baricentro
basis:Base
basis_change:Cambio de bases
between_variance:Varianza entre grupos
bezout_algorithm:Algoritmo de Bezout
bilinear_algebra:�lgebra bilineal
binomial:Binomio
binomial_distribution:Distribuci�n binomial
binomial_test:Prueba binomial
biochemistry:Bioqu�mica
biodiversity:Biodiversidad
bioinformatics:Bioinform�tica
biology:Biolog�a
biotechnology:Biotecnolog�a
bodenstein_steady_state:Estado estacionario de Bodenstein
body_system:Sistema corporal
bonds:Bonos
botany:Bot�nica
bound:Cota
boxplot:Diagrama de caja
business_english:Ingl�s de negocios
butterfly:Mariposa
calculation:C�lculo
calculus:C�lculo
calendar:Calendario
canonical_form:Forma can�nica
capacitor:Condensador
catalysis:Cat�lisis
cell_biology:Biolog�a celular
cell_cycle:Ciclo celular
cell_death:Muerte celular
cell_differentiation:Diferenciaci�n celular
cell_division:Divisi�n celular
cercl_a1:CERCL_A1
cercl_a2:CERCL_A2
cercl_b1:CERCL_B1
cercl_b2:CERCL_B2
cercl_c1:CERCL_C1
cercl_c2:CERCL_C2
characteristic_polynomial:Polinomio caracter�stico
chemical_bond:Enlace qu�mico
chemical_equilibrium:Equilibrio qu�mico
chemical_kinetics:Cin�tica qu�mica
chemical_nomenclature:Nomenclatura qu�mica
chemical_polarity:Polaridad qu�mica
chemical_reaction:Reacci�n qu�mica
chemical_synthesis:S�ntesis qu�mica
chemistry:Qu�mica
chessgame:Juego de ajedrez
chisquare_test:Prueba ji cuadrado
chromatin:Cromatina
chromosome:Cromosoma
cinematics:Cinem�tica
circle:C�rculo
circle_equation:Ecuaci�n de la circunferencia
circuit_law:Ley de Kirchhoff
circulation:Circulaci�n
civic_education:Educaci�n c�vica
class_i:Clase I
class_ii:Clase II
classical_antiquity:Antig�edad cl�sica
climate:Clima
clock:Reloj
clonal_selection:Selecci�n clonal
clthm:Teorema del l�mite central
clustering:An�lisis de cl�steres
code:C�digo
coding:Codificaci�n
codon:Cod�n
collections:Colecci�n
color:Color
combination:Combinaci�n
combinatorics:Combinatoria
combustion:Combusti�n
common_ancestor:Antepasado com�n
complement_system:Sistema del complemento
complementary_subspace:Subespacio complementario
complex_analysis:An�lisis complejo
complex_number:N�mero complejo
complex_plane:Plano complejo
complexation:Complejizaci�n
complexity:Complejidad
composite_types:Tipos compuestos
cond_instruction:Instrucci�n condicional
conditional_frequency:Frecuencia condicional
conditional_probability:Probabilidad condicional
cone:Cono
confidence_interval:Intervalo de confianza
congruence:Congruencia
conics:C�nica
connective_tissue:Tejido conectivo
contingency_table:Tabla de contingencia
continued_fraction:Fracci�n continua
continuity:Continuidad
continuous_probability_distribution:Distribuci�n de probabilidad continua
contraposition:Contraposici�n,Contrarrec�proca
contrast_agent:Agente de contraste
contribution_variance:Contribuci�n a la varianza
convergence:Convergencia
conversion:Conversi�n
convexity:Convexidad
cooperativity:Cooperatividad
coordinates:Coordenada
coordination:Coordinaci�n
corporate_finance:Finanzas corporativas
correlation:Correlaci�n
covariance:Covarianza
cram:Representaci�n de Cram
critical_point:Punto cr�tico
cross_product:Producto cruz
cryptology:Criptolog�a
crystallography:Cristalograf�a
cube:Cubo
cubic_crystal_system:Sistema cristalino c�bico
cumulative_distribution:Funci�n de distribuci�n acumulada
cumulative_frequency:Frecuencia acumulada
curl:Rotacional
curve_fitting:Ajuste de curvas
curves:Curva
cyclic_code:C�digo c�clico
cyclic_group:Grupo c�clico
cylinder:Cilindro
cytokine:Citocina
data_analysis:An�lisis de datos
data_gestion:Gesti�n de datos
data_structures:Estructuras de datos
decile:Decil
decimals:N�meros decimales, N�mero decimal
definite_integral:Integral definida
dendritic_cell:C�lula dendr�tica
density:Densidad
derivative:Derivada
descriptive_statistics:Estad�stica descriptiva
determinant:Determinante
development_of_lymphocytes:Desarrollo de linfocitos
diabete_drug:F�rmaco para la diabetes
diacritical_mark:Signo diacr�tico
diagonalization:Diagonalizaci�n
diastereoisomerism:Diastereoisomerismo
differentiability:Diferenciabilidad
differential_equation:Ecuaci�n diferencial
differential_geometry:Geometr�a diferencial
differential_system:Sistema de ecuaciones diferenciales
differentiation:Diferenciaci�n
dimension:Dimensi�n
dimensional_analysis:An�lisis dimensional
diophantine_equations:Ecuaciones diof�nticas
dirac_delta_function:Delta de Dirac
direction:Direcci�n
discrete_logarithm:Logaritmo discreto
discrete_mathematics:Matem�ticas discretas
discrete_probability_distribution:Distribuci�n de probabilidad discreta
distance:Distancia
distance_method:M�todo de distancias
divergence:Divergencia
diversity:Diversidad
divisibility:Divisibilidad
division:Divisi�n
dna:ADN
dna_polymerase:ADN polimerasa
dna_repair:Reparaci�n del ADN
dna_replication:Replicaci�n del ADN
domain:Dominio
dose:Dosis
dynamic_system:Sistema din�mico
dynamics:Din�mica
earth_sciences:Ciencias de la Tierra
ecogestion:Gesti�n ecol�gica
ecology:Ecolog�a
economics:Econom�a
electrical_engineering:Ingenier�a el�ctrica
electricity:Electricidad
electro_chemistry:Electroqu�mica
electrode:Electrodo
electronics:Electr�nica
electrophoresis:Electroforesis
electrostatics:Electrost�tica
elementary_algebra:�lgebra elemental
elementary_arithmetic:Aritm�tica elemental
elementary_calculus:C�lculo elemental
elementary_geometry:Geometr�a elemental
elementary_mathematics:Matem�ticas elementales
elementary_reaction:Reacci�n elemental
elimination_reaction:Reacci�n de eliminaci�n
ellipse:Elipse
emphasis:�nfasis
enantiomerism:Enantiomerismo
encryption:Encriptaci�n
endomorphism_reduction:Reducci�n de endomorfismo
energy:Energ�a
english:Ingl�s
english_grammar:Gram�tica inglesa
english_vocabulary:Vocabulario ingl�s
enthalpy:Entalp�a
entropy:Entrop�a
enzyme:Enzima
epithelial_tissue:Tejido epitelial
equation_systems:Sistema de ecuaciones
equations:Ecuaci�n
error_correcting_codes:C�digo corrector de errores
error_detection:Detecci�n de errores
estimation:Estimaci�n
ethology:Etolog�a
etymology:Etimolog�a
eucaryote:Eucariota
euclidean_algorithm:Algoritmo de Euclides
euclidean_cloud:Nube euclidiana
euclidean_geometry:Geometr�a euclidiana
eulerian_graph:Grafo euleriano
events:Eventos
evolution:Evoluci�n
expectation:Esperanza
exponent:Exponente
exponential:Exponencial
exponential_distribution:Distribuci�n exponencial
extremum:Extremo
factorial:Factorial
factorization:Factorizaci�n
fc_receptor:Receptor Fc
field:Cuerpo
financial_analysis:An�lisis financiero
financial_market:Mercado financiero
financial_mathematics:Matem�ticas financieras
finite_field:Cuerpo finito
finite_group:Grupo finito
finnish:Finland�s
flow_cytometry:Citometr�a de flujo
fluid_mechanics:Mec�nica de fluidos
force:Fuerza
fourier_series:Series de Fourier
fourier_transform:Transformada de Fourier
fraction:Fracci�n
french:Franc�s
french_conjugation:Conjugaci�n francesa
french_grammar:Gram�tica francesa
french_reading:Lectura en franc�s
french_spelling:Ortograf�a francesa
french_vocabulary:Vocabulario franc�s
frieze:Friso
function_variation:Variaci�n de funciones
functional_group:Grupo funcional
functions:Funci�n
games:Juegos
gas_mechanics:Mec�nica de gases
gauss_algorithm:Algoritmo de Gauss
gaz_mechanics:Mec�nica de gases
gcd_lcm:M�ximo com�n divisor y m�nimo com�n m�ltiplo
gene_expression:Expresi�n g�nica
gene_rearrangement:Reordenamiento g�nico
genetics:Gen�tica
geography:Geograf�a
geology:Geolog�a
geometric_distribution:Distribuci�n geom�trica
geometric_sequence:Progresi�n geom�trica
geometric_vocabulary:Vocabulario geom�trico
geometry:Geometr�a
german:Alem�n
german_conjugation:Conjugaci�n alemana
german_grammar:Gram�tica alemana
german_vocabulary:Vocabulario alem�n
gestion:Gesti�n
gibbs_free_energy:Energ�a libre de Gibbs
goldbach:Goldbach
gradient:Gradiente
grammar:Gram�tica
graph:Teor�a de grafos
graph_coloring:Coloraci�n de grafos
graphing:Gr�fico de funciones
gravity:Gravedad
greek:Griego
group:Grupo
group_action:Acci�n de grupo
group_theory:Teor�a de grupos
habitat:H�bitat
hamming:Distancia de Hamming
heat_exchange:Intercambio de calor
hematopoiesis:Hematopoyesis
hierarchical_cluster_analysis:An�lisis de cl�steres jer�rquicos
histocompatibility:Histocompatibilidad
histogram:Histograma
histology:Histolog�a
history:Historia
homography:Homograf�a
homology:Homolog�a
homonymy:Homonimia
homothety:Homotecia
hyperbola:Hip�rbola
hyperplane:Hiperplano
ideal_gas:Gas ideal
image:Imagen
immunoglobulines:Inmunoglobulinas
immunology:Inmunolog�a
imperative_programming:Programaci�n imperativa
implication:Implicancia,Implicaci�n
independent_events:Eventos independientes
independent_random_variables:Variables aleatorias independientes
inductance:Inductancia
industrial_sciences:Ciencias e Ingenier�as Industriales
inequalities:Desigualdad
inequations:Inecuaci�n
inertia_principle:Principio de inercia
infection:Infecci�n
inferential_statistics:Estad�stica inferencial
infonet:Tecnolog�as de la informaci�n y la comunicaci�n
informatics:Inform�tica
information_theory:Teor�a de la informaci�n
inhibitor:Inhibidor
injectivity:Inyectividad
innate_immunity:Inmunidad innata
inorganic_chemistry:Qu�mica inorg�nica
integers:N�meros enteros,Enteros
integral:Integral
integrated_rate_law:Ley de velocidad integrada
integration:C�lculo integral
interferon:Interfer�n
interpolation:Interpolaci�n
intervals:Intervalo
invasive_species:Especies invasoras
inverse_function:Funci�n inversa
ionisation:Ionizaci�n
ioput:Entrada/Salida
irreducible_fraction:Fracci�n irreducible
isomers:Is�meros
isometries:Isometr�a
kepler:Kepler
kinetic_mechanism:Mecanismo cin�tico
kinetics:Cin�tica
language:Lenguaje
laplace_distribution:Distribuci�n de Laplace
laplace_transforms:Transformada de Laplace
latin:Lat�n
length:Longitud
leucocyte:Leucocito
level_set:Conjunto de nivel
lewis:Representaci�n de Lewis
lexical_field:Campo l�xico
life_sciences:Sciences de la vie
limit:L�mite
line:Recta
line_equation:Ecuaci�n de la recta
line_integral:Integral de l�nea
linear_algebra:�lgebra lineal
linear_differential_equation:Ecuaci�n diferencial lineal
linear_function:Funci�n lineal
linear_maps:Aplicaci�n lineal
linear_optimisation:Optimizaci�n lineal
linear_system:Sistema lineal
lines:Rectas
linguistics:Ling��stica
lissajous:Curvas de Lissajous
listening:Comprensi�n auditiva
literal_calculation:C�lculo literal
literature:Literatura
logarithm:Logaritmo
logic:L�gica
logic_recreation:Recreaciones l�gicas
loops:Bucles
lymphocyte:Linfocito
lymphoid_organ:�rgano linfoide
macrophage:Macr�fago
magnetism:Magnetismo
management:Gesti�n
maps:Mapas
markov:Markov
markov_chains:Cadenas de Markov
masse:Masa
math_skill:Habilidad matem�tica
math_symbols:S�mbolos matem�ticos
mathematics:Matem�ticas
matrix:Matriz
maximum_flow:Flujo m�ximo
maximum_parsimony:M�xima parsimonia
mean:Media
measurement:Medici�n
mechanics:Mec�nica
medecine:Medicina
median:Mediana
median_line:L�nea mediana
medication:Medicaci�n
medicinal_chemistry:Qu�mica medicinal
meiosis:Meiosis
membrane:Membrana
mental_calculation:C�lculo mental
metabolism:Metabolismo
methodical_calculation:C�lculo met�dico
methodology:Metodolog�a
mhc:CMH
microbe:Microorganismo
microbiology:Microbiolog�a
midpoint:Punto medio
misc:Miscel�nea
mitosis:Mitosis
modelling:Modelizaci�n
modular_arithmetic:Aritm�tica modular
module:M�dulo
molecular_biology:Biolog�a molecular
molecular_orbital:Orbital molecular
molecule:Mol�cula
monophyletic_group:Grupo monofil�tico
monotonic_function:Funci�n mon�tona
month_week:Meses y semanas
moon:Luna
multiplication:Multiplicaci�n
multivariable_function:Funci�n de varias variables
muscle_tissue:Tejido muscular
music:M�sica
mycology:Micolog�a
negation:Negaci�n
nervous_tissue:Tejido nervioso
neurosciences:Neurociencias
neutrophil:Neutr�filo
newman:Representaci�n de Newman
nk_cell:C�lula NK
normal_distribution:Distribuci�n normal
normal_vector:Vector normal
nsaids:AINEs
nucleic_acid:�cido nucleico
nucleotide:Nucle�tido
number:N�mero
number_line:Recta num�rica
number_theory:Teor�a de n�meros
number_writing:Escritura de n�meros
numbering_system:Sistema de numeraci�n
numeration:Numeraci�n
numerical_analysis:An�lisis num�rico
numerical_integration:Integraci�n num�rica
numerical_method:M�todos num�ricos
ode:EDO
ohm_law:Ley de Ohm
oper_prec:Precedencia de operadores
operation:Operaci�n
operational_research:Investigaci�n de operaciones
optics:�ptica
order:Orden
organelle:Organelo
organic_chemistry:Qu�mica org�nica
oscillators:Oscilador
oxydoreduction:Oxidaci�n-reducci�n
pain_drug:F�rmaco para el dolor
parabola:Par�bola
parallel:Paralelo
parallelogram:Paralelogramo
parametric_curves:Curvas param�tricas
parametric_surfaces:Superficies param�tricas
partial_derivative:Derivada parcial
passive_form:Forma pasiva
past_tenses:Tiempos pasados
pbsolving:Resoluci�n de problemas
peptide:P�ptido
percentage:Porcentaje
percents:Porcentajes
perimeter:Per�metro
periodic_table:Tabla peri�dica
permutation:Permutaci�n
perpendicular_bisector:Mediatriz
pert:PERT
phagocytosis:Fagocitosis
pharmacology:Farmacolog�a
pharmacy:Farmacia
pharmocophore:Farmac�foro
phase_changes:Cambios de fase
phi2:Coeficiente de contingencia
phonetics:Fon�tica
photosynthesis:Fotos�ntesis
phylo_homology:Homolog�a filogen�tica
phylogenetic_tree:�rbol filogen�tico
phylogenetics:Filogen�tica
phys:F�sica
physical_chemistry:Qu�mica f�sica
physical_education:Educaci�n f�sica
physics:F�sica
physiology:Fisiolog�a
plane:Plano
plane_equation:Ecuaci�n del plano
plane_section:Secci�n plana
planet:Planeta
planetology:Planetolog�a
plant:Planta
plant_cell:C�lula vegetal
poisson_distribution:Distribuci�n de Poisson
polar_curves:Curvas polares
polygons:Pol�gonos
polyhedron:Poliedro
polynomials:Polinomios
population_genetics:Gen�tica de poblaciones
potential:Potencial
power:Potencias
power_series:Series de potencias
pre_algebra:�lgebra elemental
pre_calculus:C�lculo elemental
precipitation:Precipitaci�n 
prediction_interval:Intervalo de predicci�n
preimage:Preimagen
pression:Presi�n
primes:N�meros primos
primitive:Primitiva
priority_rule:Reglas de prioridad
prism:Prisma
prob_graph:Grafo probabil�stico
probability:Probabilidad
probability_distribution:Distribuci�n de probabilidad
probability_generating_function:Funci�n generadora de probabilidad
probability_space:Espacio de probabilidad
procedures:Procedimientos
process_control:Control de procesos
process_engineering:Ingenier�a de procesos
production_management:Gesti�n de la producci�n
prog_functions:Funciones en programaci�n
programming:Programaci�n
pronunciation:Pronunciaci�n
proportionality:Proporcionalidad
protein:Prote�na
protractor:Transportador
pyramid:Pir�mide
pythagore:Pit�goras
python:Python
qhse:QHSE
quadratic_form:Forma cuadr�tica
quadrilateral:Cuadril�tero
quality_control:Control de calidad
quantifier:Cuantificador
quantile:Cuantil
quartile:Cuartil
quizz:Prueba
quotient_group:Grupo cociente
radiation:Radiaci�n
random:Aleatorio
random_variable:Variable aleatoria
random_vector:Vector aleatorio
range_kernel:Rango y n�cleo
rank:Rango
rate_constant:Constante de velocidad
rate_law:Ley de velocidad
ratio:Raz�n
rational_number:N�mero racional
ray_optics:�ptica geom�trica
reaction_order:Orden de reacci�n
reaction_rate:Velocidad de reacci�n
reading:Lectura
real_function:Funci�n real
real_number:N�mero real
reasoning:Razonamiento
receptor:Receptor
rectangles:Rect�ngulos
recurrence_relation:Relaci�n de recurrencia
recursive_functions:Funciones recursivas
reference_function:Funciones de referencia
regression:Regresi�n
relative_number:N�mero relativo
relativity:Relatividad
resistance:Resistencia
ribosome:Ribosoma
ring:Anillo
riskreturn:Riesgo y rentabilidad
rna:ARN
rna_transcription:Transcripci�n del ARN
rna_translation:Traducci�n del ARN
road_traffic_safety:Seguridad vial
root_tree:�rbol enraizado
roots:Ra�ces
rotation:Rotaci�n
ruler_and_compass:Regla y comp�s
rv_convergence:Convergencia de variables aleatorias
sampling:Muestreo
sampling_distribution:Distribuci�n muestral
satellite:Sat�lite
scalar_product:Producto escalar
scheduling:Teor�a de la planificaci�n
sciences:Ciencias
scientific_notation:Notaci�n cient�fica
selection_of_lymphocytes:Selecci�n de linfocitos
sense:Sentido
sentence_stress:Acento oracional
sequence:Sucesiones
series:Series
set_theory:Teor�a de conjuntos
sets:conjuntos
signal_processing:Procesamiento de se�ales
signaling_pathway:V�a de se�alizaci�n intracelular
signdigits:Cifras significativas
similarities:Similitudes
simplex_method:M�todo simplex
simulation:Simulaci�n
skill:Habilidades
skill_calculate:Habilidad de c�lculo
skill_communicate:Habilidad de comunicaci�n
skill_modelling:Habilidad de modelizaci�n
skill_reasoning:Habilidad de razonamiento
skill_represent:Habilidad de representaci�n
skill_search:Habilidad de b�squeda
solar_system:Sistema solar
solid_geometry:Geometr�a en el espacio
solubility:Solubilidad
solutions:Soluciones
spatial_skill:Habilidad espacial
speaking:Expresi�n oral
specificity:Especificidad
spelling:Ortograf�a
sphere:Esfera
sports:Deportes
spreadsheet:Hoja de c�lculo
squareroot:Ra�z cuadrada
standard_deviation:Desviaci�n est�ndar
static_register:Registro est�tico
statistical_test:Prueba estad�stica
statistics:Estad�stica
stereochemistry:Estereoqu�mica
stereogenic_center:Centro estereog�nico
steroid:Esteroides
stokes_thm:Teorema de Stokes
struct:Estructura
student_test:Prueba t de Student
subst_integral:Integraci�n por sustituci�n
substitution_reaction:Reacci�n de sustituci�n
subtraction:Sustracci�n
supervision:Supervisi�n
surfaces:Superficies
surjectivity:Sobreyectividad
sustainable_dev:Desarrollo sostenible
symmetric_group:Grupo sim�trico
symmetry:Simetr�a
t_cell_receptor:Receptor de c�lula T
tangent:Tangente
tangent_plane:Plano tangente
tax:Impuesto
taylor_expansion:Expansi�n de Taylor
taylor_series:Serie de Taylor  
technical_language:Lenguaje t�cnico
tensor_field:Campo tensorial
test:Prueba estad�stica
thales:Tales
thermochemistry:Termoqu�mica
thermodynamics:Termodin�mica
threatened_species:Especies amenazadas
tiling:Teselado
time:Tiempo
timeline:L�nea de tiempo
titration:Titulaci�n
topology:Topolog�a
toxin:Toxina
trajectory:Trayectoria
transformation_group:Grupo de transformaciones
transition_matrix:Matriz de transici�n
translation:Traducci�n
tree_topology:Topolog�a de �rbol
triangles:Tri�ngulos
trigonometric_circle:C�rculo trigonom�trico
trigonometric_functions:Funciones trigonom�tricas
trigonometry:Trigonometr�a
trinomial:Trinomio
truth_table:Tabla de verdad
uniform_distribution:Distribuci�n uniforme
units_tests:Pruebas unitarias
unroot_tree:�rbol no enraizado
upper_bound:Cota superior
vaccine:Vacuna
variables:Variables
variance:Varianza
variation_coefficient:Coeficiente de variaci�n
vector_space:Espacio vectorial
vectorial_analysis:An�lisis vectorial
vectorial_field:Campo vectorial
vectorial_geometry:Geometr�a vectorial
vectors:Vectores
vegetal_physiology:Fisiolog�a vegetal
vegetal_tissue:Tejido vegetal
velocity:Velocidad
vibration:Ac�stica
virology:Virolog�a
virus:Virus
vocabulary:Vocabulario
voltage:Voltaje
volume:Volumen
ward_method:M�todo de Ward
weaver:Representaci�n Weaver
within_variance:Varianza intragrupos
writing:Escritura
written_english:Ingl�s escrito
zdomain:Otros dominios
zoology:Zoolog�a
