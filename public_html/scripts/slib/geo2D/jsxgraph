!!! JsxGraph responsive Version 4 - 05/07/2022
!!! CSS hack padding-bottom instead aspect-ratio because not supported by old browser
!!! works with jsxgraph 1.4.2
!!! options : max-width, min-width, resize (to resize points with the width of the applet),
!!! center, nocssjs
!!! Automatic research of the board's name if not indicated
!!! New option : scroll to add a horizontal scrolling bar (class wims_scrollable)
!!! WARNING : with version 1.4.4 of jsxgraph, javascript updateSize function
!!! is needed even the option resize is not called with this line
!!! slib_board.resizeContainer(theWidth,theHeight);

!if $wims_read_parm!=slib_header
 !goto proc
!endif

slib_require=jsxgraph
slib_author=Bernadette,Perrin-Riou;Gilles, Marbeuf

slib_example=jsxbox brd,[300 x 300,min=200px max=400px],var brd = JXG.JSXGraph.initBoard('jsxbox', {axis:true,originX: 150, originY: 150, unitX: 10, unitY: 10});p1 = brd.create('point', [-2,1]);p2 = brd.create('point', [0,function () { return p1.X();}]);brd.create('line',[p1,p2]);\
j1 brd1,[400x400,min=20% max=60% center resize],var brd1 = JXG.JSXGraph.initBoard('j1',{boundingbox: [-2.5, 6.5, 4, -7.5],axis: true,showCopyright: false,keepAspectRatio:true,moveTarget: document,zoom: {pinchHorizontal: false,pinchVertical: false,pinchSensitivity: 7,min: 0.5,max: 2,wheel: true,needShift: false},pan: {needTwoFingers: true,needShift: false}});var A = brd1.create('point', [2.5, 1],{face:'plus',size:10});var B = brd1.create('point', [-1.25, 0.5],{face:'cross',size:4});var C = brd1.create('point', [-1, -5],{face:'circle',size:4});var el = brd1.create('hyperbola', [A, B, C]);var el2 = brd1.create('circle', [A, B], {strokecolor:'#f0f'});\
j2 brd2,[300x200,max=400px],var brd2 = JXG.JSXGraph.initBoard('j2',{boundingbox: [-6, 2, 6, -3],axis: true,showCopyright: false,zoom: {pinchHorizontal: false,pinchVertical: false,pinchSensitivity: 7,min: 0.5,max: 2,wheel: true,needShift: false},pan:{needTwoFingers: true,needShift:false}});var s = brd2.create('slider',[[-2,-2],[2,-2],[-6,5,6]]);var graph = brd2.create('functiongraph',[function(x){ return Math.sin(Math.pow(x,2));},-2,function(){return s.Value();}]);

!exit

:proc

#### PARAM 1 : ID AND BOARD ####
slib_jsxbox= $(wims_read_parm[1])
!distribute words $slib_jsxbox into slib_jsxbox,slib_board

#### PARAM 2: ONLY SIZES OR SIZES AND OPTIONS ####
slib_resp=$(wims_read_parm[2])

#### PARAM 3:  ####
slib_script= $(wims_read_parm[3..-1])

#### TO DETECT OLD (NO RESPONSIVE) AND NEW VERSION (RESPONSIVE) OF THE SLIB ####
slib_char1=!char 1 of $slib_resp
slib_char2=!char -1 of $slib_resp
!if ($slib_char1=[) and ($slib_char2=])
  slib_jsversion=responsive
  !goto responsive
!else
  slib_jsversion=no_responsive
  !goto no_responsive
!endif

:no_responsive
slib_size=!declosing $slib_resp
slib_size=!replace internal x by , in $slib_size
slib_width=!nospace $(slib_size[1]) px
slib_height=!nospace $(slib_size[2]) px
!goto result

:responsive
slib_resp=!declosing $slib_resp
slib_size=$(slib_resp[1])
slib_size=!replace internal x by , in $slib_size
slib_width=!nospace $(slib_size[1])
slib_height=!nospace $(slib_size[2])
slib_ratio=$[$slib_height*100/$slib_width]

### AUTOMATIC RESEARCH OF BOARD'S NAME ###
!if $slib_board=$empty
  slib_tempbrd=$slib_script
  slib_tempbrd=!replace internal initBoard by $ $@@@@$ $ in $slib_tempbrd
  slib_tempbrd=!replace internal . by $ $ in $slib_tempbrd
  slib_pos=!positionof word @@@@ in $slib_tempbrd
  slib_tempbrd=!replace internal = by  in $slib_tempbrd
  slib_tempbrd=!words2items $slib_tempbrd
  slib_tempbrd=!nonempty item $slib_tempbrd
  slib_itempos=!positionof item @@@@ in $slib_tempbrd
  slib_board=!item $[$slib_itempos-3] of $slib_tempbrd
!endif

### OPTIONS ###
slib_option=$(slib_resp[2])

## scroll ##
!if scroll iswordof $slib_option
  slib_scroll=wims_scrollable
!endif

## center ##
!if center iswordof $slib_option
  slib_css_jsxwrap=margin:.5rem auto;
!else
  slib_css_jsxwrap=
!endif

## min and max width ##
## user must add unit like px or % or ...##
slib_minwidth=!getopt min in $slib_option
slib_maxwidth=!getopt max in $slib_option

!if $(slib_maxwidth) !=$empty
  slib_css_jsxwrap=$slib_css_jsxwrap\
  max-width:$(slib_maxwidth);
!endif
!if $(slib_minwidth) !=$empty
  slib_css_jsxwrap=$slib_css_jsxwrap\
  min-width:$slib_minwidth;
!endif
slib_css_jsxwrap=$slib_css_jsxwrap\
height:auto;

### nocssjs ###
!if nocssjs iswordof $slib_option
  slib_nocssjs=nocssjs
!endif

## resize to resize the points with the width of the screen ##
## REQUIRES THE BOARD'S NAME ##


!goto result

:result

!if $slib_nocssjs=
  slib_out=<link rel="stylesheet" href="scripts/js/jsxgraph/jsxgraph.css">
!endif
!if $slib_jsversion=responsive
  slib_out=$slib_out\
  <style>\
  div.jsxwrap_$slib_jsxbox{$slib_css_jsxwrap}\
  div.resp_$slib_jsxbox{\
    height:0;\
    padding-bottom:$slib_ratio%;\
    box-sizing: border-box;\
    /*width:100%;*/\
  }\
  @media screen and (max-height:500px) {\
    div.resp_$slib_jsxbox{\
      margin:0 auto;\
      width: calc(100vmin - 2em);\
      height: calc(100vmin - 2em);\
      padding:0;\
    }\
  }\
  </style>
!endif
!if $slib_nocssjs=
  slib_out=$slib_out\
  <script src="scripts/js/jsxgraph/jsxgraphcore.js" charset="utf-8"></script>
!endif
!if $slib_jsversion=no_responsive
  slib_out=$slib_out\
  <div class="wims_scrollable jsxwrap">\
    <div id="$slib_jsxbox" class="jxgbox" style="width:$slib_width; height:$slib_height"></div>\
  </div>
!else
  slib_out=$slib_out\
  <div class="$slib_scroll jsxwrap_$slib_jsxbox jsxwrap">\
    <div id="$slib_jsxbox" class="jxgbox resp_$slib_jsxbox"></div>\
    <canvas class="jsxg_placeholder" width="$slib_width" height="1"></canvas>\
  </div>
!endif

slib_out=$slib_out\
<script>\
/*<![CDATA[*/\
$slib_script\
/*]]>*/\
</script>

!if resize iswordof $slib_option
  slib_out=$slib_out\
  <script>\
  document.addEventListener('load', updateSize_$slib_jsxbox);\
  window.addEventListener('resize', updateSize_$slib_jsxbox);\
  window.onload = function() {updateSize_$slib_jsxbox()};\
  function updateSize_$slib_jsxbox() {\
     var myid=document.getElementById("$slib_jsxbox");\
     myid.style.width="";\
     myid.style.height="";\
     var theWidth=myid.getBoundingClientRect().width;\
     var theHeight=myid.getBoundingClientRect().height;\
     /*$slib_board.resizeContainer(theWidth,theHeight);*/\
     if(theWidth < 400){\
      cellSize = theWidth/100;\
      for (elt in $slib_board.objects) {\
        if (JXG.isPoint($slib_board.objects[elt])) {\
          $slib_board.objects[elt].setAttribute({ size: cellSize.toFixed(0) });\
        }\
      }\
    }\
  }\
  </script>
!endif

!if $slib_jsversion=no_responsive
  !reset slib_script slib_size slib_width slib_height slib_jsxbox slib_jsversion
!else
  !reset slib_script slib_size slib_width slib_height slib_jsxbox slib_ratio slib_board slib_jsversion slib_option
!endif
