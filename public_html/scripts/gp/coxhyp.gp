!set slib_header_cox=\
\
\\ Etant donnee une matrice a symetrique definie positive\
\\ Renvoie p, la matrice triangulaire superieure\
\\ a  diagonale positive telle que a == p~*p.\
choleski(a)={\
  my(n=#a,p=matrix(n,n),c);\
  for (i=1,n,\
    p[i,i]=c=sqrt(a[i,i]-sum(j=1,i-1,p[j,i]^2));\
    for (j=i+1,n,\
      p[i,j]=(a[i,j]-sum(k=1,i,p[k,i]*p[k,j]))/c));\
  p\
};\
\
\\ Polynome minimal de 2*cos(Pi/n)\
real_cyclo(n)={\
 if(n==1,return(x+2));\
 my(p=polcyclo(2*n),nn=eulerphi(2*n)/2,res=0,c);\
 forstep(j=nn,0,-1,\
   c = polcoeff(p,nn+j);\
   res += c*x^j;\
   p -= c*x^(nn-j)*(x^2+1)^j);\
 res\
};\
\
\\ p(2*cos(t)) = 2*cos(n*t)\
cheby(n)=2*subst(polchebyshev(n),x,x/2);\
\
\\ m est une matrice de Coxeter\
\\ renvoie les generateurs\
cox_base(m)={\
  my(n=#m,nn=1);\
  for(i=1,n,for(j=i+1,n,if(m[i,j],nn=lcm(nn,m[i,j]))));\
  xx=Mod(x,real_cyclo(nn));\
  module=xx.mod;\
  racine=2*cos(Pi/nn);\
  gram=matrix(n,n,i,j,if(m[i,j],-cos(Pi/m[i,j]),-1));\
  v=vector(n,k,matrix(n,n,i,j,\
    (i==j)+(i==k)*lift(subst(if(m[k,j],cheby(nn/m[k,j]),2),x,xx))));\
  Set(v)\
};\
\
longer(shorter,current,cb)={\
  my(l=List());\
  foreach(current,w,foreach(cb,s,\
    listput(l,simplify((w*s)%module))));;\
  setminus(Set(l),shorter)\
};\
\
\\ m est une matrice de Coxeter. Enumere les l premiers element du groupe W\
\\ de maniere purement combinatoire, probablement lineaire en min(l,#W).\
\
cox_enum(m,l)={\
  my(d=#m,nf0,nf1,nf2,f1,f2,ff1,s,t,u,\
  cd0=vector(l), cd1=vector(l*d), cd2=vector(l*d*(d-1)/2), ce2=vector(l*d*(d-1)/2));\
\
  \\ Creation de la premiere chambre et de son (d-2)-squelette\
  cd0[nf0=1]=vector(d,s,s);\
  for (s = 1, d,\
    cd1[nf1+=1] = vector(d+1, t, if(t>d,s,-1));\
    for (t = 1, s-1,\
      cd2[nf2+=1] = 2*m[s,t]-2;\
      cd1[t][s] = cd1[s][t] = nf2));\
\
  while(f1<nf1 && nf0<l, f1+=1; v1=cd1[f1]; if(s=v1[d+1], \\ face du bord\
\
\\ Nouvelle chambre et ses (d-1)-facettes, manquantes ou pas\
    w0=vector(d); w0[s]=f1; new=vector(d,t,if(t!=s,cd2[v1[t]]));\
    for(t=1, d, if(t!=s, f2=v1[t]; if(new[t],\
    w1=vector(d+1); w1[d+1]=t; w1[s]=f2; w1[t]=-1;\
    cd1[nf1+=1]=w1; cd2[f2]=new[t]-1; ce2[f2]=w0[t]=nf1,\
    g1=w0[t]=ce2[f2]; cd1[g1][d+1]=0)));\
\\ Mise a jour (et creation si necessaire) des (d-2)-facettes non encore traitees\
      for(t=1,d,if(new[t], g1=w0[t];\
        for(u=1,d,if((u!=t) && (u!=s),\
          if(new[u],\
            if(w0[u]<g1, cd2[nf2+=1]=2*m[t,u]-2; ce2[nf2]=g1; cd1[w0[u]][t]=cd1[g1][u]=nf2),\
            cd1[g1][u]=f2=cd1[w0[u]][t]; cd2[f2]-=1; ce2[f2]=g1)))));\
    cd0[nf0+=1]=w0;\
    )\
  );\
\\ Nombre de chambres (elements de W) enumeres\
\\ de (d-1)-cloisons et de (d-2)-facettes\
  print(nf0," ",nf1," ",nf2);\
};\
\
\\ Homographie ou antihomographie selon que g[3] vaut 0 ou 1.\
hom(g,z)=if(g[3],z=conj(z));(g[1]*z+g[2])/(conj(g[2])*z+conj(g[1]));\
\
\\ Sous l'hypothese 1/p+1/q+1/r<1\
\\ renvoie un pavage du disque de Poincare\
\\ du disque de Poincare avec des triangles\
\\ hyperboliques d'angles Pi/p, etc.\
\\ sous la forme d'un vecteur forme de points du plan\
\\ et d'une matrice a  quatre colonnes donnat les numeros\
\\ des sommets des triangles et 0 ou 1 selon l'orientation du triangle\
hyp3(p,q,r,l,dep)={\
  my(m=[1,p,q;p,1,r;q,r,1],nf0,nf1,nf2,f1,f2,ff1,s,t,u,g,gg,ggg,g1,z,res1,res2,\
  ep=exp(I*(Pi/p)),\
  eq=exp(I*(Pi/q)),\
  er=exp(I*(Pi/r)),\
  a=0,\
  b=sqrt((cos(Pi/r)+cos(Pi/p+Pi/q))/(cos(Pi/r)+cos(Pi/p-Pi/q))),\
  c=ep*sqrt((cos(Pi/q)+cos(Pi/p+Pi/r))/(cos(Pi/q)+cos(Pi/p-Pi/r))),\
  mm=(b*c*conj(b-c)+c-b)/(conj(b)*c-conj(c)*b),\
  cd0=vector(l), \\ pour chaque f, les numeros des 3 aretes du bord de la chambre f\
  ce0=vector(l), \\ et un elment g du groupe qui envoie la chambre 1 sur la chambre f\
  cd1=vector(2*l+1),\\ pour chaque a, les numeros des extremites de a\
  ce1=vector(2*l+1),\\ et le numero de la premiere chambre qu'elle borde\
  cd2=vector(l+2),\\ pour chaque s, l'arite restante,\
  ce2=vector(l+2) \\ les deux aretes qui bordent le secteur restant\
  );\
  \\ Les trois reflexions qui engendrent W et leurs conjugues\
  g=[[1,0],[ep,0],[I*mm,-I],[1,0],[1/ep,0],[conj(I*mm),I]];\
  \\ Creation de la premiere chambre et de son squelette\
  cd0[nf0=1]=vector(3,s,s);\
  ce0[nf0]=[1,0,0]; \\ Identite\
  for (s = 1, 3,\
    cd1[nf1+=1] = vector(4, t, if(t>3,s,-1));\
    ce1[nf1]=1;\
    for (t = 1, s-1,\
      cd2[nf2+=1] = 2*m[s,t]-2;\
      ce2[nf2] = [s,t];\
      cd1[t][s] = cd1[s][t] = nf2));\
\
  while(f1<nf1 && nf0<l, f1+=1; v1=cd1[f1]; if(s=v1[4], \\ arete du bord\
\\ element du groupe associe au triangle precedent\
   gg=ce0[ce1[f1]];\
\\ Nouveau triangle et ses aretes, manquantes ou pas\
    ce0[nf0+=1]=ggg=if(gg[3],\
      [gg[1]*g[3+s][1]+gg[2]*g[s][2],gg[1]*g[3+s][2]+gg[2]*g[s][1],0],\
      [gg[1]*g[s][1]+gg[2]*g[3+s][2],gg[1]*g[s][2]+gg[2]*g[3+s][1],1]);\
\\ Tracer le triangle, le remplir s'il est impair et si c'est demande\
    w0=vector(3); w0[s]=f1; new=vector(3,t,if(t!=s,cd2[v1[t]]));\
    for(t=1, 3, if(t!=s, f2=v1[t]; if(new[t],\
    w1=vector(4); w1[4]=t; w1[s]=f2; w1[t]=-1;\
    cd1[nf1+=1]=w1; ce1[nf1]=nf0; cd2[f2]=new[t]-1; ce2[f2]=w0[t]=nf1,\
    g1=w0[t]=ce2[f2]; cd1[g1][4]=0)));\
\\ Mise a jour (et creation si necessaire) des sommets non encore traites\
      for(t=1,3,if(new[t], g1=w0[t];\
        for(u=1,3,if((u!=t) && (u!=s),\
          if(new[u],\
      if(w0[u]<g1, cd2[nf2+=1]=2*m[t,u]-2; ce2[nf2]=g1;\
      cd1[w0[u]][t]=cd1[g1][u]=nf2),\
      cd1[g1][u]=f2=cd1[w0[u]][t]; cd2[f2]-=1; ce2[f2]=g1)))));\
      cd0[nf0]=w0;\
    )\
  );\
  res2=vector(nf0);\
  res1=vector(nf2);\
  for(x=1,nf0,w0=cd0[x];ggg=ce0[x];\
    s=cd1[w0[2]][1]; z=hom(ggg,a); if(dep,z=hom(dep,z)); res1[s]=[real(z),imag(z)];\
    t=cd1[w0[1]][3]; z=hom(ggg,b); if(dep,z=hom(dep,z)); res1[t]=[real(z),imag(z)];\
    u=cd1[w0[3]][2]; z=hom(ggg,c); if(dep,z=hom(dep,z)); res1[u]=[real(z),imag(z)];\
    res2[x]=[s,t,u,ggg[3]];\
  );\
  [matrix(#res1,2,i,j,res1[i][j]),matrix(#res2,4,i,j,res2[i][j])]\
};
