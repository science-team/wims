type=select
textarea="data pre post"
iEdit="pre post"

:Feu clic a les paraules per ressaltar-les en un text.

L'exercici presenta un text on les paraules s�n "marcables" amb el ratol�. Cal que
en seleccioneu algunes d'acord amb l'enunciat de l'exercici.
<p>
Per construir un exercici amb aquest model, nom�s cal que introdu�u els textos.
</p><p class="wims_credits">
Autor del model: <a href="mailto:qualite@wimsedu.info">Gang Xiao</a>
</p>
:%%%%%%%%%%%%%%%%%      ATTENTION      %%%%%%%%%%%%%%%%%%%%

Enlevez l'en-t�te ci-dessus si vous d�truisez les balises pour le mod�le !
(Ce sont les lignes qui commencent par un ':'.)
Sinon l'exercice risque de ne pas pouvoir repasser sous Createxo.

:%%%%%%%% Param�tres d'exemples � red�finir %%%%%%%%%%%%%%%%%

:\title{Seleccionar paraules en un text}
:\author{XIAO, Gang}
:\email{qualite@wimsedu.info}
:\credits{}

:Nombre m�nim de paraules a marcar a cada exercici. Almenys 1.
\integer{minmark=1}

:Nombre m�xim de paraules a marcar a cada exercici.
\integer{maxmark=8}

:Dades principals.
Escriure una pregunta per l�nia i separar les l�nies
per un punt i coma. Per tant, els textos no poden contenir 
punt i coma. <p>
Els camps que s'han de marcar han d'estar situats entre un parell de dobles
signes d'interrogaci� "??". Cada camp cont� una llista d'elements separats
per comes.</p>El primer element abans de la coma
�s <ul><li>una paraula err�nia,</li><li> una llista de paraules err�nies
posades entre claus</li><li>o es pot deixar buit.
</li></ul>
El segon element �s la paraula correcta.
<pre>
Le chat ??manges,mange?? la souris.
Le chat ??{mangent,menge},mange?? la souris.
Le chat ??,mange?? la souris.
</pre>

Aquestes paraules s'escolliran a l'atzar abans de mostrar
la frase.
</p>
Fins a cent punts de marcatge
es poden posar en cada frase.
$embraced_randitem

\text{data=
{Jack,Jean,Louis,Michel,Pierre} ??et,est?? forain, il
??{tien,tiens},tient?? ??un,une?? baraque de tir ??a,�?? la noix de coco.
??Ont,On?? ??trouvent,trouve?? des ??Baraque,Baraques?? Noix de Coco dans
??tous,toutes?? les foires. Les ??,gens?? ??arrive,arrivent??,
??donne,donnent?? des ??,sous??
??est,et?? ??envoie,envoient?? des ??,boules?? sur une noix de coco
??{poser,pos�},pos�e?? en haut d'une ??,colonne??.
Ceux qui ??fait,font??
??{d�gringol�,d�gringol�e},d�gringoler?? une noix de coco
??{peu,peut,peux},peuvent?? ??{le,les},la??
??{gard�e,gard�},garder??.
;

??{Quel,Quels,Quelles},Quelle??
id�e ??est-je,ai-je??
??{d'achet�,d'achet�e,d'achet�s},d'acheter?? ??{cept,cette,ces,ce},cet??
oiseau ? L'oiselier me dit : "??{S'est,Cet},C'est?? un ??{mal,malle},m�le??.
??Attender,Attendez?? une ??,semaine?? qu'il
??{s'abitue,s'abituent,s'habituent},s'habitue??, ??est,et?? il chantera".
??Hors,Or??, l'oiseau ??sobstine,s'obstine?? ??a,�?? ??ce,se??
??tait,taire?? et il ??fais,fait?? ??tous,tout?? de ??{traver,travert},travers??.
;

Les ??d�sert,d�serts?? de ??sables,sable?? ??occupe,occupent?? de
??large,larges?? parties {de la plan�te,du monde,de la Terre}.
Il n'y ??{pleu,pleus,pleuvent},pleut??
presque ??,pas??. Tr�s ??peut,peu?? de plantes et ??,d'animaux?? y
??vit,vivent??. Les ??,dunes?? ??son,sont?? des collines de
??,sable?? que le vent ??�,a??
??{construit,construits,construite},construites??. Les
??grains,graines?? de ??{certain,certaine,certains},certaines??
plantes ??reste,restent?? sous le ??sole,sol?? du d�sert pendant
des ann�es. ??{Ils,Elle},Elles?? ??ce,se?? ??met,mettent??
??a,�?? ??{pouss�es,pouss�e,pouss�s},pousser?? d�s qu'il y a
??une,un?? orage.
;

}

:Text que es mostra abans de la pregunta. Es permeten etiquetes HTML. Pot estar buit.
$embraced_randitem
\text{pre=
Marquez les fautes d'orthographe dans la phrase ci-dessous.
}

:Text que es mostra despr�s de la pregunta. Es permeten etiquetes HTML. Pot estar buit.
\text{post=}

:S'accepta text aleatori posat entre claus
$embraced_randitem
\text{accolade=item(1,1 s�,
2 no)}

:Opcions.
Afegiu la paraula <span class="tt wims_code_words">split</span> a la definici� si voleu donar una nota 
parcial per a respostes parcialment correctes.
\text{option=split}

:%%%%%%%%%%%%%% Rien � modifier avant l'�nonc� %%%%%%%%%%%%%%%%

\text{accolade=wims(word 1 of \accolade)}
\text{data=wims(singlespace \data)}
\text{data=wims(nonempty rows \data)}
\text{data=randomrow(\data)}
\text{data=\accolade=1 ? wims(embraced randitem \data)}
\text{data=slib(text/cutchoice2 \data)}
\integer{qs=floor(rows(\data)/2)}
\text{list=}
\text{ind=}
\for{i=1 to \qs}{
 \text{d=\data[2*\i;]}
 \text{list=\list \d;}
 \if{\d[1] issametext }{\text{ind=\ind[]2,}}{\text{ind=\ind[]1,}}
}

\text{can=positionof(1,\ind)}
\text{cancnt=items(\can)}
\integer{tomark=randint(min(\minmark,\cancnt)..min(\maxmark,\cancnt))}
\text{can=\cancnt>1?shuffle(\can)}
\text{good=item(1..\tomark,\can)}
\text{good=wims(sort numeric items \good)}

\text{show=}
\for{i=1 to \qs}{
 \text{d=\i isitemof \good?\list[\i;1]:\list[\i;2]}
 \text{show=\show[]\d,}
}

\text{pre=\accolade=1 ? wims(embraced randitem \pre)}

:%%%%%%%%%%%%% Maintenant l'�nonc� en code html. %%%%%%%%%%%%%%%%%%%%

::Vous n'avez pas besoin de modifier ceci en g�n�ral.

\statement{<div>
\pre
</div>
\data[1;]
\for{k=1 to \qs}{
\embed{r1,\k} \data[2*\k+1;]
}
\post
}

:%%%%%%%%%%%%% Rien � modifier ci-apr�s. %%%%%%%%%%%%%%%%%

\answer{Champ 1}{\good;\show}{type=mark}{option=\option}
