type=numbers gapfill
textarea="explain"
iEdit="explain"
random="a b c d"
computed="ans"
asis="explain"

:Une question num�rique avec 4 param�tres al�atoires.

<p>Mod�le permettant de faire un exercice dont l'�nonc� d�pend de 4 param�tres \a, \b, \c et \d et  et dont la r�ponse demand�e est un nombre.
 A chaque fois que l'�l�ve recommencera l'exercice, les valeurs des 4 param�tres seront tir�es au hasard dans un intervalle ou parmi une liste de valeurs qui vous d�finirez.
</p>
Vous avez la possibilit� de d�finir :
<ul><li>les intervalles ou listes de valeurs parmi lesquels les valeurs \a, \b, \c, \d des param�tres seront tir�s </li>
<li> la formule pour la r�ponse qui pourra d�pendre de \a, \b, \c et \d </li>
<li>quelles sont les r�ponses accept�es :
si la r�ponse demand�e est un entier ou une fraction rationnelle, il est possible de n'accepter que la valeur exacte. 
Dans les autres cas, vous pouvez d�finir une marge d'erreur absolue ou relative.  </li></ul>

$oef_help

<p class="wims_credits">
  Auteur du mod�le : <a href="mailto:qualite@wimsedu.info">Gang Xiao</a></p>

:%%%%%%%%%%%%%%%%%      ATTENTION      %%%%%%%%%%%%%%%%%%%%

Enlevez l'en-t�te ci-dessus si vous d�truisez les balises pour le mod�le !
(Ce sont les lignes qui commencent par un ':'.)
Sinon l'exercice risque de ne pas pouvoir repasser sous Createxo.

:%%%%%%%% Param�tres d'exemples � red�finir %%%%%%%%%%%%%%%%%

:\title{Quatre param�tres}
:\author{XIAO, Gang}
:\email{qualite@wimsedu.info}
:\credits{}

:Premier param�tre \a.
\real{a=randint(-20..20)}

:Deuxi�me param�tre \b.
\real{b=randint(-20..20)}

:Troisi�me param�tre \c.
\real{c=randint(-20..20)}

:Quatri�me param�tre \d.
\real{d=randint(-20..20)}

:La bonne r�ponse calcul�e.
Toutes les fonctions usuelles sont reconnues. On utilise \a, \b, \c, \d pour
d�signer les param�tres al�atoires d�finis en haut.
\real{ans=(\a)*(\d) - (\b)*(\c)}
:Marge d'erreur accept�e
<p>
S�lectionnez "aucune" si  seule la valeur exacte
est accept�e (utile pour des r�ponses enti�res ou des fractions). </p><p>
S�lectionnez "relative" (resp. "absolue") si vous acceptez des valeurs approch�es � une pr�cision relative (resp. absolue) que vous fixerez ensuite (voir l'aide du champ suivant).
</p>
\text{marge=item(2,1. aucune,2. relative,3. absolue)}

:Valeur de cette marge d'erreur
Notons <span class="tt">err</span> la valeur de cette marge d'erreur, <span class="tt">good</span>  la bonne r�ponse
et <span class="tt">rep</span>  la r�ponse de l'�l�ve. La r�ponse de l'�l�ve sera consid�r�e
comme juste  si
<ul><li>  <span class="tt">|good - rep| &le; err</span> dans le cas o� vous avez s�lectionn� "absolue",</li>
<li><span class="tt">|good - rep| &le;  err*|rep|</span>  dans le cas o� vous avez s�lectionn� "relative",</li></ul>
\real{erreur=0.001}

:Consigne pour entrer la r�ponse
Modifiez le texte pour l'adapter suivant en fonction de la marge d'erreur que vous avez choisie.
\text{instruction=La valeur donn�e sera consid�r�e comme correcte si l'erreur relative est inf�rieure � 0.001.}

:Le texte qui explique ce qu'il faut faire.

$embraced_randitem
\text{explain=
Calculez le d�terminant de la matrice \([\a,\b;\c,\d]\).
}

:Mise en al�atoire par des accolades emboitables
$embraced_randitem
\text{accolade=item(1,1. oui,
2. non)}

: Solution
Texte qui apparait en dessous de l'analyse de la r�ponse envoy� par l'apprenant et
(l'enseignant qui utilisera cet exercice peut d�cider ou non de faire apparaitre ce texte).
\text{textsol=}

:%% Calculs � ne pas modifier
\text{accolade=wims(word 1 of \accolade)}
\text{explain=\accolade=1. ?wims(embraced randitem \explain)}
\text{marge=wims(word 1 of \marge)}
\if{\marge=1.}
  {\text{format=numexp}
  }
  {\text{format=range}
   \if{\marge=2.}{\real{ecart=\erreur*\ans}}{\real{ecart=\erreur}}
   \real{ansmin=\ans-\ecart}
   \real{ansmax=\ans+\ecart}
   \text{ans=\ansmin,\ansmax,\ans}
   }
:%%%%%%%%%%%%% Maintenant l'�nonc� en code html. %%%%%%%%%%%%%%%%%%%%

::Vous n'avez pas besoin de modifier ceci en g�n�ral.

\statement{\explain
\if{\instruction notsametext}{<div class="wims_instruction">\instruction</div>}
}

:%%%%%%%%%%%%% Rien � modifier ci-apr�s. %%%%%%%%%%%%%%%%%5

\answer{R�ponse}{\ans}{type=\format}
\if{\textsol notsametext}{
\solution{\textsol}
}
\latex{
\begin{statement}
 \explain\newline
 \textit{\instruction}
\end{statement}
\begin{solution}
\textsol
\end{solution}
}
