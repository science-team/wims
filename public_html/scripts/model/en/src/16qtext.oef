type=gapfill first
textarea="explain data feedback_general"
iEdit="explain feedback_general"
asis="explain feedback_general"

:Mixte multiple choice questions embedded in a non variable text.

This exercise presents a text where response fields are inserted.
<p>
To build an exercise with this model, just enter the text.
An easy syntax allows you to incorporate multiple choice fields and
free answers in the text.
</p>
<p class="wims_credits">Author of the model: <a href="mailto:qualite@wimsedu.info">Gang Xiao</a></p>

:%%%%%%%%%%%%%%%%%      ATTENTION      %%%%%%%%%%%%%%%%%%%%

Remove the above header if you destroy the model tags! (The lines starting
with a ':'.) Otherwise the exercise might not be taken back by Createxo.

:%%%%%%%% Sample parameters to be redefined %%%%%%%%%%%%%%%%%

:\title{Choices in a text}
:\author{XIAO, Gang}
:\email{qualite@wimsedu.info}
:\credits{}

:Instruction for learners

\text{explain=Remplissez.}

:Main data.
Questions to ask should be put between a pair of
double-question marks "??".
<p>
The question is a multiple choice if it contains multiple objects separated by commas,
 the correct answer being first.<br>
If the question (between ??) contains only one object, then it is a free answer,
 numeric if it is a number or textual otherwise.
</p><p>
You can ask up to 20 questions in one sentence.
If several correct answers are possible in a free answer,
 separate all the correct answers (synonyms) with a vertical line <span class="tt wims_code_words">|</span>.
(the analysis is then done by symtext).
</p>
$embraced_randitem
\text{data=Linux was created in ??1991?? by Linus Torvalds,
who was a ??Finnish, Swedish, American, English, Nordic?? student
at the time working on the ??Minix?? system on
??Intel 386,Sun,Macintosh,Bull,Microsoft?? architecture machines.<br>
He posted on a newsgroup the first version of his system,
 which was version ??0.02??.
}
:Embraced random items
$embraced_randitem
\text{accolade=item(1,1 yes,
2 no)}

:Menu display order
Menus can be displayed in a shuffle order or sorted.
\text{list_order=item(1,alpha,shuffle)}

:Tolerance level for free answers
The types <span class="tt">case</span> and <span class="tt">nocase</span> are especially suitable for language applications.
In other cases, prefer<span class="tt">atext</span>.
\text{type_rep=item(3,case (no error tolerance),
  nocase (tolerate lowercase / uppercase),
  atext (ignore plural / singular and articles))}

:%%%%%%%%%%%%%% Nothing to modify until statement %%%%%%%%%%%%%%%%
\text{accolade=wims(word 1 of \accolade)}
\precision{10000}
\text{data=wims(singlespace \data)}
\text{data=\accolade=1 ? wims(embraced randitem \data)}
\text{data=slib(text/cutchoice2 \data)}
\integer{qs=min(20,floor(rows(\data)/2))}
\text{types=}
\text{options=}
\text{lengths=}

\text{good=}
\text{field=}
\for{i=1 to \qs}{
 \text{f=\data[2*\i;]}
 \integer{test=items(\f)}
 \if{\test>1}{
  \text{g=\f[1]}
  \if{\list_order issametext alpha}{
    \text{f=wims(sort items \f)}
  }{
    \text{f=shuffle(\f,,)}
  }
  \text{g=positionof(\g,\f)}
  \text{good=\good\g;,}
  \text{field=\field\f;}
  \text{types=\types[]menu,}
  \text{lengths=\lengths,}
 }{
  \text{good=\good\f,}
  \integer{c=wims(charcnt \f)}
  \text{field=\field;}
  \real{test=\f}
  \if{\test issametext NaN}{
   \text{test=wims(translate internal | to , in \f)}
   \integer{c=wims(charcnt \test[1])}
   \integer{c=min(50,max(6,\c+6))}
   \text{lengths=\lengths\c,}
   \text{types=\types[]\type_rep,}
   \text{options=| isin \f?\options[]symtext}
  }{
   \integer{c=max(5,\c+2)}
   \text{lengths=\lengths\c,}
   \text{f1=wims(nospace \f)}
   \text{test=wims(text select -0123456789 in \f)}
   \text{types=\test issametext \f1 and abs(\f)<4000?\types[]numexp,:\types[]number,}
  }
 }
 \text{options=\options,}
}

\text{qlist=wims(makelist reply x for x=1 to \qs)}
\steps{\qlist}

:%%%%%%%%%%%%% Now the statement in HTML. %%%%%%%%%%%%%%%%%%%%

::You don't need to modify this in general.

\statement{<div>\explain</div>
  \data[1;]
  \for{k=1 to \qs}{
    \embed{r \k,\lengths[\k]} \data[2*\k+1;]
  }
}

:%%%%%%%%%%%%% Nothing to modify after. %%%%%%%%%%%%%%%%%5

\answer{Field 1}{\good[1]\field[1;]}{type=\types[1]}{option=\options[1]}
\answer{Field 2}{\good[2]\field[2;]}{type=\types[2]}{option=\options[2]}
\answer{Field 3}{\good[3]\field[3;]}{type=\types[3]}{option=\options[3]}
\answer{Field 4}{\good[4]\field[4;]}{type=\types[4]}{option=\options[4]}
\answer{Field 5}{\good[5]\field[5;]}{type=\types[5]}{option=\options[5]}
\answer{Field 6}{\good[6]\field[6;]}{type=\types[6]}{option=\options[6]}
\answer{Field 7}{\good[7]\field[7;]}{type=\types[7]}{option=\options[7]}
\answer{Field 8}{\good[8]\field[8;]}{type=\types[8]}{option=\options[8]}
\answer{Field 9}{\good[9]\field[9;]}{type=\types[9]}{option=\options[9]}
\answer{Field 10}{\good[10]\field[10;]}{type=\types[10]}{option=\options[10]}
\answer{Field 11}{\good[11]\field[11;]}{type=\types[11]}{option=\options[11]}
\answer{Field 12}{\good[12]\field[12;]}{type=\types[12]}{option=\options[12]}
\answer{Field 13}{\good[13]\field[13;]}{type=\types[13]}{option=\options[13]}
\answer{Field 14}{\good[14]\field[14;]}{type=\types[14]}{option=\options[14]}
\answer{Field 15}{\good[15]\field[15;]}{type=\types[15]}{option=\options[15]}
\answer{Field 16}{\good[16]\field[16;]}{type=\types[16]}{option=\options[16]}
\answer{Field 17}{\good[17]\field[17;]}{type=\types[17]}{option=\options[17]}
\answer{Field 18}{\good[18]\field[18;]}{type=\types[18]}{option=\options[18]}
\answer{Field 19}{\good[19]\field[19;]}{type=\types[19]}{option=\options[19]}
\answer{Field 20}{\good[20]\field[20;]}{type=\types[20]}{option=\options[20]}

