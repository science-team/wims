type=question
textarea="data instruction instruction1 instruction2 instruction3 feedback1 feedback2 feedback3"
iEdit="instruction instruction1 instruction2 instruction3 feedback1 feedback2 feedback3"

:Vengono poste due o tre domande successive su un oggetto scelto a caso in una lista.
� possibile scegliere il tipo di domande
(numeriche, testo, scelte multiple). Nel caso di domande a scelta multipla,
� necessario compilare l'elenco delle scelte possibili.

<p>
I dati per ciuascun oggetto (un oggetto per riga) devono essere separati da virgole
e formattati nel modo seguente:
</p>
<pre>
oggetto, risposta alla domanda 1, risposta alla domanda 2, risposta alla domanda 3
</pre>
<p>
L'oggetto pu� essere una immagine (nel qual caso inserire il codice html: <span class="tt wims_address">&lt;img src="\imagedir/nom.png" alt=""></span>).
Nel caso di risposte a scelta multipla, come risposta occorre inserire il numero della risposta corretta.
</p>
<p>
Nei feedback, per indicare l'oggetto si pu� utilizzare la variabile <span class="tt wims_code_variable">\name</span>.
</p>
:%%%%%%%% Parametri che possono essere ridefiniti %%%%%%%%%%%%%%%%%

:\title{Domande successive su un oggetto}
:\author{Bernadette, Perrin-Riou}
:\email{bernadette.perrin-riou@math.u-psud.fr}
:\credits{}

:Formato 1
\text{format1=item(1, radio (una scelta sola nella lista seguente),
    numeric (la risposta � un numero),
    case (nessuna differenza ammessa),
	nocase (ammessa differenza maiuscolo/minuscolo),
	atext (ignorato plurale/singolare e articoli)
	)}
:Lista di scelte per la domanda 1 (se di tipo radio)
Inserire l'elenco delle risposte corrette se si � scelto il tipo radio
\text{choix1=primo gruppo, secondo gruppo, terzo gruppo}

:Formato 2
\text{format2=item(3,radio (una scelta sola nella lista seguente),
    numeric (la risposta � un numero),
    case (nessuna differenza ammessa),
	nocase (ammessa differenza maiuscolo/minuscolo),
	atext (ignorato plurale/singolare e articoli)
	)}
:Lista di scelte per la domanda 2 (se di tipo radio)
Inserire l'elenco delle risposte corrette se si � scelto il tipo radio
\text{choix2=}
:Formato 3
\text{format3=item(4,0 (nessuna domanda),
    radio (una scelta sola nella lista seguente),
    numeric (la risposta � un numero),
    case (nessuna differenza ammessa),
	nocase (ammessa differenza maiuscolo/minuscolo),
	atext (ignorato plurale/singolare e articoli)
   	)}
:Lista di scelte per la domanda 3 (se di tipo radio)
Inserire l'elenco delle risposte corrette se si � scelto il tipo radio
\text{choix3=}
:Testo per risposta corretta
\text{right=Hai ragione.}
:Testo per risposta non corretta
\text{false=La risposta � sbagliata.}

:Dati principali
\text{data= manger,1,Je mangeais, Je mangerai
finir, 2, Je finissais, Je finirai
courir,3, Je courais, Je courrai}


:%%%%%%%%%%%%%%
\text{data=randomrow(\data)}
\text{name=<span class="name">\data[1]</span>}
\text{data=\data[2..-1]}

:Istruzioni generali
\text{instruction=Dobbiamo studiare il verbo \name.}
:Istruzioni per il primo passo
\text{instruction1=Qual � il grupo di \name?}

:Istruzioni per il secondo passo
\text{instruction2=Inserisci l'imperfetto del verbo \name alla prima persona singolare, senza dimenticare il pronome.}

:Istruzioni per il terzo passo
\text{instruction3=Inserisci il futuro del verbo \name alla prima persona singulare, senza dimenticare il pronome.}

:%%%%%%%%%%%%%% Nulla da modificare fino all'enunciato %%%%%%%%%%%%%%%%
\css{<style>
  .name {color:#047;}
  .big_bullet>li{font-size:2em;color:#599;margin:0 0 .5em .5em;}
  .big_bullet div, .big_bullet label{font-size:1rem;margin:.2em;}
  .big_bullet .instruction,.big_bullet .reponse{color:initial;}
</style>}

\text{format1=wims(word 1 of \format1)}
\text{format2=wims(word 1 of \format2)}
\text{format3=wims(word 1 of \format3)}
\text{rep1=\data[1]}
\text{rep2=\data[2]}
\text{rep3=\data[3]}

\text{reponse1=\data[1]}
\text{reponse2=\data[2]}
\text{reponse3=\data[3]}
\if{\format1 issametext radio}{
  \text{rep1=\data[1];\choix1}
  \text{reponse1=\choix1[\data[1]]}
}
\if{\format2 issametext radio}{
  \text{rep2=\data[2];\choix2}
  \text{reponse2=\choix2[\data[2]]}
}
\if{\format3 issametext radio}{
  \text{rep3=\data[3];\choix3}
  \text{reponse3=\choix3[\data[3]]}
}
\text{STEP=r1}
\if{\format2!=0}{
  \text{STEP=\STEP
  r2}
  \if{\format3!=0}{
  \text{STEP=\STEP
  r3}
  }
}

\text{reponse1=<span class="oef_indgood">\reponse1</span>}
\text{reponse2=<span class="oef_indgood">\reponse2</span>}
\text{reponse3=<span class="oef_indgood">\reponse3</span>}
:Feedback per il primo passo

\text{feedback1=\name � un verbo del gruppo \reponse1.}

:Feedback per il secondo passo

\text{feedback2=L'imperfetto alla prima persona singolare del verbo \name � \reponse2.}

:Feedback finale

\text{feedback3=Il futuro alla prima persona singolare del verbo \name � \reponse3.}


:%%%
\nextstep{\STEP}

\statement{
<div>\instruction</div>
<ol class="big_bullet"><li>
  <div class="instruction">\instruction1</div>
  \if{\step=1 or \step>=4}{
    \if{\format1 notsametext radio}{
      <label class="reponse" for="reply1">\name_my_answer \embed{r1}</label>
    }{
      <div class="reponse">\name_my_answer \embed{r1}</div>
    }
  }{
    <div class="reponse">
      \name_my_answer <span class="name">\reply1</span>
    </div>
  }

  \if{\step>=2}{
    <div class="feedback
      \if{\sc_reply1=1}{oef_indgood">\right}{\if{\sc_reply1<=0}{oef_indbad}{oef_indpartial}">\false} \feedback1
    </div>
    </li><li>
    <div class="instruction">
      \instruction2
    </div>
  }

  \if{\step=2 or \step>=4}{
    \if{\format2 notsametext radio}{
      <label class="reponse" for="reply2">\name_my_answer \embed{r2}</label>
    }{
      <div class="reponse">\name_my_answer \embed{r2}</div>
    }
  }

  \if{\step>=3}{
    \if{\step < 4}{
      <div class="reponse">
        \name_my_answer <span class="name">\reply2</span>
      </div>
    }
    <div class="feedback
      \if{\sc_reply2=1}{oef_indgood">\right}{\if{\sc_reply2<=0}{oef_indbad}{oef_indpartial}">\false} \feedback2
    </div>
    \if{\format3!=0}{
      </li><li>
      <div class="instruction"> \instruction3</div>
      \if{\format3 notsametext radio}{
        <label class="reponse" for="reply3">\name_my_answer \embed{r3}</label>
      }{
        <div class="reponse">\name_my_answer \embed{r3}</div>
      }
    }
  }
  \if{\step>=4 and \format3!=0}{
    <div class="feedback
      \if{\sc_reply3=1}{oef_indgood">\right}{\if{\sc_reply3<=0}{oef_indbad}{oef_indpartial}">\false} \feedback3
    </div>
  }
  </li>
</ol>
}

\answer{}{\rep1}{type=\format1}{option=nonstop}
\answer{}{\rep2}{type=\format2}{option=nonstop}
\answer{}{\rep3}{type=\format3}{option=nonstop}
