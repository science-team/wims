type=question
textarea="data1 data2 data3 data4 data5 data6 data7 data8 data9 data10 instruction"
asis="data1 data2 data3 data4 data5 data6 data7 data8 data9 data10 instruction"
iEdit="instruction"

:Varias preguntas seguidas, todas del tipo QCM

<p>Este ejercicio presenta preguntas de opci�n m�ltiple.</p>
<p>El n�mero de preguntas est� limitado a 10. Podemos elegir la cantidad de preguntas
para ser presentado en cada ejercicio. Estas preguntas se tomar�n al azar o no.</p>
<p class="wims_credits">Autor del modelo : <a href="mailto:bernadette.m.riou@orange.fr">Bernadette Perrin-Riou</a></p>

:%%%%%%%%%%%%%%%%%      ATENCI�N      %%%%%%%%%%%%%%%%%%%%

Elimina la cabecera anterior si borra las etiquetas para el modelo!
(Son las l�neas que comienzan con un ':'.)
En caso contrario el ejercicio puede no funcionar bajo Createxo.

:%%%%%%%% Par�metros del ejemplo a redefinir %%%%%%%%%%%%%%%%%

:\title{Opci�n m�ltiple avanzada}
:\author{Bernadette, Perrin-Riou}
:\email{bpr@math.u-psud.fr}
:\credits{Image de Pfly [CC BY-SA 2.5], via Wikimedia Commons}

:Instruciones Globales
Escriba aqu� una instrucci�n com�n a todas las preguntas del ejercicio
$embraced_randitem
\text{instruction=Responda cada pregunta en una primera serie, valide y responda las preguntas en la segunda serie.}

:Orden aleatorio
Elija "s�" para activar la aleatoriedad de las preguntas.
Elija "no" para presentarlas en el orden en que se ingresaron.
\text{alea=item(1,s�,no)}

:N�mero m�ximo de preguntas por etapa
WIMS dividir� autom�ticamente su ejercicio en etapas seg�n la cantidad total de preguntas establecidas y la cantidad de preguntas por etapa.
\text{N=3}

:N�mero m�ximo de etapas
WIMS dividir� autom�ticamente su ejercicio en etapas seg�n la cantidad total de preguntas establecidas y la cantidad de preguntas por etapa.
\text{MAX=2}

:Texto junto a la respuesta dada una vez que el alumno ha respondido.
Estas palabras intervienen una vez que las preguntas han sido respondidas.
Es aconsejable ejecutar el ejercicio de demostraci�n con
respuestas correctas e incorrectas para entender d�nde intervienen estas palabras.
\text{qcm_prompt1=Vuestra respuesta:}

:Texto para el feedback para "La respuesta correcta"

\text{qcm_prompt2=La(s) respuesta(s) correcta(s) era(n):}

:Texto para el feedback para "La respuesta correcta"

\text{good_answer_text=Respuesta correcta!}

:Texto para el feedback para "Respuesta incorrecta"

\text{bad_answer_text=Respuesta incorrecta!}

:Texto para el feedback para "Respuesta incompleta"

\text{incomplete_answer_text=Respuesta incompleta...}

:Estilo CSS para las preguntas

\text{style_question=background-color: #F2F9FC;}

:Estilo CSS para las  respuestas

\text{style_reponse=color:#555;font-size:90%;}

:Porcentaje de �xito requerido para pasar a la siguiente etapa (si el n�mero m�ximo de etapas es> 1)

\text{percent=0}

:Se acepta texto aleatorio puesto entre llaves
$embraced_randitem
\text{accolade=item(2, s�, no)}

:Affichage de l'analyse des r�ponses
Si el ejercicio se utiliza con la configuraci�n
"Nunca mostrar respuestas correctas":
<ul><li>si elige "si", luego de cada pregunta
se indicar� si las opciones seleccionadas son correctas o incorrectas.
</li><li>si elige "no", despu�s de cada pregunta, las opciones
seleccionadas se mostrar�n sin indicaci�n.
</li></ul>
En otros casos, despu�s de cada pregunta, se indicar� si
las opciones seleccionadas son correctas o incorrectas y, en caso de error,
se mostrar� la lista de opciones correctas.

\text{answer_given=item(1, si, no)}

:Pregunta
<p>Ingrese aqu� la pregunta que quiere hacer, de acuerdo con el siguiente formato:</p>
<ol>
<li>La primera l�nea contiene el enunciado de la pregunta.</li>
<li>La segunda l�nea representa un comentario, que se mostrar� despu�s de la validaci�n de las respuestas
(puede dejarse vac�o).</li>
<li>Ingrese en la tercera l�nea la lista de n�meros de las respuestas correctas, separados por comas. (Los n�meros representan el orden en que se escriben las propuestas a continuaci�n)</li>
<li>Cada l�nea siguiente representa las diferentes propuestas de respuesta.</li></ol>
<div style="border-left:2px solid orange;width:45em;background-color:white;padding:1em .5em;">Ejemplo: <br>
<pre>Enunciado de la pregunta n�1
Explicaci�n (comentarios) que se muestra en caso de respuesta incorrecta a la pregunta n� 1
N�mero(s) que indiquen el lugar de la(s) respuesta(s) correcta(s)
Propuesta n�1
Propuesta n�2
Propuesta n�3</pre></div>
<p class="oef_indbad"><strong>Advertencia:</strong> los punto y coma est�n prohibidos aqu�.</p>
<hr>
<p><em>Opcional:</em> opcionalmente puede agregar una primera l�nea que contendr� variables para insertar un t�tulo, una imagen, sonido. (nb: debe estar en una clase o m�dulo para transferir estos archivos). En este caso, ser� la segunda l�nea (y no la primera) la que representar� el enunciado, la tercera los comentarios, etc.
Las posibles variables son:
<ul>
  <li><code>Qtitle</code> : muestra un t�tulo al comienzo de la pregunta. (por ejemplo, para indicar el tema)</li>
  <li><code>Qimage</code> : si se especifica un archivo de imagen, se mostrar� al principio de la pregunta (solo funciona en un m�dulo o clase).</li>
  <li><code>Qaudio</code> : si se indica un archivo de sonido, se mostrar� al principio de la pregunta (solo funciona en un m�dulo o una clase. Errores conocidos con Safari).</li>
  </ul>
<div style="border-left:2px solid orange;width:45em;background-color:white;padding:1em .5em;">Ejemplo: <br>
<pre>Qtitle="T�tulo de la pregunta n� 2" Qimage="image.jpg" Qaudio="son.mp3"
Enunciado de la pregunta n� 2
Explicaci�n (comentarios) que se muestra en caso de respuesta incorrecta a la pregunta n� 2
N�mero(s) que indiquen el lugar de la(s) respuesta(s) correcta(s)(s)
Propuesta n�1
Propuesta n�2
Propuesta n�3</pre></div>
\text{data1=asis(�En qu� a�o tuvo lugar la Batalla de Mari��n?
Mari��n fue la primera victoria del joven rey Francisco I, el primer a�o de su reinado.
1
1515
1414
1313
1616)}

:Pregunta 2

\text{data2=asis(En 2014, �cu�l es la ciudad m�s poblada del mundo?
Es Tokio, la capital de Jap�n (37,7 millones de habitantes), muy por delante de las dem�s (que tienen entre 20 y 24 millones de habitantes). <p class="right">Fuente<a href="http://www.populationdata.net/index2.php?option=palmares&rid=4&nom=grandes-villes-du-monde" target="_blank">populationdata.net</a></p>
7
Beijing
Karachi
Manila
Ciudad de M�xico
Nueva York
Shang�i
Tokio)}

:Pregunta 3

\text{data3=asis(�Cu�l es el nombre de este r�o?<div><img src="http://upload.wikimedia.org/wikipedia/commons/e/e4/Mekong_River_watershed.png"></div>
Alrededor de 70 millones de personas viven directamente en la cuenca del Mekong.
1
El mekong
El Yangzi Jiang
El volga
El Danubio)}

:Pregunta 4

\text{data4=asis(Qtitle="En Pintura"
�Cu�les de estos diferentes pintores son parte del movimiento impresionista?
Ingres est� asociado con el movimiento rom�ntico y Rapha�l con el Renacimiento.
1,3
Edgar Degas
Dominique Ingres
Claude Monet
Rapha�l)}

:Pregunta 5

\text{data5=asis(Qtitle=ANIMALES
�Qu� es el grito del camello?
Rebuzno es el grito del burro, balido: el de las ovejas y las cabras.
1
balbuceo
rebuzno
balido)}

:Pregunta 6

\text{data6=asis(Qtitle=Qu�mica
�Cu�l es la f�rmula qu�mica de la testosterona?
C<sub>8</sub>H<sub>10</sub>N<sub>4</sub>O<sub>2</sub> correspond � la caf�ine, C<sub>3</sub>H<sub>5</sub>N<sub>3</sub>O<sub>9</sub> � la nitroglyc�rine, et C<sub>17</sub>H<sub>19</sub>NO<sub>3</sub> la morphine
4
C<sub>8</sub>H<sub>10</sub>N<sub>4</sub>O<sub>2</sub>
C<sub>3</sub>H<sub>5</sub>N<sub>3</sub>O<sub>9</sub>
C<sub>17</sub>H<sub>19</sub>NO<sub>3</sub>
C<sub>19</sub>H<sub>28</sub>O<sub>2</sub>)}

:Pregunta 7

\text{data7=asis(�A partir de qu� d�a est� disponible a la venta el Beaujolais Nouveau?
Beaujolais Nouveau es un vino primeur, es decir, un vino del mismo a�o, cuya comercializaci�n se autoriza inmediatamente despu�s de finalizada la vinificaci�n.
3
el primer jueves de noviembre
el segundo jueves de noviembre
el tercer jueves de noviembre
el cuarto jueves de noviembre)}

:Pregunta 8

\text{data8=asis(En la pel�cula <i>"Rain Man"</i>, �qu� actor tiene un hermano autista superdotado?
<i>Rain Man</i> es una pel�cula estadounidense dirigida por Barry Levinson, estrenada en 1989 en Francia.
2
Bruce Willis
Tom Cruise
Woody Allen)}

:Pregunta 9

\text{data9=asis(El esqu� acu�tico es una disciplina ol�mpica desde 1976.
La aparici�n del esqu� acu�tico en los Juegos Ol�mpicos solo data de 2004. Sin embargo, fue un deporte de demostraci�n en 1972.
2
Verdadero
Falso)}

:Pregunta 10

\text{data10=asis(Qtitle=M�sica
�Cu�l es el primer ballet escrito por Tchaikovsky?
El lago de los cisnes se escribi� en 1875 y El cascanueces en 1891.
1
El lago de los cisnes
El cascanueces)}

:Tipo de respuestas
El tipo "checkbox" (botones cuadrados) permite varias respuestas por pregunta. <br>
Mientras que el tipo "radio" (botones redondos) permite solo una respuesta por pregunta.
\text{format=item(1, checkbox, radio)}

:
\language{es}
\computeanswer{no}
\format{html}

\text{paste=yes}

\text{option=}

:%%%%%%%%%%%%%% Rien � modifier jusqu'� l'�nonc� %%%%%%%%%%%%%%%%
\text{givegood=slib(oef/env givegood)}
\text{givegood=\givegood issametext ? 1}
\text{answer_given=\givegood>0? si}
\text{data_q=\data1!= ? 1:}
\text{data_q=\data2!= ? wims(append item 2 to \data_q)}
\text{data_q=\data3!= ? wims(append item 3 to \data_q)}
\text{data_q=\data4!= ? wims(append item 4 to \data_q)}
\text{data_q=\data5!= ? wims(append item 5 to \data_q)}
\text{data_q=\data6!= ? wims(append item 6 to \data_q)}
\text{data_q=\data7!= ? wims(append item 7 to \data_q)}
\text{data_q=\data8!= ? wims(append item 8 to \data_q)}
\text{data_q=\data9!= ? wims(append item 9 to \data_q)}
\text{data_q=\data10!= ? wims(append item 10 to \data_q)}

\text{instruction=\accolade issametext s� ? wims(embraced randitem \instruction):\instruction}

\integer{cnt_question=items(\data_q)}

\text{nopaste=\paste issametext no ? slib(utilities/nopaste )}

\text{style = <style>
 .enonce{margin-bottom:0}
 .qcm_prompt2{margin-top:.5em}
 img{vertical-align:middle;}
 .q_num, .panel .wims_title{
 font-size:120%;font-family:Impact, Charcoal, sans-serif;
 color:#717171;
 }
 .feedback{border:1px dashed grey;padding:.5em;margin-top:.5em;}
 .reponse {
   margin: 0 1em;
   padding: .5em;
   border-radius:5px;
   \style_reponse
 }
 .panel{
    padding:.5em 1em .5em 1em;
    margin:.5em 0;
    border:1px solid #d8d8d8;
    border-radius:5px;
 }
 .panel.callout{
    border-color:#c5e5f3;
    border-width:2px;
    \style_question
 }
 .callout .q_num, .callout .wims_title{color:black;}

 .panel .wims_title, .panel .wimscenter{margin-top:-1.2em;}
 ol li{margin-bottom: .5em;list-style:upper-alpha;}
 input[type='checkbox'] { font-size:120%; }
 .strike{text-decoration:line-through;}
 .oef_indpartial{color:navy;}
 </style>
}

\integer{N = min(\cnt_question,\N)}
\integer{MAX=min(\N*\MAX,\cnt_question)}
\text{battage=\alea issametext si ? shuffle(\data_q,,) :\data_q}
\text{battage=wims(nonempty items \battage)}

\text{option=\option noanalyzeprint}
\matrix{question=}
\matrix{explication=xxx}
\matrix{rep=}
\text{CNT_choix= }
\matrix{CHOIX=}
\text{PRELIMINAIRE=}

\for{i= 1 to \MAX}{
 \matrix{QUEST=\battage[\i]=1? \data1}
 \matrix{QUEST=\battage[\i]=2? \data2}
 \matrix{QUEST=\battage[\i]=3? \data3}
 \matrix{QUEST=\battage[\i]=4? \data4}
 \matrix{QUEST=\battage[\i]=5? \data5}
 \matrix{QUEST=\battage[\i]=6? \data6}
 \matrix{QUEST=\battage[\i]=7? \data7}
 \matrix{QUEST=\battage[\i]=8? \data8}
 \matrix{QUEST=\battage[\i]=9? \data9}
 \matrix{QUEST=\battage[\i]=10? \data10}

 \text{preliminaire_test=\QUEST}
 \text{preliminaire_test=row(1,\preliminaire_test)}
 \text{inst_audio=wims(getopt Qaudio in \preliminaire_test)}
 \text{inst_image=wims(getopt Qimage in \preliminaire_test)}
 \text{inst_title=wims(getopt Qtitle in \preliminaire_test)}

 \text{rab_inst=}
 \text{rab_inst=\inst_title notsametext ?\rab_inst <h2 class="wims_title">\inst_title</h2>}
 \text{rab_inst=\inst_image notsametext ?\rab_inst <div class="wimscenter"><img src="\imagedir/\inst_image" alt=""></div>}
 \if{\inst_audio notsametext }{
     \text{rab_inst1= . isin \inst_audio ?
       <audio controls>
        <source src="\imagedir/\inst_audio" type="audio/mpeg">
        D�sol�, votre navigateur est incompatible avec la lecture de fichiers audio.
       </audio>}
     \text{rab_inst=\rab_inst <div class="wimscenter audio">\rab_inst1[1;1]</div>}
 }

 \integer{test_inst=\inst_audio\inst_image\inst_title notsametext ? 1 : 0}
 \text{preliminaire=\test_inst=1 ? \rab_inst:&nbsp;}
 \matrix{QUEST=\test_inst=1 ? \QUEST[2..-1;]}
 \matrix{QUEST=\accolade issametext si ?wims(embraced randitem \QUEST):\QUEST}
 \matrix{question = \question
\QUEST[1;]}
 \matrix{PRELIMINAIRE=\PRELIMINAIRE
\preliminaire}

 \text{expl=\QUEST[2;]}
 \text{ligne=wims(upper \expl)}
 \text{ligne=wims(text select ABCDEFGHIJKLMNOPQRSTUVWXYZ in \ligne)}
 \if{\ligne issametext and \expl notsametext }{
  \matrix{explication = \explication;}
  \integer{debut = 2}
 }
 \if{\ligne issametext and \expl issametext }{
  \matrix{explication = \explication;}
  \integer{debut = 3}
  }
  \if{\ligne notsametext}{
     \matrix{explication = \explication;\expl}
  \integer{debut = 3}
 }
  \integer{cnt_choix=rows(\QUEST)-\debut}
  \text{CNT_choix=\CNT_choix,\cnt_choix}
   \text{Choix=}
   \text{mix=shuffle(\cnt_choix)}
   \for{j=\debut+1 to \cnt_choix + \debut+1}{
     \text{choix= \QUEST[\j;]}
     \text{choix=wims(replace internal , by  &#44; in \choix)}
     \matrix{Choix = \Choix, \choix[1;]}
   }
   \text{Choix=wims(nonempty items \Choix)}
   \text{Choix= \Choix[\mix]}
   \matrix{CHOIX=\CHOIX
     \Choix}
   \text{H = wims(nospace \QUEST[\debut;])}
   \text{cnt_c=items(\H)}
   \text{Rep = }
   \for{ k = 1 to \cnt_c}{
     \text{Rep = \Rep, position(\H[\k],\mix)}
   }
   \text{Rep = wims(sort items wims(nonempty items \Rep))}
   \matrix{rep = \rep
    \Rep}
}

\text{CNT_choix=wims(nonempty items \CNT_choix)}

\text{U = pari(divrem(\MAX,\N)~)}
\integer{cnt_step = \U[1] + 1}
\matrix{STEPS = }
\matrix{CNT = }
\text{CONDSTEP=}
\for{ u = 1 to \cnt_step -1}{
   \matrix{STEPS =\STEPS
wims(makelist r x for x = \N*\u -\N+1 to \N*\u)}
   \matrix{CNT =\CNT
wims(makelist x for x = \N*\u -\N+1 to \N*\u)}
   \text{condstep= wims(values \u+1 for x = (\u-1)*\N +1 to \u*\N)}
   \text{CONDSTEP= wims(append item \condstep to \CONDSTEP)}
}
 \matrix{STEPS = \STEPS
 wims(makelist r x for x = \N*\cnt_step-\N+1 to \MAX)
}
\matrix{CNT = \CNT
 wims(makelist x for x = \N*\cnt_step-\N+1 to \MAX)
}
\text{CONDSTEP=\CONDSTEP, wims(values \cnt_step+1 for x = \N*\cnt_step-\N+1 to \MAX)}

\text{nstep=\STEPS[1;]}
\text{TEST=}
\text{explication=\explication[2..-1;]}

text{testexp=wims(rows2lines \explication)}
text{testexp=wims(lines2items \testexp)}
text{testexp=wims(items2words \testexp)}
text{testexp=wims(nospace \testexp)}
\nextstep{\nstep}
\text{REP=}
\text{etape=wims(values x * \N for x = 1 to \cnt_step+1)}
\text{CONDITION = wims(makelist x for x = 1 to 2*\MAX)}
\text{CONDITION =wims(items2words \CONDITION)}
\conditions{\CONDITION}
\integer{cnt_juste=0}
\real{v = 10}
\integer{questioncnt=items(\question[;1])}
\text{latexsrc=}
\text{latexsol=}
\for{hh=1 to \questioncnt}{
  \text{prel=\PRELIMINAIRE[\hh;]!= and \PRELIMINAIRE[\hh;] notsametext &nbsp;? \PRELIMINAIRE[\hh;]
\newline:}
  \text{latexsrc=\latexsrc \item \prel \question[\hh;]
\begin{\format}}
  \for{ss=1 to \CNT_choix[\hh]}{\text{latexsrc=\latexsrc\item \CHOIX[\hh;\ss]}}
  \text{listsol=\CHOIX[\hh;\rep[\hh;]]}
  \text{latexsol=\latexsol \item \listsol}
  \text{latexsrc=\latexsrc
\end{\format}}
}

\statement{\nopaste
  \style
  <div class="instruction">\instruction</div>

  \for{h=1 to \etape[\step]}{

    \if{\question[\h;] notsametext }{
      \if{\h <= \etape[\step] - \N}
        {<fieldset class="panel">}
        {\if{r \h isitemof \nstep}{<fieldset class="panel callout">}}
      }

   \if{(\h <= \etape[\step] - \N or r \h isitemof \nstep) and \question[\h;] notsametext }{
     <div class="enonce">
       \if{\cnt_step > 1 and \MAX > 1}{<span class="q_num">\h. </span>}
       \if{\PRELIMINAIRE[\h;] notsametext and \PRELIMINAIRE[\h;] notsametext &nbsp;}
       {<div class="preliminaire">\PRELIMINAIRE[\h;]</div>}
        <span class="question">\question[\h;]</span>
     </div>
   }

   \if{\h <= \etape[\step] - \N and \question[\h;] notsametext}{
    <div class="reponse">
      <span class="qcm_prompt1">\qcm_prompt1</span>
       \for{ a in \REP[\h;]}{
         \if{\answer_given=si}{
           \if{ \a isitemof \CHOIX[\h;\rep[\h;]]}{
             \if{\TEST[\h;2]>0 and \TEST[\h;3]=0}{
                 <span class="oef_indpartial">\a</span>}
                {<span class="oef_indgood">\a</span>}
                }{
                 <span class="oef_indbad strike">\a</span>} - }
             {<span class="oef_indneutral">\a</span> -}
           }
         \if{\answer_given=si}{
           \if{\TEST[\h;2]>0 and \TEST[\h;3]=0}
              {\incomplete_answer_text
               \if{\givegood>0}{<br> <span class="prompt">\qcm_prompt2</span>
                  <span class="oef_indgood">\CHOIX[\h;\rep[\h;]]</span>}
              }
           \if{\TEST[\h;3]>0}
             {\bad_answer_text
              \if{\givegood>0}{
                 <br> <span class="prompt">\qcm_prompt2</span>
                 <span class="oef_indgood">\CHOIX[\h;\rep[\h;]]</span>}
             }
           \if{\TEST[\h;3]=0 and \TEST[\h;2]=0}
             {\good_answer_text}
            }
           \if{\givegood>0}{
             \if{\explication[\h;] notsametext }{<div class="feedback">\explication[\h;]</div>}
             }
     </div>
     }{
      \if{ r \h isitemof \nstep}{
      <div class="question">
        <ol>
          \for{s=1 to \CNT_choix[\h]}{ <li>\embed{reply \h , \s}</li> }
        </ol>
      </div>
      }
   }
   \if{\question[\h;] notsametext
    and (\h <= \etape[\step] - \N or r \h isitemof \nstep)}{</fieldset>}
  }
 }
\answer{}{\REP1;\CHOIX[1;]}{type=\format}{option=\option}
\answer{}{\REP2;\CHOIX[2;]}{type=\format}{option=\option}
\answer{}{\REP3;\CHOIX[3;]}{type=\format}{option=\option}
\answer{}{\REP4;\CHOIX[4;]}{type=\format}{option=\option}
\answer{}{\REP5;\CHOIX[5;]}{type=\format}{option=\option}
\answer{}{\REP6;\CHOIX[6;]}{type=\format}{option=\option}
\answer{}{\REP7;\CHOIX[7;]}{type=\format}{option=\option}
\answer{}{\REP8;\CHOIX[8;]}{type=\format}{option=\option}
\answer{}{\REP9;\CHOIX[9;]}{type=\format}{option=\option}
\answer{}{\REP10;\CHOIX[10;]}{type=\format}{option=\option}

\matrix{REP = \REP1
\REP2
\REP3
\REP4
\REP5
\REP6
\REP7
\REP8
\REP9
\REP10}
\if{\format=radio}{
  \text{REP=wims(replace internal , by &#44; in \REP)}
}
\matrix{explication2 = \explication2}

\for{u = 1 to \N}{
   \text{H = \CNT[\step-1;\u]}
   \text{test1 = wims(listuniq \REP[\H;],\CHOIX[\H;\rep[\H;]])}
   \integer{test1 = items(\test1)-items(\CHOIX[\H;\rep[\H;]])}
   \text{test2 = wims(listcomplement \REP[\H;] in \CHOIX[\H;\rep[\H;]])}
   \text{test3 = wims(listcomplement \CHOIX[\H;\rep[\H;]] in \REP[\H;])}
  %%% \integer{test4=items(\REP[\H;]) - items(\CHOIX[\H;])}
   \text{test_cnt=\test1, items(\test2),items(\test3)}
   \integer{cnt_juste= \test_cnt[1]+\test_cnt[2]+\test_cnt[3] =0 ? \cnt_juste + 1}
   \matrix{TEST=\TEST
   \test_cnt}
}

test1 = 0 rep < juste
test2 nombre de r�ponses dites justes et en fait fausses
test3 nombre de r�ponses dites fausses et en fait justes

totalement justes : test1=0, test2=0 test3=0
partiellement justes :

\real{v=\cnt_juste/\CNT[\step-1;\N]}

\text{nstep = \v >= \percent ? \STEPS[\step;]:}

\condition{Question 1 : \REP1}{\TEST[1;3]=0}{option=hide}
\condition{Question 1 : \REP1}{\TEST[1;1]=0 and \TEST[1;2]=0 and \TEST[1;3]=0}{option=hide}
\condition{Question 2 : \REP2}{\TEST[2;3]=0 and \step >=\CONDSTEP[2]}{option=hide}
\condition{Question 2 : \REP2}{\TEST[2;1]=0 and \TEST[2;2]=0 and \TEST[2;3]=0 and \step >=\CONDSTEP[2]}{option=hide}
\condition{Question 3 : \REP3}{\TEST[3;3]=0 and \step >=\CONDSTEP[3]}{option=hide}
\condition{Question 3 : \REP3}{\TEST[3;1]=0 and \TEST[3;2]=0 and \TEST[3;3]=0 and \step >=\CONDSTEP[3]}{option=hide}
\condition{Question 4 : \REP4}{\TEST[4;3]=0 and \step >=\CONDSTEP[4]}{option=hide}
\condition{Question 4 : \REP4}{\TEST[4;1]=0 and \TEST[4;2]=0 and \TEST[4;3]=0 and \step >=\CONDSTEP[4]}{option=hide}
\condition{Question 5 : \REP5}{\TEST[5;3]=0 and \step >=\CONDSTEP[5]}{option=hide}
\condition{Question 5 : \REP5}{\TEST[5;1]=0 and \TEST[5;2]=0 and \TEST[5;3]=0 and \step >=\CONDSTEP[5]}{option=hide}
\condition{Question 6 : \REP6}{\TEST[6;3]=0 and \step >=\CONDSTEP[6]}{option=hide}
\condition{Question 6 : \REP6}{\TEST[6;1]=0 and \TEST[6;2]=0 and \TEST[6;3]=0 and \step >=\CONDSTEP[6]}{option=hide}
\condition{Question 7 : \REP7}{\TEST[7;3]=0 and \step >=\CONDSTEP[7]}{option=hide}
\condition{Question 7 : \REP7}{\TEST[7;1]=0 and \TEST[7;2]=0 and \TEST[7;3]=0 and \step >=\CONDSTEP[7]}{option=hide}
\condition{Question 8 : \REP8}{\TEST[8;3]=0 and \step >=\CONDSTEP[8]}{option=hide}
\condition{Question 8 : \REP8}{\TEST[8;1]=0 and \TEST[8;2]=0 and \TEST[8;3]=0 and \step >=\CONDSTEP[8]}{option=hide}
\condition{Question 9 : \REP9}{\TEST[9;3]=0 and \step >=\CONDSTEP[9]}{option=hide}
\condition{Question 9 : \REP9}{\TEST[9;1]=0 and \TEST[9;2]=0 and \TEST[9;3]=0 and \step >=\CONDSTEP[9]}{option=hide}
\condition{Question 10 : \REP10}{\TEST[10;3]=0 and \step >=\CONDSTEP[10]}{option=hide}
\condition{Question 10 : \REP10}{\TEST[10;1]=0 and \TEST[10;2]=0 and \TEST[10;3]=0 and \step >=\CONDSTEP[10]}{option=hide}


\text{test=wims(rows2lines \explication)}
\text{test=wims(lines2items \test)}
\text{test=wims(items2words \test)}
\text{test=wims(nospace \test)}
feedback{1=1}{\explication
   \if{\test notsametext}{
     <div class="reponse"><ol>
      \for{w = 1 to \MAX}{
        \if{\explication[\w;] notsametext }
         {<li style="list-style:decimal;" value="\w">\explication[\w;] </li>}
     }
     </ol>
    </div>
  }
}
{<ol>
  \for{ t = 1 to \N}{
    \if{\CNT[\step;\t] != }{
  <li style="list-style:decimal;" value="\CNT[\step;\t]"> <b>\question[\N*(\step-1) + \t;]</b>
  <div class="question"><ol>
   \for{s=1 to \CNT_choix[\N*(\step-1) + \t]}{
   <li>\embed{\STEPS[\step;\t] , \s }</li>
    }
   </ol>
   </div>
   </li>}
 }
</ol>
}

\latex{
\begin{statement}
\instruction
\begin{enumerate}
\latexsrc
\end{enumerate}
\end{statement}
\begin{solution}\
\begin{enumerate}
\latexsol
\end{enumerate}
\end{solution}
}
