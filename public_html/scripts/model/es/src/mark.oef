type=select
textarea="data pre post"
iEdit="pre post"

:Haga clic en las palabras para resaltarlas en un texto.

El ejercicio presenta un texto donde las palabras son "marcables" con el mouse. Hay que
seleccionar algunas de acuerdo con las instrucciones del ejercicio.
<p>
Para construir un ejercicio con este modelo, solo ingrese los textos.
</p><p class="wims_credits">
Autor del modelo: <a href="mailto:qualite@wimsedu.info">Gang Xiao</a>
</p>
:%%%%%%%%%%%%%%%%%      ATENCI�N      %%%%%%%%%%%%%%%%%%%%

Elimina la cabecera anterior si borra las etiquetas para el modelo!
(Son las l�neas que comienzan con un ':'.)
En caso contrario el ejercicio puede no funcionar bajo Createxo.

:%%%%%%%% Par�metros del ejemplo a redefinir %%%%%%%%%%%%%%%%%

:\title{Seleccionar palabras en un texto}
:\author{XIAO, Gang}
:\email{qualite@wimsedu.info}
:\credits{}

:Cantidad m�nima de palabras a marcar en cada ejercicio. Al menos 1.
\integer{minmark=1}

:Cantidad m�xima de palabras a marcar en cada ejercicio.
\integer{maxmark=8}

:Datos principales
Escribir una pregunta por l�nea y separar las l�neas
por punto y coma. Por tanto los textos en s� no pueden contener
punto y coma.
<p>Los campos a marcar deben colocarse entre un par de dobles
signos de interrogaci�n "??". Cada campo contiene una lista de elementos separados
por comas.</p>
El primer elemento antes de la coma es
<ul><li>una palabra err�nea,</li><li> una lista de palabras err�neas
puestas entre llaves</li><li>o puede dejarse vac�o.
</li></ul>
El segundo elemento es la palabra correcta.
<pre>
El gato ??comes,come?? rat�n.
El gato ??{comen,comme},come?? rat�n.
El gato ??,come?? rat�n.
</pre>

Estas palabras se escoger�n al azar antes de la visualizaci�n de
la frase.
<p>
Hasta cien puntos de marcado
se puede poner en cada oraci�n.</p>
$embraced_randitem

\text{data=
{Jack,Jean,Louis,Michel,Pierre} ??est�,es?? extranjero, el
??{ten,tenga},tiene?? ??uno,un?? caba�a de tiro ??a,al?? blanco.
Se ??encontramos,encuentran??  ??caba�a,caba�as?? de tiro al blanco en
??todos,todas?? las ferias. ??Las gentes,La gente?? ??llegan,llega??,
??dan,da?? ??,dinero??
y ??recibo,recibe?? ??,un rifle de municiones?? para tirar sobre
??,figuras?? {colocada,puesta},colocadas?? sobre estantes.
;

??{Quel,Quels,Quelles},Quelle??
id�e ??est-je,ai-je??
??{d'achet�,d'achet�e,d'achet�s},d'acheter?? ??{cept,cette,ces,ce},cet??
oiseau ? L'oiselier me dit : "??{S'est,Cet},C'est?? un ??{mal,malle},m�le??.
??Attender,Attendez?? une ??,semaine?? qu'il
??{s'abitue,s'abituent,s'habituent},s'habitue??, ??est,et?? il chantera".
??Hors,Or??, l'oiseau ??sobstine,s'obstine?? ??a,�?? ??ce,se??
??tait,taire?? et il ??fais,fait?? ??tous,tout?? de ??{traver,travert},travers??.
;

Les ??d�sert,d�serts?? de ??sables,sable?? ??occupe,occupent?? de
??large,larges?? parties {de la plan�te,du monde,de la Terre}.
Il n'y ??{pleu,pleus,pleuvent},pleut??
presque ??,pas??. Tr�s ??peut,peu?? de plantes et ??,d'animaux?? y
??vit,vivent??. Les ??,dunes?? ??son,sont?? des collines de
??,sable?? que le vent ??�,a??
??{construit,construits,construite},construites??. Les
??grains,graines?? de ??{certain,certaine,certains},certaines??
plantes ??reste,restent?? sous le ??sole,sol?? du d�sert pendant
des ann�es. ??{Ils,Elle},Elles?? ??ce,se?? ??met,mettent??
??a,�?? ??{pouss�es,pouss�e,pouss�s},pousser?? d�s qu'il y a
??une,un?? orage.
;

}

:Texto mostrado antes de la pregunta. Etiquetas HTML permitidas. Puede estar vac�o.
$embraced_randitem
\text{pre=
Marquez les fautes d'orthographe dans la phrase ci-dessous.
}

:Texto que se muestra despu�s de la pregunta. Etiquetas HTML permitidas. Puede estar vac�o.
\text{post=}

:Se acepta texto aleatorio puesto entre llaves
$embraced_randitem
\text{accolade=item(1,1 s�,
2 no)}

:Opciones.
Agregue la palabra  <span class="tt wims_code_words">split</span> a la definici�n si desea dar una nota parcial
para respuestas parcialmente correctas.
\text{option=split}

:%%%%%%%%%%%%%% Nada que modificar hasta el enunciado %%%%%%%%%%%%%%%%

\text{accolade=wims(word 1 of \accolade)}
\text{data=wims(singlespace \data)}
\text{data=wims(nonempty rows \data)}
\text{data=randomrow(\data)}
\text{data=\accolade=1 ? wims(embraced randitem \data)}
\text{data=slib(text/cutchoice2 \data)}
\integer{qs=floor(rows(\data)/2)}
\text{list=}
\text{ind=}
\for{i=1 to \qs}{
 \text{d=\data[2*\i;]}
 \text{list=\list \d;}
 \if{\d[1] issametext }{\text{ind=\ind[]2,}}{\text{ind=\ind[]1,}}
}

\text{can=positionof(1,\ind)}
\text{cancnt=items(\can)}
\integer{tomark=randint(min(\minmark,\cancnt)..min(\maxmark,\cancnt))}
\text{can=\cancnt>1?shuffle(\can)}
\text{good=item(1..\tomark,\can)}
\text{good=wims(sort numeric items \good)}

\text{show=}
\for{i=1 to \qs}{
 \text{d=\i isitemof \good?\list[\i;1]:\list[\i;2]}
 \text{show=\show[]\d,}
}

\text{pre=\accolade=1 ? wims(embraced randitem \pre)}

:%%%%%%%%%%%%% Maintenant l'�nonc� en code html. %%%%%%%%%%%%%%%%%%%%

::Vous n'avez pas besoin de modifier ceci en g�n�ral.

\statement{<div>
\pre
</div>
\data[1;]
\for{k=1 to \qs}{
\embed{r1,\k} \data[2*\k+1;]
}
\post
}

:%%%%%%%%%%%%% Rien � modifier ci-apr�s. %%%%%%%%%%%%%%%%%

\answer{Champ 1}{\good;\show}{type=mark}{option=\option}
