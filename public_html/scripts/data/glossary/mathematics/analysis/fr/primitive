!!abstract,linked gloses,internal links,content,dynamic examples,...
!set gl_author=Euler, Acad�mie de Versailles
!set gl_keywords=integral,derivative,primitive
!set gl_title=Primitive d'une fonction num�rique
!set gl_level=H6 STI2D&nbsp;STL&nbsp;Sp�cialit�, H6 G�n�rale&nbsp;Sp�cialit�, H6 G�n�rale&nbsp;Compl�mentaire 
:
:
:
:
<div class="wims_defn">
<h4>D�finition</h4>
Soit \(f\) une fonction d�finie sur un intervalle
<span class="nowrap">\(\mathrm{I}\).</span><br>
On appelle <strong>primitive</strong> de \(f\) sur \(\mathrm{I}\) toute fonction
\(F\) d�rivable sur \(\mathrm{I}\) telle que pour tout
<span class="nowrap">\(x \in \mathrm{I}\),</span>
<span class="nowrap"> \(\displaystyle{F^{'}(x)=f(x)}\).</span>
</div>
:
:
<div class="wims_thm">
<h4>Th�or�me</h4>
Toute fonction \(f\) continue sur un intervalle \(\mathrm{I}\) admet une
primitive sur <span class="nowrap">\(\mathrm{I}\).</span>
</div>
:
:
<div class="wims_thm">
<h4>Th�or�me</h4>
Soit \(f\) une fonction continue sur un intervalle \(\mathrm{I}\) et \(F\) une
primitive de \(f\) sur <span class="nowrap">\(\mathrm{I}\).</span>
<ul>
<li>Soit \(k\in \displaystyle{\RR }\).
La fonction \(G\) d�finie pour tout \(x \in \mathrm{I}\)
par \(G(x)=F(x)+k\) est une primitive de \(f\) sur
<span class="nowrap">\(\mathrm{I}\).</span>
</li>
<li>
Pour toute primitive \(G\) de \(f\) sur <span class="nowrap">
\(\mathrm{I}\),</span> il existe un r�el \(k\) tel que, pour tout
<span class="nowrap">\(x \in \mathrm{I}\),</span>
<span class="nowrap">\(G(x) =F(x) + k \).</span>
</li>
</ul>
</div>
:
:
<div class="wims_thm">
<h4>Th�or�me</h4>
Soit \(f\) une fonction continue sur un intervalle
<span class="nowrap">\(\mathrm{I}\).</span><br>
Soit \(x_0 \in \mathrm{I}\) et <span class="nowrap">\(y_0 \in \RR \).</span><br>
Il existe une unique primitive \(F\) de \(f\) sur \(\mathrm{I}\) telle que
<span class="nowrap">\(F(x_0)= y_0\).</span>

</div>
