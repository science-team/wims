!!abstract,linked gloses,internal links,content,dynamic examples,...
!set gl_author=Euler, Acad�mie de Versailles
!set gl_keywords=derivative,real_function
!set gl_title=Produit de deux fonctions d�rivables
!set gl_level=H5 G�n�rale&nbsp;et&nbsp;Technologique 
:
:
:
:
<div class="wims_thm">
<h4>Th�or�me</h4>
Soit \(f\) et \(g\) deux fonctions d�finies sur un intervalle \(\mathrm{I}\) et
\(h\) la fonction d�finie sur \(\mathrm{I}\) par
<span class="nowrap">\(h=f \times g \).</span><br>
Si \(f\) et \(g\) sont d�rivables sur <span class="nowrap">\(\mathrm{I}\),</span> alors la fonction \(h\) est d�rivable sur <span class="nowrap">\(\mathrm{I}\),</span> et sur
<span class="nowrap">\(\mathrm{I}\) :</span><br>
<div class="wimscenter">
 \(\displaystyle{h^{'} = f^{'} g + f g^{'}}\)
</div>
c'est-�-dire, pour tout <span class="nowrap">\(x\in\mathrm{I}\),</span>
<div class="wimscenter">
\(\displaystyle{h^{'}(x) = f^{'}(x) \times g(x) + f(x) \times g^{'}(x)}\)
</div>
</div>
:
:
<div class="wims_thm">
<h4>Th�or�me</h4>
Soit \(f\) une fonction d�finie sur un intervalle <span class="nowrap">\(\mathrm{I}\),</span> \(k\) un nombre
r�el et \(g\) la fonction d�finie sur \(\mathrm{I}\) par
<span class="nowrap">\(g=k \times f\).</span><br>
Si \(f\) est d�rivable sur \(\mathrm{I}\), alors \(g\) est d�rivable sur
<span class="nowrap">\(\mathrm{I}\),</span> et sur
<span class="nowrap">\(\mathrm{I}\) :</span>
<div class="wimscenter">
\(\displaystyle{g^{'} = k \times f^{'}}\)
</div>
c'est-�-dire, pour tout <span class="nowrap">\(x\in\mathrm{I}\),</span>
<div class="wimscenter">
\(\displaystyle{g^{'}(x) = k \times f^{'}(x)}\)
</div>
</div>
