!!abstract,linked gloses,internal links,content,dynamic examples,...
!set gl_author=Euler, Acad�mie de Versailles
!set gl_keywords=real_number,inequalities
!set gl_title=Arrondi (lyc�e)
!set gl_level=H4
:
:
:
:
<div class="wims_defn">
  <h4>D�finition</h4>
Soit \(x\) un nombre r�el et \(n\) un entier naturel.
<ul>
<li>Si \(x\) est positif, on appelle <strong>arrondi</strong> (ou <strong>valeur
arrondie</strong>) de \(x\) � \(10^{-n}\) le nombre d�cimal \(a\) tel que
\(a \times 10^{n}\) est entier et
<span class="nowrap">
\(a-5 \times 10^{-n-1} \leqslant x \lt a + 5 \times 10^{-n-1}\).</span>
</li>
<li>Si \(x\) est n�gatif, on appelle <strong>arrondi</strong> (ou <strong>valeur
arrondie</strong>) de \(x\) � \(10^{-n}\) le nombre d�cimal \(a\) tel que
\(a \times 10^{n}\) est entier et <span class="nowrap">
\(a-5 \times 10^{-n-1} \lt x \leqslant a + 5 \times 10^{-n-1} \).</span>
</li>
</ul>
</div>
:
:
<div class="wims_rem">
<h4>Remarques</h4>
<ul>
<li>L'arrondi \(a\) de \(x\) � \(10^{-n}\) est une valeur approch�e de \(x\) �
\(10^{-n}\) pr�s.<br>
Si \(x\) est positif, cette valeur approch�e est par d�faut lorsque la
<span class="nowrap">(\(n\)+1)-i�me</span> d�cimale de \(x\) est
0, 1, 2, 3 ou 4, par exc�s lorsque cette d�cimale est 5, 6, 7, 8 ou 9.<br>
Si \(x\) est n�gatif, cette valeur approch�e est par exc�s lorsque la
<span class="nowrap">(\(n\)+1)-i�me</span> d�cimale de \(x\) est
0, 1, 2, 3 ou 4, par d�faut lorsque cette d�cimale est 5, 6, 7, 8 ou 9.
</li>
<li>
Lorsque \(n = 0\), \(a\) est l'arrondi de \(x\) � l'unit�.<br>
lorsque \(n = 1\), \(a\) est l'arrondi de \(x\) au dixi�me.<br>
lorsque \(n = 2\), \(a\) est l'arrondi de \(x\) au centi�me.<br>
lorsque \(n = 3\), \(a\) est l'arrondi de \(x\) au milli�me&#8230;
</li>
</ul>
</div>
