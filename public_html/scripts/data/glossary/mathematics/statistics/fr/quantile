!!abstract,linked gloses,internal links,content,dynamic examples,...
!set gl_author=Euler, Acad�mie de Versailles
!set gl_keywords=descriptive_statistics,quantile
!set gl_title=Quantile
!set gl_level=H5
:
:
:
:
<div class="wims_defn"><h4>D�finition</h4>
Soit \(S=\{a_i\}_{1 \leqslant i \leqslant n}\) une s�rie statistique �
une variable quantitative discr�te de taille \( n\in\NN^* \) ordonn�e dans
l'ordre croissant.<br>
La fonction <strong>quantile</strong> \(\mathrm{Q}\) est la fonction d�finie sur
<span class="nowrap">\(\left\lbrack \frac{1}{n} ; 1 \right\rbrack\),
</span> qui, � tout r�el <span class="nowrap">
\(x \in \left\lbrack \frac{1}{n} ; 1 \right\rbrack\),</span> associe le terme
\(a_i\) de \(S\) dont l'indice \(i\) est le plus petit entier sup�rieur
ou �gal � <span class="nowrap">\(n x\).</span></div>
:
:
<div class="wims_rem"><h4>Exemples</h4>
<ul>
<li>Les trois <strong>quartiles</strong> d'une s�rie \(S\) comprenant au
moins 4 termes sont donn�s respectivement par <span class="nowrap">
\(\mathrm{Q}(0,25)\),</span> \(\mathrm{Q}(0,5)\) et
<span class="nowrap">\(\mathrm{Q}(0,75)\) ;</span></li>
<li>
les neuf <strong>d�ciles</strong> d'une s�rie \(S\) comprenant au moins
10 termes sont donn�s par \(\mathrm{Q}\left(\frac{i}{10}\right)\)
o� <span class="nowrap">\(i \in \{1;2;\ldots;9\}\) ;</span>
</li>
<li>
les 99 <strong>centiles</strong> d'une s�rie \(S\) comprenant au moins 100 termes sont
donn�s par \(\mathrm{Q}\left(\frac{i}{100}\right)\)
o� <span class="nowrap">\(i \in \{1;2;\ldots;99\}\).</span>
</li>
</ul>
</div>
