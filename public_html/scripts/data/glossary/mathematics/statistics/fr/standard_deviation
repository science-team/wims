!!abstract,linked gloses,internal links,content,dynamic examples,...
!set gl_author=Euler, Acad�mie de Versailles
!set gl_keywords=descriptive_statistics,standard_deviation
!set gl_title=�cart type
!set gl_level=H4
:
:
:
:
<div class="wims_defn"><h4>D�finition</h4>
<p>Soit  une s�rie statistique prenant pour valeurs les r�els <span class="nowrap">\(x_1\),</span> <span class="nowrap">\(x_2\),</span> ..., \(x_p\) d'effectifs respectifs <span class="nowrap">\(n_1\),</span> <span class="nowrap">\(n_2\),</span> ..., <span class="nowrap">\(n_p\),</span> d'effectif total \(N\) et de moyenne arithm�tique
<span class="nowrap">\( \overline{x}\).</span><br>
</p>
<table class="wimscenter wimsborder" >
<tr>
<th style="background-color:#c8c3c3;">Valeurs</th>
<td>\(x_1\)</td>
<td>\(x_2\)</td>
<td>...</td>
<td>\(x_p\)</td>
</tr>
<tr>
<th style="background-color:#c8c3c3;">Effectifs</th>
<td>\(n_1\)</td>
<td>\(n_2\)</td>
<td>...</td>
<td>\(n_p\)</td>
</tr>
</table>
L'<strong>�cart type</strong> de cette s�rie est le nombre r�el positif
\(\displaystyle{\sigma}\) d�fini par&nbsp;: 
<div class="wimscenter unbreakable">
<span class="nowrap">\(\displaystyle{\sigma =\sqrt{
\frac{1}{N}\left(n_1(x_1 - \overline{x})^2 +
n_2(x_2 - \overline{x})^2 + \ldots + n_p(x_p  - \overline{x})^2\right)}}\).</span>
</div>

</div>
:
:
<div class="wims_rem"><h4>Remarque</h4>
L'�cart type est un <em>indicateur de dispersion associ� � la moyenne</em>&nbsp;: plus l'�cart type est grand plus les valeurs sont dispers�es autour de la moyenne.

</div>
