!set gl_type=dynamic
!set gl_author=Euler, Acad�mie de Versailles
!set gl_title=Th�or�me de Thal�s (Exemple 3)
!set gl_renew=1

!readproc data/glossary/mathematics/geometry/macro/thales_gen 4
<style>
/*<![CDATA[*/
#enonce$(gl_a){order:1;}
#applet$(gl_a){order:2;}

@media screen and (max-width: 40em) {
#enonce$(gl_a){order:2;}
#applet$(gl_a){order:1;}

}
div.applet{padding-left:1.5em;}
details{box-shadow:none}
/*]]>*/
</style>

<div>
  <p>Les droites \((\mathrm{BM})\) et \((\mathrm{CN})\) sont s�cantes en <span class="nowrap">\(\mathrm{A}\).</span><br> Une unit� de longueur �tant donn�e, on a&nbsp;: <span class="nowrap">\($gl_quest1\).</span>
  </p>
  <p>
  D�montrer que les droites \((\mathrm{BC})\) et \((\mathrm{MN})\) sont parall�les.
  </p>
</div>
<div class="grid-container fluid">
  <div class="grid-x grid-padding-x">
    <div id="enonce$(gl_a)" class="cell2 small-12 medium-6 large-6">

      <details>
        <summary class="oef_indgood">�l�ments de solution</summary>
        <div>
        Les droites \((\mathrm{BM})\) et \((\mathrm{CN})\) sont s�cantes en <span class="nowrap">\(\mathrm{A}\).</span><br>
        Avec les donn�es de l'�nonc�, on a&nbsp;:
        <ul class="wims_nopuce">
          <li>\($(gl_frac1[1]) = $(gl_frac2[1;]) \) soit \($(gl_frac1[1]) = $gl_rapAf \)</li>
          <li>\($(gl_frac1[2]) = $(gl_frac2[2;]) \) soit \($(gl_frac1[2]) = $gl_rapAf \)</li>
          <li>Donc <span class="nowrap">\($(gl_frac1[1]) = $(gl_frac1[2])\).</span></li>
        </ul>
        </div>
        <div class="spacer">
        De plus, les points <span class="nowrap">\(\mathrm{A}\),</span> \(\mathrm{B}\) et \(\mathrm{M}\) sont align�s dans le m�me ordre que les points <span class="nowrap">\(\mathrm{A}\),</span> \(\mathrm{C}\) et <span class="nowrap">\(\mathrm{N}\).</span>
        </div>
        <div class="spacer">
        Donc d'apr�s la r�ciproque du th�or�me de Thal�s, les droites \((\mathrm{BC})\) et \((\mathrm{MN})\) sont parall�les.
        </div>
      </details>
    </div>
    <div id="applet$(gl_a)" class="cell2 small-12 medium-6 large-6">
      <div class="applet">
        !readproc slib/geo2D/jsxgraph id$(gl_a) brd$(gl_a),[$gl_xsize x $gl_ysize,min=250px max=350px scroll center],$(gl_script$(gl_a))
        $slib_out
      </div>
    </div>
  </div>
</div>