!!abstract,linked gloses,internal links,content,dynamic examples,...
!set gl_author=Euler, Acad�mie de Versailles
!set gl_keywords=
!set gl_title=Coordonn�es d'un point sur une sph�re (coll�ge)
!set gl_level=H3
:
:
:
:
<div class="wims_prems"><h4>Premi�re approche</h4>
On assimile la surface de la Terre � une sph�re sur laquelle on d�finit des cercles ou demi-cercles imaginaires.
<ul>
  <li>
L'<strong>�quateur</strong> est le cercle obtenu par la section de la sph�re par le plan passant par le centre de la sph�re et perpendiculaire � l'axe des p�les.<br>
Les <strong>parall�les</strong> sont les cercles de la sph�re parall�les � l'�quateur.<br>
  </li>
  <li>Le <strong>m�ridien de Greenwich</strong> est le demi-cercle d'extr�mit�s les deux p�les et passant par l'observatoire de Greenwich.<br>
Les <strong>m�ridiens</strong> g�ographiques sont les demi-cercles d'extr�mit�s les deux p�les.
 </li>
</div>
:
:
<div class="wims_rem"><h4>Remarques</h4>
Soit \(\mathrm{O}\) le centre de la sph�re terrestre.
<ul>
  <li>Tout parall�le est identifi� par&nbsp;:
    <ul>
      <li> son h�misph�re (Nord ou Sud)&nbsp;;</li>
      <li>sa position par la mesure en degr� de l'angle \(\widehat{\mathrm{EOP}}\) o� \(\mathrm{E}\) est un point de l'�quateur et \(\mathrm{P}\) le point d'intersection entre le parall�le et le m�ridien passant par <span class="nowrap">\(\mathrm{E}\).</span></li>
   </ul></li>
 <li>Tout m�ridien est identifi� par&nbsp;:
   <ul>
     <li> sa position par rapport au m�ridien de Greenwich (Est ou Ouest)&nbsp;;</li>
     <li>par la mesure en degr� de l'angle \(\widehat{\mathrm{GOM}}\) o� \(\mathrm{M}\) est un point de ce m�ridien et \(\mathrm{G}\) est le point  d'intersection entre le m�ridien de Greenwich et le parall�le passant par <span class="nowrap">\(\mathrm{M}\).</span> </li>
  </ul></li>
</ul>
</div>
:
:
<div class="wims_defn"><h4>D�finition</h4>
On appelle <strong>latitude</strong> d�un point \(\mathrm{M}\) la mesure en degr� de l�angle associ� au parall�le sur lequel se trouve le point \(\mathrm{M}\) en pr�cisant l'h�misph�re (Nord ou Sud) auquel il appartient.
</div>
:
:
<div class="wims_defn"><h4>D�finition</h4>
On appelle <strong>longitude</strong> d�un point \(\mathrm{M}\) la mesure en degr� de l�angle associ� au m�ridien sur lequel se trouve le point \(\mathrm{M}\) en pr�cisant sa position par rapport  au m�ridien de Greenwich (Est ou Ouest).
</div>
:
:
<div class="wims_defn"><h4>D�finition</h4>
On appelle <strong>coordonn�es g�ographiques</strong> d�un point de la sph�re terrestre le couple \((x\,;y)\) o� \(x\) est la latitude et \(y\) est la longitude de ce point.  
</div>