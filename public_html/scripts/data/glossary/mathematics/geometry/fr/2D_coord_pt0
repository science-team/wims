!!abstract,linked gloses,internal links,content,dynamic examples,...
!set gl_author=Euler, Acad�mie de Versailles
!set gl_keywords=
!set gl_title=Coordonn�es d'un point du plan (coll�ge)
!set gl_level=H1 Cycle&nbsp;4
:
:
:
:
<div class="wims_defn"><h4>D�finitions</h4>
Un <strong>rep�re orthogonal</strong> du plan est d�fini par un point \(\mathrm{O}\) et par deux droites gradu�es perpendiculaires de m�me origine le point <span class="nowrap">\(\mathrm{O}\).</span><br>
Le point \(\mathrm{O}\) est appel� origine du rep�re.<br>
L'une des deux droites est appel�e <strong>axe des abscisses</strong>, l'autre, <strong>axe des ordonn�es</strong>.
</div>
:
:
<div class="wims_rem"><h4>Remarque</h4>
L'axe des abscisses est g�n�ralement repr�sent� horizontalement et orient� de gauche � droite, l'axe des ordonn�es est alors repr�sent� verticalement et orient� de bas en haut.
</div>
:
:
<div class="wims_defn"><h4>D�finition</h4>
Soit un rep�re du plan d'origine un point <span class="nowrap">\(\mathrm{O}\).</span><br>
Soit un point \(\mathrm{M}\) du plan.<br>
La droite parall�le � l'axe des ordonn�es passant \(\mathrm{M}\) coupe l'axe des abscisses en un point \(\mathrm{H}\) rep�r� par un nombre \(x\) sur cet axe&nbsp;; la droite parall�le � l'axe des abscisses passant \(\mathrm{M}\) coupe l'axe des ordonn�es en un point \(\mathrm{K}\) rep�r� par un nombre \(y\) sur cet axe.<br>
Le couple \(\left(x\,;y\right)\) est appel� couple de <strong>coordonn�es</strong> du point <span class="nowrap">\(\mathrm{M}\).</span><br>
Le nombre \(x\) est appel� <strong>abscisse</strong> du point <span class="nowrap">\(\mathrm{M}\).</span><br>
Le nombre \(y\) est appel� <strong>ordonn�e</strong> du point <span class="nowrap">\(\mathrm{M}\).</span><br>
</div>
