!!abstract,linked gloses,internal links,content,dynamic examples,...
!set gl_author=Euler, Acad�mie de Versailles
!set gl_keywords=
!set gl_title=Produit scalaire de deux vecteurs du plan
!set gl_level=H5 
:
:
:
:
<div class="wims_defn"><H4>D�finition</H4>
Soit \(\overrightarrow{u}\) et \(\overrightarrow{v}\) deux vecteurs du plan et soit <span class="nowrap">\(\mathrm{A}\),</span> \(\mathrm{B}\) et \(\mathrm{C}\) des points du plan tels que \(\overrightarrow{\mathrm{AB}}=\overrightarrow{u}\) et <span class="nowrap">\(\overrightarrow{\mathrm{AC}}=\overrightarrow{v}\).</span> <br>
Le <strong>produit scalaire</strong> du vecteur \(\overrightarrow{u}\) par le vecteur \(\overrightarrow{v}\) est le <em>r�el</em> not� \(\overrightarrow{u}\cdot\overrightarrow{v}\) d�fini par :
<ul>
<li> \(\overrightarrow{u}\cdot\overrightarrow{v}=\mathrm{AB} \times \mathrm{AC} \times \cos{\left(\widehat{\mathrm{BAC}}\right)}\) si les vecteurs \(\overrightarrow{u}\) et \(\overrightarrow{v}\)  sont des vecteurs non nuls&nbsp;;
</li>
<li>
 \(\overrightarrow{u}\cdot\overrightarrow{v}=0\) si  \(\overrightarrow{u}\) ou \(\overrightarrow{v}\)  est le vecteur nul.
</li>
</ul>

</div>
:
:
<div class="wims_thm"><H4>Th�or�me</H4>
Soit <span class="nowrap">\(\mathrm{A}\),</span> \(\mathrm{B}\) et \(\mathrm{C}\) des points du plan tels que \(\mathrm{A}\) soit diff�rent de <span class="nowrap">\(\mathrm{B}\).</span>
On note \(\mathrm{H}\) le projet� orthogonal du point \(\mathrm{C}\) sur la droite <span class="nowrap">\((\mathrm{AB})\).</span> <br> 
On a alors&nbsp;:  \( \overrightarrow{\mathrm{AB}}\cdot\overrightarrow{\mathrm{AC}}=\overrightarrow{\mathrm{AB}}\cdot\overrightarrow{\mathrm{AH}}\).

<div class="spacer"><strong>Remarque</strong>
<ul>
<li>
Si les vecteurs \(\overrightarrow{\mathrm{AB}}\) et \(\overrightarrow{\mathrm{AH}}\) sont de m�me sens alors <span class="nowrap">\(\overrightarrow{\mathrm{AB}}\cdot\overrightarrow{\mathrm{AC}}=\mathrm{AB}\times \mathrm{AH}\) ;</span>
</li>
<li>
si les vecteurs \(\overrightarrow{\mathrm{AB}}\) et \(\overrightarrow{\mathrm{AH}}\) sont de sens oppos� alors <span class="nowrap">\(\overrightarrow{\mathrm{AB}}\cdot\overrightarrow{\mathrm{AC}}= - \mathrm{AB}\times \mathrm{AH}\).</span>
</li>
</ul>
</div>

</div>
:
:
<div class="wims_thm"><H4>Propri�t�s</H4>
Pour tous vecteurs du plan \(\overrightarrow{u}\), \(\overrightarrow{v}\) et \(\overrightarrow{w}\) et pour tout r�el \(a\) on a&nbsp;:<br>
<ul>
<li>
\(\overrightarrow{u}\cdot\overrightarrow{v}=\overrightarrow{v}\cdot\overrightarrow{u}\) (sym�trie)&nbsp;;
</li>
<li>
\(\left(a\overrightarrow{u}\right)\cdot\overrightarrow{v}=\overrightarrow{u}\cdot\left(a\overrightarrow{v}\right)=a\times \left(\overrightarrow{u}\cdot\overrightarrow{v}\right)\)&nbsp;;
</li>
<li>
\(\overrightarrow{u}\cdot\left(\overrightarrow{v}+\overrightarrow{w}\right)=\overrightarrow{u}\cdot\overrightarrow{v}+\overrightarrow{u}\cdot\overrightarrow{w}\).
</li>
</ul>

</div>
:
:
<div class="wims_thm"><H4>Th�or�me</H4>
Le plan est muni d'une base orthonorm�e. <br>
Soit \(\overrightarrow{u}\) et \(\overrightarrow{v}\) deux vecteurs du plan de coordonn�es respectives \((x\,;y)\) et \((x^{'}\,;y^{'})\) dans cette base. On a alors&nbsp;:<br>
\(\overrightarrow{u}\cdot\overrightarrow{v}=x x^{'} + y y^{'}\).

</div>

