!!abstract,linked gloses,internal links,content,dynamic examples,...
!set gl_author=Euler, Acad�mie de Versailles
!set gl_keywords=line_equation,analytic_geometry,direction
!set gl_title=Coefficient directeur d'une droite
!set gl_level=H4 
:
:
:
:
Le plan est muni d'un rep�re.

<div class="wims_thm spacer"><h4>Th�or�me</h4>
Soit \(\mathcal{D}\) une droite du plan non parall�le � l'axe des ordonn�es.<br>
Il existe un unique couple de r�els \((m,p)\) tel que la droite \(\mathcal{D}\)
ait pour �quation r�duite <span class="nowrap">\(y=m x + p\).</span>
</div>
:
:
<div class="wims_defn"><h4>D�finition</h4>
Le r�el \(m\) est appel� <strong>coefficient directeur</strong> de la droite
<span class="nowrap">\(\mathcal{D}\).</span></div>
:
:
<div class="wims_thm"><h4>Th�or�me</h4>
Soit \(\mathcal{D}\) et \(\mathcal{D}^{'}\) deux droites du plan non parall�les �
l'axe des ordonn�es.<br>
\(\mathcal{D}\) et \(\mathcal{D}^{'}\) sont parall�les si et seulement si elles ont
le m�me coefficient directeur.</div>
:
:
<div class="wims_thm"><h4>Th�or�me</h4>
Soit \(\mathcal{D}\) une droite du plan non parall�le � l'axe des ordonn�es, et
\(\mathrm{A}\) et \(\mathrm{B}\) deux points distincts de \(\mathcal{D}\) de coordonn�es respectives
\((x_\mathrm{A}\,;y_\mathrm{A})\) et <span class="nowrap">\((x_\mathrm{B}\,;y_\mathrm{B})\).</span><br>
Le coefficient directeur de la droite \(\mathcal{D}\) est le r�el \(m\) tel
que&nbsp;:
<div class="wimscenter">
\(m=\frac{y_\mathrm{B}-y_\mathrm{A}}{x_\mathrm{B}-x_\mathrm{A}}\)

</div>
</div>
