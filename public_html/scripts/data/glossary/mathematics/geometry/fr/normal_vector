!!abstract,linked gloses,internal links,content,dynamic examples,...
!set gl_author=Euler, Acad�mie de Versailles
!set gl_keywords=vectors,analytic_geometry,line_equation,normal_vector
!set gl_title=Vecteur normal � une droite du plan
!set gl_level=H5 G�n�rale
:
:
:
:
<div class="wims_defn"><h4>D�finition</h4>
Soit \(\mathcal{D}\) une droite du plan.<br>
On appelle <strong>vecteur normal</strong> � \(\mathcal{D}\) tout vecteur
directeur d'une droite perpendiculaire � <span class="nowrap">
\(\mathcal{D}\).</span></div>
:
:
<div class="wims_rem"><h4>Remarque</h4>
Tout vecteur non nul colin�aire � un vecteur normal d'une droite est �galement un vecteur normal de cette droite.
</div>
:
:
<div class="wims_thm"><h4>Th�or�me</h4>
Soit \(\mathrm{A}\) un point et \(\overrightarrow{v}\) un vecteur non nul du
plan.<br>
La droite \(\mathcal{D}\) passant par \(\mathrm{A}\) et de vecteur normal
\(\overrightarrow{v}\) est l'ensemble des points \(\mathrm{M}\) du plan tels que
les vecteurs \(\overrightarrow{\mathrm{AM}}\) et \(\overrightarrow{v}\) soient
orthogonaux.</div>
:
:
<div class="wims_thm"><h4>Propri�t�</h4>
Deux droites du plan de vecteurs normaux respectifs
\(\overrightarrow{u}\) et \(\overrightarrow{v}\) sont parall�les si et seulement
si les vecteurs \(\overrightarrow{u}\) et \(\overrightarrow{v}\) sont colin�aires.
</div>
:
:
<div class="wims_thm"><h4>Propri�t�</h4>
Deux droites du plan de vecteurs normaux respectifs
\(\overrightarrow{u}\) et \(\overrightarrow{v}\) sont perpendiculaires si et seulement
si les vecteurs \(\overrightarrow{u}\) et \(\overrightarrow{v}\) sont orthogonaux.
</div>
:
:
<div  class="wims_thm"><h4>Th�or�me</h4>
Le plan est muni d'un rep�re <strong>orthonorm�</strong>.<br>
 Si \(a x + b y +c = 0\) est une �quation cart�sienne de la droite
 \(\mathcal{D}\) alors le vecteur de coordonn�es \((a\,;b)\)
  est un vecteur normal � la droite <span style="white-space:nowrap">
  \(\mathcal{D}\).</span>
</div>
:
:
<div  class="wims_thm"><h4>Th�or�me</h4>
Le plan est muni d'un rep�re <strong>orthonorm�</strong>.<br>
Si \(\mathcal{D}\) est une droite dont un vecteur normal a pour coordonn�es
\((a \,;b)\) alors il existe un r�el \(c\) tel que
  \(a x + b y +c = 0\) soit une �quation cart�sienne de
 <span class="nowrap">\(\mathcal{D}\).</span>

</div>

