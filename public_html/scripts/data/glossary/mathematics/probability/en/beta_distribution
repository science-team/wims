!!abstract,linked gloses,internal links,content,dynamic examples,...
!set gl_author=Sophie, Lemaire
!set gl_keywords=discrete_probability_distribution
!set gl_title=beta distribution
!set gl_level=U1,U2,U3
:
:
:
:
<div class="wims_defn"><h4>Definition</h4>
Let \(a) and \(b) be two positive numbers. The <strong>beta distribution</strong> with parameters \(a) and \(b) (denoted by
\(\mathcal{B}(a,b))) is a continuous distribution over \(\rbrack 0, 1\lbrack)
with density function

<div class="wimscenter">
\(x\mapsto\frac{\Gamma(a+b)}{\Gamma(a)\Gamma(b)} x^{a-1} (1-x)^{b-1}1_{\rbrack 0, 1\lbrack}(x))
</div>
</div>
<table class="wimsborder wimscenter"><tr><th>Expectation</th>
<th>Variance</th><th>Probability generating function</th></tr><tr>
<td>\(\frac{a}{a+b})</td><td>\(\frac{ab}{(a+b)^2(a+b+1)})</td><td></td></tr></table>
