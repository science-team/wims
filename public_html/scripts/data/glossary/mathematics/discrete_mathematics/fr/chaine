!!abstract,linked gloses,internal links,content,dynamic examples,...
!set gl_author=Euler, Acad�mie de Versailles
!set gl_keywords=graph
!set gl_title=Cha�ne d'un graphe
!set gl_level=H6 G�n�rale&nbsp;Experte
:
:
:
:
<div class="wims_defn"><h4>D�finitions</h4>
  Soit \(G\) un graphe.
  <ul>
    <li>
      Une <strong>cha�ne</strong> de \(G\) est une liste ordonn�e de sommets de
      \(G\) telle que chaque sommet de la liste soit adjacent au suivant.
    </li><li>
      La <strong>longueur</strong> d'une cha�ne est le nombre d'ar�tes qui la
      composent.
    </li><li>
      La <strong>distance</strong> entre deux sommets de \(G\) est la plus petite
      des longueurs des cha�nes qui les relient.
    </li><li>
      Une cha�ne est dite <strong>ferm�e</strong> lorsque le sommet de d�part et
      le sommet d'arriv�e sont confondus.
    </li><li>
      Un <strong>cycle</strong> est une cha�ne ferm�e compos�e d'ar�tes toutes
      distinctes.
    </li>
  </ul>
</div>
:
:
<div class="wims_thm"><h4>Propri�t�</h4>
  Soit \(n\) un entier naturel non nul, soit \(G\) un graphe d'ordre \(n\) et
  soit \(A\) la matrice associ�e � ce graphe.
  <br>
  Soit \(p\) un entier naturel non nul et \(a_{i,j}\) les coefficients de la
  matrice <span class ="nowrap">\(A^p\) ;</span>
  <span class ="nowrap">\(A^p=(a_{i,j})\).</span>
  Alors, pour tous les entiers \(i\) et \(j\) tels que \(1\leqslant i\leqslant n\)
  et <span class ="nowrap">\(1\leqslant j\leqslant n),</span>
  \(a_{i,j}\) est �gal au nombre de cha�nes de longueur \(p\) permettant d'aller
  du sommet \(i\) au sommet <span class="nowrap">\(j\).</span>
</div>
:
:
<div class="wims_defn"><h4>D�finition</h4>
  Soit \(C\) l'ensemble des distances entre deux sommets quelconques d'un graphe
  <span class ="nowrap">\(G\).</span> Le <strong>diam�tre</strong> de
  \(G\) est le plus grand �l�ment de <span class ="nowrap">\(C\).</span>

</div>
