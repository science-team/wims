!!abstract,linked gloses,internal links,content,dynamic examples,...
!set gl_author=Euler, Acad�mie de Versailles
!set gl_keywords=combinatorics,combination
!set gl_title=Coefficient binomial (G�n�rale Sp�cialit�)
!set gl_level=H6 G�n�rale&nbsp;Sp�cialit�
:
:
:
:
<div class="wims_defn">
  <h4>D�finition</h4>
  Soit \(n\) un entier naturel et \(k\) un entier naturel inf�rieur ou �gal
  � <span class="nowrap">\(n\).</span>
  <br>
 Le <strong>coefficient binomial</strong> not� \(\displaystyle{\binom{n}{k}}\)
 (lire &#171; \(k\) parmi \(n\) &#187;) est le nombre de combinaisons de \(k\)
 �l�ments d'un ensemble poss�dant \(n\) �l�ments.
</div>
:
:
<div class="wims_rem">
  <h4>Cas particuliers</h4>
  <ul>
    <li>
    Pour tout entier naturel <span class="nowrap">\(n\),</span> \(\displaystyle{\binom{n}{0}=1}\) et
    <span class="nowrap">\(\displaystyle{\binom{n}{n}=1}\).</span>
    </li>
    <li>
    Pour tout entier naturel non nul <span class="nowrap">\(n\),</span> \(\displaystyle{\binom{n}{1}=n}\) et
    <span class="nowrap">\(\displaystyle{\binom{n}{n-1}=n}\).</span>
    </li>
  </ul>
</div>
:
:
<div class="wims_rem">
  <h4>Remarque</h4>
 On consid�re une exp�rience al�atoire constitu�e de la r�p�tition de \(n\)
 �preuves de Bernoulli identiques et ind�pendantes, repr�sent�e par un arbre.
 <br>
 L'entier \(\displaystyle{\binom{n}{k}}\) est le nombre de chemins de l'arbre
 r�alisant \(k\) succ�s pour les \(n\) r�p�titions de l'�preuve.
</div>
:
:
<div class="wims_thm">
  <h4>Th�or�me </h4>
Pour tout \(n \in \NN\) et pour tout \(k \in \NN\) tel que
\(0\leqslant k \leqslant n\)&nbsp;:

  <ul>
    <li>
    \(\begin{align*}
    \displaystyle{\binom{n}{k}} &= \frac{n(n-1) \times \ldots \times  (n-k+1)}{k!}
    &= \frac{n!}{(n-k)! \times k!}
    \end{align*}
       \)&nbsp;;
       </li>
       <li>
  \(\displaystyle{\binom{n}{k}=\binom{n}{n-k}}\)&nbsp;;
      </li>
      <li>
   si <span class="nowrap">\(k \ne n\),</span> <span class="nowrap">\(\displaystyle{\binom{n}{k}+\binom{n}{k+1}=\binom{n+1}{k+1}}\).</span>
      </li>
  </ul>
</div>


