!!abstract,linked gloses,internal links,content,dynamic examples,...
!set gl_author=Euler, Acad�mie de Versailles
!set gl_keywords=arithmetic,gcd_lcm
!set gl_title=PPCM (Plus petit commun multiple) de deux entiers naturels non nuls
!set gl_level=H6 G�n�rale&nbsp;Experte
:
:
:
:
<div class="wims_defn">
<h4>D�finition</h4>
Soit \(a\) et \(b\) deux entiers naturels non nuls.<br>
Le <strong>PPCM</strong> de \(a\) et \(b\) est le plus petit des multiples
communs � \(a\) et � <span class ="nowrap">\(b \).</span>
</div>
:
:
<div class="wims_rem">
<h4>Remarque</h4>
Si \(a\) est un multiple de \(b\) alors le PPCM de \(a\) et \(b\) est �gal �
<span class ="nowrap">\(a\).</span>
</div>
:
:
<div class="wims_thm">
<h4>Th�or�me</h4>
Soit \(a\) et \(b\) deux entiers naturels non nuls, \(d\) leur PGCD, \(m\) leur
 PPCM.<br>
Alors&nbsp;: <span class ="nowrap">\(m \times d = a \times b\).</span>
</div>
:
:
<div class="wims_rem">
<h4>M�thodes de d�termination du PPCM \(m\) de deux entiers naturels non nuls
\(a\) et <span class ="nowrap">\(b\) :</span></h4>
<ul>
<li>on d�compose chacun des deux entiers \(a\) et \(b\) en produit de nombres
premiers&nbsp;; \(m\) est �gal au produit des nombres premiers apparaissant dans
l'une au moins des deux d�compositions, chacun d'eux �tant affect� du plus grand
des deux exposants&nbsp;;</li>
<li>on d�termine le PGCD de \(a\) et \(b\) et on utilise le th�or�me pr�c�dent.</li></ul>
</div>
:mathematics/arithmetic/fr/lcm_1
:
<div class="wims_thm">
<h4>Th�or�me</h4>
Soit \(a\) et \(b\) deux entiers naturels non nuls.<br>
L'ensemble des multiples communs � \(a\) et � \(b\) est l'ensemble des multiples
de leur PPCM.
</div>

