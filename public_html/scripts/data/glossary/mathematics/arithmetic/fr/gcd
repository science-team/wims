!!abstract,linked gloses,internal links,content,dynamic examples,...
!set gl_author=Euler, Acad�mie de Versailles
!set gl_keywords=arithmetic,gcd_lcm,euclidean_algorithm,division,divisibility
!set gl_title=PGCD (Plus Grand commun diviseur) de deux entiers naturels non nuls
!set gl_level=H6 G�n�rale&nbsp;Experte
:
:
:
:
<div class="wims_defn"><h4>D�finition</h4>
Soit \(a\) et \(b\) deux entiers naturels non nuls.<br>
Le <strong>PGCD</strong> de \(a\) et \(b\) est le plus grand des diviseurs
communs � \(a\) et � <span class ="nowrap">\(b\).</span>
</div>
<div class="wims_rem"><h4>Remarque</h4>
Si \(b\) est un diviseur de \(a\) alors le PGCD de \(a\) et \(b\) est �gal �
<span class ="nowrap">\(b\).</span>
</div>
:
:
<div class="wims_defn"><h4>D�finition</h4>
Soit \(a\) et \(b\) deux entiers naturels non nuls.<br>
Si le PGCD de \(a\) et \(b\) est �gal � 1, on dit que \(a\) et \(b\)
sont <strong>premiers entre eux</strong>.</div>
:
:
<div class="wims_rem"><h4>M�thodes de d�termination du PGCD \(d\)
de deux entiers naturels non nuls \(a\) et
<span class ="nowrap">\(b\) :</span></h4>
<ul>
<li>on d�termine l'ensemble des diviseurs de \(a\) et l'ensemble des diviseurs
de <span class ="nowrap">\(b\) ;</span> \(d\) est le plus grand
�l�ment commun � ces deux ensembles&nbsp;;</li>
<li> par l'<strong>algorithme d'Euclide</strong>&nbsp;: on effectue la division
euclidienne de \(a\) par \(b\) <span class ="nowrap">(si \(a \geqslant b\)),</span> puis la division
euclidienne de \(b\) par le reste obtenu, puis celle de ce reste par le second
reste obtenu, et ainsi de suite. Apr�s un nombre fini d'�tapes, on obtient un
reste nul&nbsp;; \(d\) est le dernier reste non nul&nbsp;;</li>
<li>on d�compose chacun des deux entiers \(a\) et \(b\) en produit de nombres
premiers&nbsp;; \(d\) est �gal au produit des nombres premiers communs aux deux
d�compositions, chacun d'eux �tant affect� du plus petit des deux exposants.</li>
</ul>
</div>
:mathematics/arithmetic/fr/gcd_1
:
<div class="wims_thm"><h4>Th�or�me</h4>
Soit \(a\) et \(b\) deux entiers naturels non nuls.<br>
L'ensemble des diviseurs communs � \(a\) et � \(b\) est l'ensemble des diviseurs
de leur PGCD.</div>
:
:
<div class="wims_thm"><h4>Cons�quence</h4>
Tout diviseur commun aux deux entiers \(a\) et \(b\) divise leur PGCD.
</div>

