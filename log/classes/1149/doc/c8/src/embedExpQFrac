Soit \(n) un entier strictement positif alors la fonction qui � tout r�el positif ou nul \(x) associe \(x^n) est
d�finie, continue et strictement croissante sur &#91;0 ; \(+\infty )&#91 et l'ensemble des images
est \(\rbrack 0 ; +\infty \lbrack\).
<h3 class="l2w_content thm">Cons�quences</h3><div class="l2w_content thm">
Si \(a \ge 0) et si \(n \in \NN^*), l'�quation \(x^n = a) admet une solution
  et une seule dans \(\rbrack 0 ; +\infty \lbrack\) qui est appel�e
  la racine \(n)<sup>i�me</sup> de \(a).<br>
Si \(a) et \(b) sont des r�els positifs, si \(n \in \NN^*) et si \(a^n=b^n) alors \(b=a).
</div>
<h3 class="l2w_content defn">D�finition</h3><div class="l2w_content defn">
Si \(a \in \rbrack 0 ; +\infty \lbrack\) et \(n \in \NN^*),
 on note \(\root{n}{a}) l'unique solution r�elle positive ou nulle de l'�quation \(x^n=a).
</div>
<p>Ceci permet de calculer le taux annuel moyen ou le taux mensuel moyen.</p>
Si \(a \gt 0\) alors \((\root{4}{a^3})^100 = (a^3)^25 = a^75 = (\root{100}{a^75})^100\),
donc \(\root{4}{a^3} = \root{100}{a^75}) ;
or \(\frac{3}{4} = \frac{75}{100}), d'o� l'id�e d'�crire
\(\root{4}{a^3} = a^{\frac{3}{4}} = a^{\frac{75}{100}}= \root{100}{a^75})
et plus g�n�ralement de noter la racine de \(a) par \(a^{\frac{1}{n}}).
<h3 class="l2w_content defn">D�finition</h3><div class="l2w_content defn">
Si \(a \in \rbrack 0 ; +\infty \lbrack\) et \(n \in \NN^*),
on note \(a^{1/n}) l'unique solution r�elle positive ou nulle de l'�quation \(x^n=a).
</div>
Ainsi \(\root{n}{a} = a^{1/n}) mais \(0^0) n'est pas d�fini.
On d�montre en exercice la coh�rence de cette notation et que les r�gles
sur les puissances enti�res s'appliquent aussi
 aux puissances rationnelles de nombres r�els strictement positifs.
On d�finit ainsi \(a^{p/n} = \root{n}{a^p}) et \(a^{-p/n} = \frac{1}{\root{n}{a^p}})
pour \(a \gt) 0.
