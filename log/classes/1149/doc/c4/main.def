!set titb=Fonctions pourcentage
!set keyw=
!set datm=20210518
!set prev=
!set next=
!set upbl=
!set dat1=19000101
!set dat2=24000101

!if $wims_read_parm!=$empty
  !goto $wims_read_parm
!endif
!exit

:content
<h2>I Pourcentages directs (de proportion ou de r�partition)</h2>
<p>Calculer le pourcentage d'une quantit� par rapport � une autre, c'est trouver la valeur
de cette quantit� pour obtenir la m�me proportion en supposant que la r�f�rence soit �gale � 100.
</p>
En fait, il s'agit simplement de calculer la proportion d'une quantit� par rapport � une autre.
<div>
!read primitives.phtml 1, fold, \embed{P100Ex1}, <h3 class="l2w_content thm">Exemple</h3> 
</div>
<p>Il faut bien noter qu'<b>un pourcentage exprime une proportion et n'a de sens que si
on pr�cise quelle est la r�f�rence qui correspond aux 100 %</b>.</p>
<p>
Remarquer aussi que les calculs tombent rarement "justes" et qu'il faudra souvent arrondir le r�sultat, la pr�cision
peut varier selon le contexte (� 1 % pr�s, ou 0,1 % pr�s...).
Dans les calculs ult�rieurs, il faut reprendre le quotient exact pour
�viter de cumuler les erreurs d'arrondi.</p>
<u>Cas g�n�ral</u>&#58;
On peut consid�rer le tableau suivant, ce qui revient � l'�galit�

!insmath \frac{y}{x} = \frac{p}{100}= t
&#58;
<table class="wimscenter wimsborder">
<caption>Tableau de proportionnalit�</caption>
<tr><th>Effectif</th><th>Pourcentage</th></tr>
<tr><th>Partie</th><td>
!insmath y
</td><td>
!insmath p
</td></tr>
<tr><th>Total</th><td>
!insmath x
</td><td>100</td></tr>
</table>
Sch�matiquement &#58;
<div class="wims_columns">
<div class="medium_size">

!insmath \Large{x\underset{\phantom{xxx}\times t\phantom{xxx}}{\overset{p\%}{\mapsto}}y }

avec 
!insmath t=\frac{p}{100}
 et 
!insmath \frac{y}{x} = \frac{p}{100}

</div><div class="medium_size">
<ul><li>Calcul de la partie � partir du tout &#58; 
!insmath y=\frac{p}{100}x
</li>
<li>Calcul du tout � partir de la partie &#58; 
!insmath x = \frac{y}{t}
</li>
<li>Calcul du pourcentage &#58; 
!insmath p=\frac{y}{x} \times 100
</li>
</ul></div></div>
<h2>II Pourcentages d'augmentation</h2>

!read primitives.phtml 2, fold, \embed{P100Ex2}, <h3 class="l2w_content thm">Exemple</h3> 

<p>
<b>Augmenter une quantit� de 
!insmath p
 %, c'est la multiplier par 
!insmath (\frac{100+p}{100})
 autrement dit
par 
!insmath (1 +\frac{p}{100})
.</b>
</p>
On peut consid�rer le tableau suivant, ce qui revient � l'�galit� 
!insmath \frac{y}{x} = \frac{100 + p}{100}= 1 + t
 &#58;
<table class="wimscenter wimsborder">
<tr><td></td><th>Effectif</th><td>Pourcentage</th></tr>
<tr><th>Valeur finale</th><td>
!insmath y
</td><td>
!insmath 100 + p
</td></tr>
<tr><th>Valeur initiale</th><td>
!insmath x
</td><td>100</td></tr>
</table>
Sch�matiquement &#58;
<div class="wims_columns">
<div class="medium_size">

!insmath \Large{x\underset{\phantom{xxx}\times (1+t)\phantom{xxx}} {\overset{+ p\%}{\mapsto}}y }

avec 
!insmath t=\frac{p}{100}
, 
!insmath x
 = valeur initiale, 
!insmath y
 = valeur finale
</div><div class="medium_size">
<ul><li>Calcul de la valeur finale &#58; 
!insmath y=(1+\frac{p}{100}) x
</li>
<li>Calcul de la valeur initiale &#58; 
!insmath x = \frac{y}{1 + t}
</li>
<li>Calcul du pourcentage &#58; 
!insmath p=\frac{y - x}{x} \times 100
</li>
</ul></div></div>
<h3 class="l2w_content exemple">Exemple</h3><div class="l2w_content exemple">
Un DVD co�te 20 &euro; Toute Taxe Comprise, la TVA est de 15 % sur le prix Hors Taxe. Quel est le prix H.T. de ce DVD ?<br>
On cherche l'ant�c�dent � l'aide du sch�ma &#58;
<div class="wimscenter">

!insmath \Large{x\underset{\phantom{xxx} \times (1+0,15)\phantom{xxx}}{\overset{+ 15\%}{\mapsto}} 20}

</div>
Le prix H.T. de ce DVD est 
!insmath \frac{20}{1,15} \approx 17,39
 &euro; et non pas 
!insmath 20 - \frac{15}{100} 20
 car les 15 % portent sur 
!insmath x
 et non pas sur 20.<br>
Rechercher la valeur initiale avant une hausse de 15 % ne revient pas � appliquer une diminution de 15 % sur la valeur finale.<br>
</div>
<h2>III. Pourcentages de diminution</h2>
<b>Diminuer une quantit� de 
!insmath p
 %, c'est la multiplier
par 
!insmath (\frac{100-p}{100})
 autrement dit par 
!insmath (1 - \frac{p}{100})
.</b>
<br>
On peut consid�rer le tableau suivant, ce qui revient � l'�galit� &#58; 
!insmath \frac{y}{x} = \frac{100 - p}{100}= 1 - \frac{p}{100}
.
<table class="wimscenter wimsborder">
<tr><td></td><th>Effectif</th><th>Pourcentage</th></tr>
<tr><th>Valeur finale</th><td>
!insmath y
</td><td>
!insmath 100 - p
</td></tr>
<tr><th>Valeur initiale</th><td>
!insmath x
</td><td>100</td></tr>
</table>
Sch�matiquement &#58;
<div class="wims_columns">
<div class="medium_size">

!insmath \Large{x\underset{\phantom{xxx} \times (1-p/100)\phantom{xxx}}{\overset{-p\%}{\mapsto}}y }

avec 
!insmath t=-\frac{p}{100}
, 
!insmath t
 n�gatif,

!insmath x
 = valeur initiale et 
!insmath y
 = valeur finale
</div><div class="medium_size">
<ul><li>Calcul de la valeur finale &#58; 
!insmath y=(1 - \frac{p}{100}) x
</li>
<li>Calcul de la valeur initiale &#58; 
!insmath x = \frac{y}{1 - p/100}
</li>
<li>Calcul du taux 
!insmath t = \frac{- p}{100} =\frac{y - x}{x} = \frac{y}{x} - 1
</li>
<li>Calcul du pourcentage &#58; 
!insmath p = - \frac{y - x}{x} \times 100
</li>
</ul></div></div>
<br>
<p>Remarque &#58; Le taux d'�volution 
!insmath t
 est toujours �gal �

!insmath \frac{\text{valeur finale} - \text{valeur initiale}}{\text{valeur initiale}}
.
S'il est positif, il s'agit d'une augmentation ; s'il est n�gatif, il s'agit d'une diminution.</p>
<h3 class="l2w_content exemple">Exemple</h3><div class="l2w_content exemple">
Un DVD co�te 18 &euro; apr�s une remise de 25 % sur le prix initial. Quel �tait le prix de ce DVD avant la remise ?<br>
Il ne faut pas ajouter 25 % au prix final puisque les 25 % doivent s'appliquer sur le prix initial.<br>
On peut utiliser une �quation &#58; 
!insmath x - \frac{25}{100} x = 18
,
c'est-�-dire 
!insmath 0,75 x = 18
,
ou plus directement, rechercher l'ant�c�dent � l'aide du sch�ma &#58;
<div class="wimscenter">

!insmath \Large{x\underset{\phantom{xxx} \times (1-0,25)\phantom{xxx}}{\overset{-25 \%}{\mapsto}} 18 }

</div>
Le prix initial de ce DVD est 
!insmath \frac{18}{0,75}= 24
 &euro;.<br>
On n'obtient pas le bon r�sultat si on augmente le prix final de 25 %, cela donnerait 
!insmath 18 + \frac{25}{100} 18 = 22,5
.
</div>
<h2>IV Conclusion</h2>
Les types de pourcentages envisag�s ici sont &#58;
<ul>
<li><b>les pourcentages proportions</b> (incluant les pourcentages de r�partition),</li>
<li><b>les pourcentages d'augmentation</b> (qui font partie des pourcentages d'�volution),</li>
<li><b>les pourcentages de diminution</b> (qui font partie des pourcentages d'�volution).</li>
</ul>
Les types de questions envisag�es ici sont &#58;
<ul>
<li><b>trouver la valeur de la quantit� observ�e</b> (la valeur de la partie ou la valeur finale),</li>
<li><b>trouver le pourcentage 
!insmath p
</b> (ou le taux 
!insmath t
 = 
!insmath p
 % = 
!insmath p
/100 ou le coefficient multiplicateur),</li>
<li><b>trouver la r�f�rence des 100 %</b> (la totalit� ou la valeur initiale).</li>
</ul>
Les types de m�thodes propos�es ici sont &#58;
<ul>
<li><b>dessiner, sch�matiser</b> pour repr�senter la situation, <b>traduire g�om�triquement</b> le probl�me.</li>
<li><b>faire un tableau de proportionnalit�, �crire des proportions �gales, r�soudre une �quation</b>,</li>
<li><b>utiliser un op�rateur</b> ou <b>une fonction lin�aire</b>.</li>
</ul>
<h3 class="l2w_content warning">Attention</h3><div class="l2w_content warning">
<b>On ne peut additionner des pourcentages que s'ils ont la m�me r�f�rence.</b>
<p style="color&#58;red">
Chercher la valeur initiale avant une augmentation de 
!insmath p
 % ne consiste pas � diminuer la valeur finale de 
!insmath p
 %.<br>
Et chercher la valeur initiale avant une diminution de 
!insmath p
 % ne consiste pas � augmenter la valeur finale de 
!insmath p
 %.<br>
Une augmentation de 10 % suivie d'une diminution de 10 % sur la nouvelle valeur ne redonne pas la valeur initiale.
</p><p>
Il faut donc bien rep�rer si la situation correspond � une augmentation ou une diminution.
</p><p>
Il faut aussi distinguer ces pourcentages du calcul direct de la valeur de l'augmentation ou de celle du rabais,
qui sont des pourcentages proportions.</p>
<p>Le pourcentage de r�partition donne les proportions de chacun
des constituants d'un tout, ainsi il ne peut d�passer 100 %.
</p><p>
Enfin, une diminution de plus de 100 % est difficile � interpr�ter (si cela a un sens).
</p><p>
Il faut noter aussi qu'aucun pourcentage d'�volution ne peut se calculer � partir d'une valeur nulle.
</p>

