\title{Equazione di rette: dal grafico all'equazione}
\language{it}
\range{-5..5}
\author{Fr�d�ric PITOUN}
\email{lva.pitoun@free.fr}
\computeanswer{yes}
\format{html}
\precision{10000}

\integer{xmin=-8}
\integer{xmax=8}
\integer{ymin=-8}
\integer{ymax=8}
\integer{gradx=1}
\integer{grady=1}
\integer{nx=(\xmax-\xmin)/\gradx}
\integer{ny=(\ymax-\ymin)/\grady}
\integer{b=random(-7..7)}
\integer{xm=random(1..7)}
\integer{y=random(-7..7)}
\integer{xp=random(1..2)}
\integer{x=\xm*(-1)^\xp}
\rational{a=(\y-\b)/\x}
\function{eq=maxima(expand(\a*x+\b))}
\text{eqa=texmath(\eq)}
\statement{Vogliamo ricavare l'equazione della retta rappresentata in figura e scriverla nella forma \(y=mx+q). Determinare dal disegno i valori di \(m) e \(q)
:<br>
<p class="wimscenter">
\draw{400,400}{
xrange=\xmin,\xmax
yrange=\ymin,\ymax
linewidth=1
parallel \xmin,\ymin,\xmin,\ymax,\gradx,0,\nx,green
parallel \xmin,\ymin,\xmax,\ymin,0,\grady,\ny,green
linewidth=2
segment 0,\ymin,0,\ymax,red
segment \xmin,0,\xmax,0,red

text green,0,0,small,0
text green,\gradx,0,small,\gradx
text green,0,\grady,small,\grady

linewidth=2
plot blue,\a*x+\b
}
</p>
<p>L'equazione della retta �:
\(y=) \embed{reply 1,6} x + \embed{reply 2,6}
</p>
}

\answer{Coefficiente angolare}{\a}{type=default}
\answer{Ordinata dell'origine }{\b}{type=default}

\solution{L'equazione della retta � \(y = \eqa)}
